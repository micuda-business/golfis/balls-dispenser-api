from golferis.db.models import Player, Card, CreditTransaction, PlayerCredit

from data.utils import is_table_empty
from engine     import engine_current
from tables     import TempPlayers, TempCards, TempCreditTransactions, TempPlayerCredit

def migrate_players_data():
  if is_table_empty(Player.__tablename__):
    with engine_current.connect() as connection:
      connection.execute(
        f"""
        insert into golferis.{Player.__tablename__}
          (id, first_name, last_name, phone_number, golfer_id, email, is_active, updated_at, created_at)
          select
            id, first_name, last_name, phone_number, golfer_id, email, is_active, updated_at, created_at
          from golferis.{TempPlayers.name};
        """
      )

def migrate_cards_data():
  if is_table_empty(Card.__tablename__):
    with engine_current.connect() as connection:
      connection.execute(
        f"""
        insert into golferis.{Card.__tablename__}
          (id, card_number, is_active, owner_id, holder_id, updated_at, created_at)
          select
           id, card_number, is_active, owner_id, holder_id, updated_at, created_at
          from golferis.{TempCards.name};
        """
      )

def migrate_credit_transactions_data():
  if is_table_empty(CreditTransaction.__tablename__):
    with engine_current.connect() as connection:
      connection.execute(
        f"""
        insert into golferis.{CreditTransaction.__tablename__}
          (id, player_id, card_id, credits_count, notes, price, expiry_at, updated_at, created_at)
          select
           id, player_id, card_id, credits_count, notes, price, expiry_at, updated_at, created_at
          from golferis.{TempCreditTransactions.name};
        """
      )

def migrate_player_credits_data():
  if is_table_empty(PlayerCredit.__tablename__):
    with engine_current.connect() as connection:
      connection.execute(
        f"""
        insert into golferis.{PlayerCredit.__tablename__}
          (id, player_id, credit_purchase_id, credits_spend, updated_at, created_at)
          select
           id, player_id, credit_purchase_id, credits_spend, updated_at, created_at
          from golferis.{TempPlayerCredit.name};
        """
      )

def migrate_data():
  migrate_players_data()
  migrate_cards_data()
  migrate_credit_transactions_data()
  migrate_player_credits_data()
