-- CARDS
insert into golferis.temp_cards
  (id, card_number, is_active, owner_id, holder_id)

with
ungrouped_cards as (
  select
    cardholders.id  as cardholder_id,
    addcards.CardID as card_number,
    False           as is_primary
  from addcards
    inner join cardholders on addcards.`parent_CardID` = cardholders.`CardID`

  union all

  select
    cardholders.id     as cardholder_id,
    cardholders.CardID as card_number,
    True               as is_primary
  from cardholders
),
grouped_cards as (
  select
    cardholder_id,
    card_number,
    is_primary
  from ungrouped_cards
  group by card_number
),
cards as (
  select
    golferis.uuid_to_bin(uuid(), false) as id,
    card_number                         as card_number,
    temp_players.is_active              as is_active,
    temp_players.id                     as owner_id,
    temp_players.id                     as holder_id
  from grouped_cards
    inner join golferis.temp_players as temp_players on temp_players.cardholder_id = grouped_cards.cardholder_id
)
select
  id, card_number, is_active, owner_id, holder_id
from cards;
