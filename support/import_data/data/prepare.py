from __future__ import annotations

from uuid        import UUID
from pathlib     import Path
from typing      import List
from collections import deque
from itertools   import chain

from sqlalchemy     import and_
from sqlalchemy.sql import text, select

from data.utils import is_table_empty
from engine     import engine_current, engine_original
from tables     import TempPlayers, TempCards, TempCreditTransactions, TempPlayerCredit

CURRENT_DIR = Path(__file__).resolve(strict = True).parent

SETUP_PLAYERS_SQL             = CURRENT_DIR / 'setup_players.sql'
SETUP_CARDS_SQL               = CURRENT_DIR / 'setup_cards.sql'
SETUP_CREDIT_TRANSACTIONS_SQL = CURRENT_DIR / 'setup_credit_transactions.sql'

def prepare_from_file(file: str):
  with open(file, 'r') as f:
    with engine_original.connect() as connection:
      with connection.begin():
        connection.execute(text(f.read()))


class CreditPurchaseBase:
  def __init__(self):
    self.credit_spends      = []
    self.credit_transaction = None

  @property
  def expiry_at(self):
    return None

  @property
  def has_expiration(self):
    return self.expiry_at is not None

  @property
  def credits_spend(self):
    return len(self.credit_spends)

  def add_credit_spend(self, credit_spend):
    if self.credit_transaction is None:
      self.credit_transaction = credit_spend.credit_transaction

    self.credit_spends.append(credit_spend)

  @property
  def has_free_credits(self):
    return True

class CreditPurchaseOnDebt(CreditPurchaseBase):
  pass

class CreditPurchase(CreditPurchaseBase):
  def __init__(self, credit_transaction):
    super().__init__()

    self.credit_transaction = credit_transaction

  @property
  def expiry_at(self):
    return self.credit_transaction.expiry_at

  @property
  def has_free_credits(self):
    return (self.credit_transaction.credits_count - self.credits_spend) > 0

class CreditPurchaseQueue:
  def __init__(self, credit_purchases: List[CreditPurchase]):
    self._current   = None
    self._processed = []
    self.queue      = deque(credit_purchases)

    self.next()

  @property
  def current(self) -> CreditPurchaseBase:
    return self._current

  def next(self):
    if isinstance(self.current, CreditPurchaseOnDebt):
      return

    try:
      self._current = self.queue.popleft()
    except IndexError:
      self._current = CreditPurchaseOnDebt()

    self._processed.append(self._current)

  def __iter__(self):
    return chain(self._processed, self.queue)

class CreditSpend:
  def __init__(self, credit_transaction):
    self.credit_transaction = credit_transaction

  @property
  def created_at(self):
    return self.credit_transaction.created_at


def serialize_credit_purchase(credit_purchase: CreditPurchaseBase) -> dict:
  if (credit_transaction := credit_purchase.credit_transaction) is None:
    return
  credit_purchase_id = \
    None \
    if   isinstance(credit_purchase, CreditPurchaseOnDebt) \
    else credit_purchase.credit_transaction.id

  return {
    'credit_purchase_id': credit_purchase_id,
    'credits_spend':      credit_purchase.credits_spend,
    'player_id':          credit_transaction.player_id,
    'updated_at':         credit_transaction.updated_at,
    'created_at':         credit_transaction.created_at,
    'credit_spends':      [serialize_credit_spend(cs) for cs in credit_purchase.credit_spends]
  }

def serialize_credit_spend(credit_spend: CreditSpend) -> dict:
  return {
    'credit_transaction_id': credit_spend.credit_transaction.id
  }

class PreparePlayerCreditsData:
  def __call__(self):
    for player in self.all_players:
      self.process_credit_transactions_for(player)

  @property
  def all_players(self):
    get_players_stmt = select(TempPlayers.c)

    with engine_current.connect() as connection:
      players = connection.execute(get_players_stmt).fetchall()

    for player in players:
      yield player

  def process_credit_transactions_for(self, player):
    credit_purchases = CreditPurchaseQueue(self._credit_purchases_for(player))
    credit_spends    = self._credit_spends_for(player)

    for credit_spend in credit_spends:
      self._process_credit_transaction(credit_purchases, credit_spend)

    with engine_current.connect() as connection:
      with connection.begin():
        self._save_credit_purchases(connection, credit_purchases)

  def _save_credit_purchases(self, connection, credit_purchases: CreditPurchaseQueue):
    for credit_purchase in credit_purchases:
      if (data := serialize_credit_purchase(credit_purchase)) is None:
        continue

      player_credit_id = self._create_temp_player_credit(
        connection,
        player_id          = data['player_id'],
        updated_at         = data['updated_at'],
        created_at         = data['created_at'],
        credits_spend      = data['credits_spend'],
        credit_purchase_id = data['credit_purchase_id']
      )

      self._link_temp_credit_transaction_to_player_credit(
        connection, data['credit_purchase_id'], player_credit_id
      )

      for credit_spend_data in data['credit_spends']:
        self._link_temp_credit_transaction_to_player_credit(
          connection, credit_spend_data['credit_transaction_id'], player_credit_id
        )

  @staticmethod
  def _link_temp_credit_transaction_to_player_credit(
    connection, temp_credit_transtaction_id, temp_player_credit_id
  ):
    update_credit_transaction = TempCreditTransactions \
      .update() \
      .where(TempCreditTransactions.c.id == temp_credit_transtaction_id) \
      .values(player_credit_id = temp_player_credit_id)

    connection.execute(update_credit_transaction)

  def _process_credit_transaction(
    self, credit_purchases: CreditPurchaseQueue, credit_spend: CreditSpend
  ):
    current_purchase = credit_purchases.current

    if current_purchase.has_expiration:
      if credit_spend.created_at <= current_purchase.expiry_at and current_purchase.has_free_credits:
        current_purchase.add_credit_spend(credit_spend)
      else:
        credit_purchases.next()
        self._process_credit_transaction(credit_purchases, credit_spend)

    elif current_purchase.has_free_credits:
      current_purchase.add_credit_spend(credit_spend)

    else:
      credit_purchases.next()
      self._process_credit_transaction(credit_purchases, credit_spend)

  @staticmethod
  def _create_temp_player_credit(
    connection, *, credits_spend, player_id, updated_at, created_at, credit_purchase_id
  ) -> UUID:
    insert_temp_player_credit_stmt = TempPlayerCredit \
      .insert() \
      .values(player_id          = player_id,
              updated_at         = updated_at,
              created_at         = created_at,
              credits_spend      = credits_spend,
              credit_purchase_id = credit_purchase_id)

    result = connection.execute(insert_temp_player_credit_stmt)

    return result.inserted_primary_key[0]

  @staticmethod
  def _credit_purchases_for(player) -> List[CreditPurchase]:
    purchases_only_stmt = select(TempCreditTransactions.c) \
      .where(and_(TempCreditTransactions.c.price.isnot(None),
                  TempCreditTransactions.c.player_id == player.id)) \
      .order_by(TempCreditTransactions.c.created_at)

    with engine_current.connect() as connection:
      return [CreditPurchase(ct) for ct in connection.execute(purchases_only_stmt).fetchall()]

  @staticmethod
  def _credit_spends_for(player) -> List[CreditSpend]:
    credit_spends_only_stmt = select(TempCreditTransactions.c) \
      .where(and_(TempCreditTransactions.c.price.is_(None),
                  TempCreditTransactions.c.player_id == player.id)) \
      .order_by(TempCreditTransactions.c.created_at)

    with engine_current.connect() as connection:
      return [CreditSpend(ct) for ct in connection.execute(credit_spends_only_stmt).fetchall()]


def prepare_players_data():
  if is_table_empty(TempPlayers.name):
    prepare_from_file(SETUP_PLAYERS_SQL)

def prepare_cards_data():
  if is_table_empty(TempCards.name):
    prepare_from_file(SETUP_CARDS_SQL)

def prepare_credit_transactions_data():
  if is_table_empty(TempCreditTransactions.name):
    prepare_from_file(SETUP_CREDIT_TRANSACTIONS_SQL)

prepare_player_credits_data = PreparePlayerCreditsData()


def prepare_data():
  prepare_players_data()
  prepare_cards_data()
  prepare_credit_transactions_data()
  prepare_player_credits_data()
