-- CREDIT TRANSACTIONS
insert into golferis.temp_credit_transactions
  (id, player_id, card_id, credits_count, notes, price, expiry_at, created_at)

with
buying_credit as (
  select
    golferis.uuid_to_bin(uuid(), false)          as id,
    temp_cards.owner_id                          as player_id,
    temp_cards.id                                as card_id,
    BasketNumber                                 as credits_count,
    null                                         as notes,
    cast(Price as decimal(8, 2))                 as price,
    case rocnik
      when 0
        then null
      else
        concat(rocnik, '-12-31 23:59:59')
    end                                          as expiry_at,
    BuyingDate                                   as created_at
  from logbuyingballs
  inner join golferis.temp_cards as temp_cards on temp_cards.card_number = CardID
),
spending_credit as (
  select
    golferis.uuid_to_bin(uuid(), false)          as id,
    temp_cards.owner_id                          as player_id,
    temp_cards.id                                as card_id,
    1                                            as credits_count,
    case poznamka
      when ''
        then null
      else
        poznamka
    end                                          as notes,
    null                                         as price,
    null                                         as expiry_at,
    TakeDate                                     as created_at
  from logtakeball
  inner join golferis.temp_cards as temp_cards on temp_cards.card_number = CardID
),
credit_transactions as (
  select * from buying_credit
  union all
  select * from spending_credit
)
select
  id, player_id, card_id, credits_count, notes, price, expiry_at, created_at
from credit_transactions
order by created_at;
