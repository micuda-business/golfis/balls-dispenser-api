from engine import engine_current

def is_table_empty(table_name: str) -> bool:
  with engine_current.connect() as connection:
    count = connection.execute(f'select count(*) from {table_name};').scalar()

  return count == 0
