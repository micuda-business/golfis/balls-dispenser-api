-- PLAYERS
insert into golferis.temp_players
  (id, cardholder_id, first_name, last_name, phone_number, golfer_id, email, created_at, is_active)

with
players_name_parts_count as (
  select
    ID                                                as cardholder_id,
    length(Name) - length(replace(Name, ' ', '')) + 1 as name_parts_count
  from cardholders
),

player_lowercased_names as (
  select
    cardholder_id,
    case name_parts_count
      when 1 -- e.g. cardholder "Tateyama"
        then null
      when 2
        then
          case cardholder_id
            when 111 -- e.g. cardholder ". PRODEJ"
              then null
            else
              lower(substring_index(Name, ' ', -1))
          end
      when 3 -- e.g. cardholder "Plachky Ludek JUN", "Spurnik Mirko ML"
        then concat(lower(substring_index(substring_index(Name, ' ', -2), ' ', 1)), ' Jnr.')
      else -- cardholders e.g. "AKADEMIE - ŠKOLA - TRÉNINKY"
        null
    end                                                                                           as first_name,

    case name_parts_count
      when 1 -- e.g. cardholder "Tateyama"
        then lower(Name)
      when 2
        then
          case cardholder_id
            when 111 -- e.g. cardholder ". PRODEJ"
              then upper(Name)
            else
              lower(substring_index(Name, ' ', 1))
          end
      when 3 -- e.g. cardholder "Plachky Ludek JUN", "Spurnik Mirko ML"
        then lower(substring_index(Name, ' ', 1))
      else -- cardholders e.g. "AKADEMIE - ŠKOLA - TRÉNINKY"
        upper(Name)
    end                                                                                           as last_name
  from cardholders
    inner join players_name_parts_count on cardholders.ID = players_name_parts_count.cardholder_id
),

player_names as (
  select
    player_lowercased_names.cardholder_id as cardholder_id,

    case player_lowercased_names.first_name
      when null
        then null
      else
        concat(upper(left(player_lowercased_names.first_name, 1)), substring(player_lowercased_names.first_name, 2))
    end                                                                                                                   as first_name,

    case players_name_parts_count.name_parts_count
      when 1
        then concat(upper(left(player_lowercased_names.last_name, 1)), substring(player_lowercased_names.last_name, 2))
      when 2
        then
          case player_lowercased_names.cardholder_id
            when 111
              then player_lowercased_names.last_name
            else
              concat(upper(left(player_lowercased_names.last_name, 1)), substring(player_lowercased_names.last_name, 2))
          end
      when 3
        then
          concat(upper(left(player_lowercased_names.last_name, 1)), substring(player_lowercased_names.last_name, 2))
      else
        player_lowercased_names.last_name
    end                                                                                                                   as last_name

  from player_lowercased_names
    inner join players_name_parts_count on player_lowercased_names.cardholder_id = players_name_parts_count.cardholder_id
),

players as (
  select
    uuid()                     as id,
    player_names.cardholder_id as cardholder_id,
    player_names.first_name    as first_name,
    player_names.last_name     as last_name,
    case cardholders.Telephone
      when '0'
        then null
      else
        cardholders.Telephone
    end                        as phone_number,
    case cardholders.GolferNumber
      when ''
        then null
      else
        cardholders.GolferNumber
    end                        as golfer_id,
    null                       as email,
    cardholders.InsertDate     as created_at,
    case cardholders.State
      when 'DISABLED'
        then false
      else
        true
    end                        as is_active
  from cardholders
    inner join player_names on cardholders.ID = player_names.cardholder_id
)
select
  golferis.uuid_to_bin(id, false), cardholder_id, first_name, last_name, phone_number, golfer_id, email, created_at, is_active
from players;
