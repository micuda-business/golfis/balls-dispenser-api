select
  trim(c.`Name`) as name,
  case
    when c.`GolferNumber` = ''
      then '-'
    when c.`GolferNumber` is null
      then '-'
    else
      trim(c.`GolferNumber`)
  end as golfer_id,

  /* výběry bez ročníku a s aktuálním ročníkem */
  IFNULL((SELECT SUM(b.BasketNumber) FROM logbuyingballs as b WHERE c.CardID = b.CardID AND (b.rocnik = 0 OR b.rocnik = YEAR(CURDATE())) GROUP BY b.CardID ),0)
  +

  /* co bylo vybrané v daných ročnících */
  IFNULL(
      (SELECT
      SUM(i.aktualne) as plus
      FROM
      (
          /* výpočty vybraných za ročníky */
          SELECT
          lb.CardID,
          lb.rocnik,
          IFNULL(Sum(lb.BasketNumber),0) AS koupeno,
          IFNULL(lt.vydano,0) + IFNULL(lt1.vydano_add,0) as vydano,
          IF(IFNULL(Sum(lb.BasketNumber),0) - (IFNULL(lt.vydano,0) + IFNULL(lt1.vydano_add,0)) <= 0, IFNULL(Sum(lb.BasketNumber),0),(IFNULL(lt.vydano,0) + IFNULL(lt1.vydano_add,0))) as aktualne

          FROM
          logbuyingballs AS lb

          /* připojené vybrané cards */
          LEFT JOIN (
              SELECT
              DATE_FORMAT(TakeDate,'%Y') as rok,
              CardID,
              Count(id) AS vydano
              FROM logtakeball
              GROUP BY
              CardID,DATE_FORMAT(TakeDate,'%Y')
          ) as lt on lt.rok = lb.rocnik and lt.CardID = lb.CardID

          /* připojené vybrané add_cards */
          LEFT JOIN (
              SELECT
              DATE_FORMAT(TakeDate,'%Y') as rok,
              CardID,
              Count(id) AS vydano_add
              FROM logtakeball
              GROUP BY
              CardID,DATE_FORMAT(TakeDate,'%Y')
          ) as lt1 on lt1.rok = lb.rocnik and lt1.CardID IN
                  (SELECT
                  ac.CardID
                  FROM
                  addcards AS ac
                  WHERE
                  ac.parent_CardID = lb.CardID)

          /* definice CardID */
          INNER JOIN cardholders as ch on ch.CardID = lb.CardID

          WHERE
          lb.rocnik > 0 AND lb.rocnik <> YEAR(CURDATE())
          GROUP BY
          lb.rocnik, lb.CardID) as i
  WHERE i.CardID = c.CardID
      ),0
  )

  -

  ifnull(
    (SELECT COUNT(v.id) FROM logtakeball as v WHERE c.CardID = v.CardID GROUP BY v.CardID),
    0
  ) as available_credits

from cardholders as c
order by c.`InsertDate`;


