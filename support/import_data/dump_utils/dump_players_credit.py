#!/usr/bin/env python3
from golferis            import crud
from golferis.config     import settings
from golferis.db.session import session_provider
from golferis.db.models  import Player

def run():
  with session_provider() as db:
    players = db.query(Player).order_by(Player.created_at).all()

    with open(settings.PROJECT_DIR / '..' / '..' / 'players_credit_imported.txt', 'w') as f:
      f.write(f'name\tgolfer_id\tavailable_credits\n')
      # f.write(f'golfer_id available_credits\n')

      for player in players:
        credits = crud.player.get_credit_amount(db, player_id = player.id)

        f.write(f'{player.fullname}\t{player.golfer_id or "-"}\t{credits}\n')
        # f.write(f'{player.golfer_id or "-"} {credits}\n')

if __name__ == '__main__':
  run()
