from sqlalchemy import create_engine

from golferis.config     import settings, MySqlDsn
from golferis.db.session import engine

GOLFERIS_ORIGINAL_DATABASE_URI = MySqlDsn.build(
  scheme   = settings.DATABASE_URI.scheme,
  user     = settings.DATABASE_URI.user,
  password = settings.DATABASE_URI.password,
  host     = settings.DATABASE_URI.host,
  path     = '/golferis_orig',
  query    = settings.DATABASE_URI.query
)

engine_current  = engine
engine_original = create_engine(GOLFERIS_ORIGINAL_DATABASE_URI)
