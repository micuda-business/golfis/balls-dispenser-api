from functools import partial

from sqlalchemy import Table, MetaData, Column, Integer, Boolean, Numeric

from golferis.db.models.fields import (
  PrimaryKey, CharField, ForeignKey, UpdatedAtField, CreatedAtField, UUID, DateTimeField
)

NullableUUIDField = partial(Column, UUID, nullable = True, default = None, server_default = None)

metadata = MetaData()

TempPlayers = Table('temp_players', metadata,
  PrimaryKey(name = 'id'),
  CharField(name = 'first_name', length = 50,  nullable = True, default = None, server_default = None),
  CharField(name = 'last_name', length = 100, nullable = True, default = None, server_default = None),
  CharField(name = 'phone_number', length = 15,  nullable = True, default = None, server_default = None),
  CharField(name = 'golfer_id', length = 7,   nullable = True, default = None, server_default = None),
  CharField(name = 'email', length = 254, nullable = True, default = None, server_default = None),
  Column(Boolean, name = 'is_active', default = False),
  UpdatedAtField(name = 'updated_at'),
  CreatedAtField(name = 'created_at'),
  Column(Integer, name = 'cardholder_id', nullable = False)
)

TempCards = Table('temp_cards', metadata,
  PrimaryKey(name = 'id'),
  CharField(name = 'card_number', length = 20),
  Column(Boolean, name = 'is_active', default = False),
  NullableUUIDField(name = 'owner_id'),
  NullableUUIDField(name = 'holder_id'),
  UpdatedAtField(name = 'updated_at'),
  CreatedAtField(name = 'created_at')
)

TempCreditTransactions = Table('temp_credit_transactions', metadata,
  PrimaryKey(name = 'id'),
  NullableUUIDField(name = 'player_id'),
  NullableUUIDField(name = 'card_id'),
  Column(Integer, name = 'credits_count'),
  CharField(name = 'notes', length = 512, default = None, server_default = None),
  Column(Numeric(8, 2), name = 'price', default = None, server_default = None),
  DateTimeField(name = 'expiry_at', default = None, server_default = None),
  UpdatedAtField(name = 'updated_at'),
  CreatedAtField(name = 'created_at'),
  NullableUUIDField(name = 'player_credit_id')
)

TempPlayerCredit = Table('temp_player_credits', metadata,
  PrimaryKey(name = 'id'),
  NullableUUIDField(name = 'player_id'),
  NullableUUIDField(name = 'credit_purchase_id'),
  Column(Integer, name = 'credits_spend', default = 0),
  UpdatedAtField(name = 'updated_at'),
  CreatedAtField(name = 'created_at')
)
