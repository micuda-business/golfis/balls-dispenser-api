#!/usr/bin/env python3
from tables import metadata
from engine import engine_current
from data   import prepare_data, migrate_data

def create_schema():
  metadata.create_all(bind = engine_current)

def run():
  create_schema()

  prepare_data()

  migrate_data()


if __name__ == '__main__':
  run()
