delimiter $$

create function bin_to_uuid(b binary(16), f boolean)
  returns char(36)
  deterministic
begin
   declare hex_str char(32);
   set hex_str = hex(b);
   return lower(
     concat(
       if(f,substr(hex_str, 9, 8),substr(hex_str, 1, 8)),
       '-',
       if(f,substr(hex_str, 5, 4),substr(hex_str, 9, 4)),
       '-',
       if(f,substr(hex_str, 1, 4),substr(hex_str, 13, 4)),
       '-',
       substr(hex_str, 17, 4), '-',
       substr(hex_str, 21)
     )
  );
end$$


create function uuid_to_bin(uuid char(36), f boolean)
  returns binary(16)
  deterministic
begin
  return unhex(
    concat(
      if(f, substring(uuid, 15, 4), substring(uuid, 1, 8)),
      substring(uuid, 10, 4),
      if(f, substring(uuid, 1, 8), substring(uuid, 15, 4)),
      substring(uuid, 20, 4),
      substring(uuid, 25)
    )
  );
end$$

delimiter ;
