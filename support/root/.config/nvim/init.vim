set rtp+=~/.fzf

" vundle
call plug#begin('~/.local/share/nvim/bundle')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --key-bindings --completion --no-update-rc' }
Plug 'junegunn/fzf.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dispatch'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'junegunn/vim-easy-align'
Plug 'pgdouyon/vim-evanesco'
Plug 'itchyny/lightline.vim'
Plug 'machakann/vim-highlightedyank'
Plug 'christoomey/vim-tmux-navigator'

call plug#end()

colorscheme palenight

set encoding=utf-8
set number
set relativenumber
set backupcopy=yes
set noshowmode
set updatetime=100

set hlsearch
set incsearch
set ignorecase
set smartcase

let g:highlightedyank_highlight_duration=500

" maintain undo btw sessions
set undodir=~/.local/share/nvim/undodir
set undofile

" filetype associations
au BufRead,BufNewFile *.txt    set filetype=zim
au BufRead,BufNewFile *.md     set filetype=markdown
au BufRead,BufNewFile *.asd    set filetype=lisp
au BufRead,BufNewFile *.tex    set filetype=tex
au BufRead,BufNewFile *.scheme set filetype=scheme
au BufRead,BufNewFile *.god    set filetype=ruby
au BufRead,BufNewFile *.py     set textwidth=100 tabstop=2 softtabstop=2 shiftwidth=2 expandtab foldmethod=indent
au FileType sh                 set noexpandtab

" map , (reverse of ;) to \
noremap \ ,
" use , as <leader>
let mapleader=','

" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :wincmd =<cr>

" disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

" close the previous window, especially used for closing floating windows
map <leader>c <c-w>p :q<cr>

" toggle visibility of nonprintable characters
map <leader>h :set list!<cr>

" visual symbols for nonprintable characters
set listchars=tab:▸\ ,eol:¬

" strip trailing whitespace when saving file
function! <SID>StripTrailingWhitespaces()
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  %s/\s\+$//e
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" always show at least 5 lines around cursor
set scrolloff=3
set sidescrolloff=3
set display+=lastline

" use ag as grep program
set grepprg=ag

" set search matches highlighting color
hi Search ctermbg=13

" use c-d as delete in insert mode
inoremap <c-d> <del>

" for compatibility with fish shell
set shell=/bin/bash

" %% in command expands to current dir
cnoremap %% <c-r>=fnameescape(expand('%:h')).'/'<cr>

" needed for % to jump begin/def/class <-> end, etc.
runtime macros/matchit.vim

" use ,f and ,b for fuzzy search in files and buffers
map <leader>f :Files<cr>
map <leader>p :Files ..<cr>
map <leader>b :Buffers<cr>

" use enter in normal mode to insert newline
nnoremap <cr> o<esc>k
autocmd CmdwinEnter * nnoremap <cr> <cr>
autocmd BufReadPost quickfix nnoremap <cr> <cr>

" use ,n to hide search results
map <leader>n :nohl<cr>

" use <space><char> to insert single character from normal mode
nmap <space> i_<esc>r

set laststatus=2
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set textwidth=100
" see :h fo-table, add a to reformat paragraph after every change
" (:set formatoptions+=a)
set formatoptions=tcrqj
" set formatprg=par\ -w80r

" highlight current line
set cursorline

" use ,, to switch btw. alternate files
" this is slow, because easymotion has mapping starting with ,,
map <leader><leader> <c-^>

set wildignore+=.git*,coverage/**,tmp/**,**~

" code folding options
set foldmethod=syntax
set foldlevel=1
set nofoldenable

" open new splits right and below
set splitbelow
set splitright

" complete with tab, only to longest match
" imap <tab> <c-p>
set completeopt=menu,longest,preview

" completion in command
set wildmode=longest,list:longest

" reindent whole file (make mark, jump to bof, = to eof, back to mark)
map <leader>i mmgg=G`m

set winheight=5
set winminheight=5
set winwidth=85
set winminwidth=65

autocmd WinEnter * wincmd _
" autocmd WinEnter * wincmd |

let g:ackprg = 'ag --vimgrep'

" disable arrows
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" editorconfig
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" " Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

let g:python3_host_prog = "/home/golferis/.pyenv/versions/nvim3.8.2/bin/python"

" lightline
let g:lightline = {
      \ 'active': {
      \   'left':  [['mode', 'paste'],
      \             ['readonly', 'python_info', 'filename']],
      \   'right': [['lineinfo'], ['percent'], ['fileformat', 'fileencoding', 'filetype']]
      \ },
      \ 'inactive': {
      \   'left':  [['filename']],
      \   'right': [['lineinfo'], ['percent']]
      \ },
      \ 'component_function': {
      \   'filename':     'LightLineFilenameWithModified',
      \   'python_info':  'LightLinePythonInfo',
      \ },
      \ }

function! LightLineFilenameWithModified()
  let filename = expand('%:t') ==# '' ? '[No Name]' : expand('%:t')
  let modified = &modified ? ' +' : ''

  return filename . modified
endfunction

function! LightLinePythonInfo()
  " only for python files
  if &filetype !=# 'python'
    return ''
  endif

  " show name of virtualenv
  let env_name = $VIRTUAL_ENV
  if env_name !=# ''
    return split(env_name, '/')[-1]
  endif

  " ... or version of python if there is no virtualenv active
  if has('python3')
    python3 import sys, vim
    python3 major, minor, micro = sys.version_info[:3]
    python3 vim.command(f"let g:python_version = '{major}.{minor}.{micro}'")
  elseif has('python')
    python import sys, vim
    python major, minor, micro = sys.version_info[:3]
    python vim.command("let g:python_version = '{major}.{minor}.{micro}'".format(major = major, minor = minor, micro = micro))
  endif

  return 'Python ' . get(g:, 'python_version', 'unknown')
endfunction
