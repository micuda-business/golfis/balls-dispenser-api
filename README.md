# golferis - ball dispenser

## installation

```bash
sudo apt-get update
sudo apt-get install mariadb-server libmariadbclient-dev
sudo mysql_secure_installation
```

```bash
pip install -e .[dev,test,pytest-watch]
```

## setup db

```bash
sudo mysql -u root
```

* create database
``` sql
create database golferis character set utf8 collate utf8_czech_ci;
create database golferis_test character set utf8 collate utf8_czech_ci;
```

* create user and set privileges
```sql
create user 'golferis'@'localhost' identified by '';
grant all privileges on * . * to 'golferis'@'localhost';
flush privileges;
```

* exit the `mysql` terminal

* apply migrations
```bash
schema_migrate.py head
```

## prepare data
* create superuser
```bash
create_superuser.py user@email.com
```

* [optional] migrate data from Tomas' structure
```bash
./support/import_data/main.py
```

## run server
