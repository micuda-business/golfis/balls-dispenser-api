import pytest

from golferis.utils import string

@pytest.mark.parametrize(
  'text, expected', [
    pytest.param('ÁÉÍÝÓÚŮČĎĚŇŘŠŤŽ', 'AEIYOUUCDENRSTZ', id = 'uppercase_letters'),
    pytest.param('áéíýóúůčďěňřšťž', 'aeiyouucdenrstz', id = 'lowercase_letters')
  ]
)
def test_strip_accent(text, expected):
  actual = string.strip_accents(text)

  assert actual == expected
