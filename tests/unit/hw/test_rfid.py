import pytest, flexmock
import wiringpi

from hwberry.rfid import WiegandRFID

PIN_0     = 0  # GPIO Pin 17 |  Green cable | Data0
PIN_1     = 2  # GPIO Pin 27 |  White cable | Data1
PIN_SOUND = 25 # GPIO Pin 26 | Yellow cable | Sound

class RFIDReaderHelpers:
  rfid_reader = None

  def setup_rfid_reader(self):
    flexmock(wiringpi,
             wiringPiSetup = lambda:           None,
             wiringPiISR   = lambda *args:     None,
             pinMode       = lambda pin, mode: None)

    self.rfid_reader = WiegandRFID(d0_pin = PIN_0, d1_pin = PIN_1, sound_pin = PIN_SOUND)

  def mock_get_monotonic_timestamp(self, *, returns):
    return flexmock(self.rfid_reader, _get_monotonic_timestamp = lambda: returns)

  @property
  def reader_timeout(self):
    return self.rfid_reader.READER_TIMEOUT

class HasDataTests(RFIDReaderHelpers):
  setup = pytest.fixture(autouse = True)(lambda self: self.setup_rfid_reader())

  def test_returns_false_if_read_time_is_lower_or_equal_than_readers_timeout(self):
    self.mock_get_monotonic_timestamp(returns = self.reader_timeout - 1)

    assert self.rfid_reader.has_data is False

  def test_returns_false_if_no_data_was_read(self):
    self.mock_get_monotonic_timestamp(returns = self.reader_timeout + 1)

    assert self.rfid_reader.has_data is False

  def test_returns_true_if_data_was_read(self):
    self.mock_get_monotonic_timestamp(returns = self.reader_timeout + 1)
    self.rfid_reader._bit_count = 1

    assert self.rfid_reader.has_data is True

class ReadDataTests(RFIDReaderHelpers):
  setup = pytest.fixture(autouse = True)(lambda self: self.setup_rfid_reader())

  def test_read_data_while_no_data_are_processed(self):
    rfid_reader_mock = \
      self.mock_get_monotonic_timestamp(returns = self.reader_timeout + 1)
    rfid_reader_mock.should_call('_reset').never()

    actual_bit_count,   actual_data   = self.rfid_reader.read_data()
    expected_bit_count, expected_data = 0, []

    assert actual_bit_count == expected_bit_count
    assert actual_data      == expected_data

  def test_read_data_while_some_data_are_processed(self):
    rfid_reader_mock = \
      self.mock_get_monotonic_timestamp(returns = self.reader_timeout + 1)
    rfid_reader_mock.should_call('_reset').once()
    self.rfid_reader._bit_count = 26
    self.rfid_reader._data[:4]  = [185, 250, 237, 3]

    actual_bit_count,   actual_data   = self.rfid_reader.read_data()
    expected_bit_count, expected_data = 26, [185, 250, 237, 3]

    assert actual_bit_count == expected_bit_count
    assert actual_data      == expected_data

# TODO test get_data_0
# TODO test get_data_1
# TODO test beep
