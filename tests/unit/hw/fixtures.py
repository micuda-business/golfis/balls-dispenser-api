import sys
import pytest, flexmock

@pytest.fixture
def mock_rpi_gpio():
  rpi_gpio_mock = flexmock(IN = 1, OUT = 0, HIGH = 1, LOW = 0,
                           input   = lambda gpio:             0,
                           output  = lambda gpio, value:      None,
                           setup   = lambda gpio, direction:  None,
                           cleanup = lambda gpio_list = None: None)

  if 'RPi.GPIO' not in sys.modules:
    sys.modules['RPi.GPIO'] = rpi_gpio_mock

  return rpi_gpio_mock
