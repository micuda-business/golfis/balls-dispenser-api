import pytest, flexmock, time
import smbus

from hwberry.utils import I2CDevice

DATA           = 'data'
COMMAND        = 0x08
DEVICE_ADDRESS = 0x3F

class I2CDeviceHelpers:
  i2c_device  = None
  sm_bus_mock = None

  def setup_i2c_device(self):
    sm_bus_mock = flexmock()
    flexmock(smbus, SMBus = lambda port: sm_bus_mock)

    self.sm_bus_mock = sm_bus_mock
    self.i2c_device  = I2CDevice(addr = DEVICE_ADDRESS, port = 1)

class I2CDeviceReadTests(I2CDeviceHelpers):
  setup = pytest.fixture(autouse = True)(lambda self: self.setup_i2c_device())

  def test_read(self):
    self.sm_bus_mock \
      .should_receive('read_byte') \
      .with_args(DEVICE_ADDRESS) \
      .and_return('value') \
      .once()

    assert self.i2c_device.read() == 'value'

  def test_read_data(self):
    self.sm_bus_mock \
      .should_receive('read_byte_data') \
      .with_args(DEVICE_ADDRESS, COMMAND) \
      .and_return('value') \
      .once()

    assert self.i2c_device.read_data(COMMAND) == 'value'

  def test_read_block_data(self):
    self.sm_bus_mock \
      .should_receive('read_block_data') \
      .with_args(DEVICE_ADDRESS, COMMAND) \
      .and_return('value') \
      .once()

    assert self.i2c_device.read_block_data(COMMAND) == 'value'

class I2CDeviceWriteTests(I2CDeviceHelpers):
  setup = pytest.fixture(autouse = True)(lambda self: self.setup_i2c_device())

  def test_write_cmd(self):
    self.sm_bus_mock \
      .should_receive('write_byte') \
      .with_args(DEVICE_ADDRESS, COMMAND) \
      .once()
    flexmock(time).should_receive('time').with_args(.0001)

    self.i2c_device.write_cmd(COMMAND)

  def test_write_cmd_arg(self):
    self.sm_bus_mock \
      .should_receive('write_byte_data') \
      .with_args(DEVICE_ADDRESS, COMMAND, DATA) \
      .once()
    flexmock(time).should_receive('time').with_args(.0001)

    self.i2c_device.write_cmd_arg(COMMAND, DATA)

  def test_write_block_data(self):
    self.sm_bus_mock \
      .should_receive('write_block_data') \
      .with_args(DEVICE_ADDRESS, COMMAND, DATA) \
      .once()
    flexmock(time).should_receive('time').with_args(.0001)

    self.i2c_device.write_block_data(COMMAND, DATA)
