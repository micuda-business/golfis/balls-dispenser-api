import pytest, flexmock

from unit.hw.fixtures import mock_rpi_gpio

@pytest.fixture
def test_device_factory(mock_rpi_gpio):
  from hwberry.utils.gpio import GPIODevice, requires_gpio_setup

  class TestDevice(GPIODevice):
    @requires_gpio_setup
    def do_something(self): pass

  return TestDevice

@pytest.fixture
def incompatible_device_factory(mock_rpi_gpio):
  from hwberry.utils.gpio import requires_gpio_setup

  class IncompatibleDevice:
    @requires_gpio_setup
    def do_something(self): pass

  return IncompatibleDevice

class GPIOTestHelpers:
  def setup_gpio_test_helpers(self, mock_rpi_gpio, test_device_factory,
                              incompatible_device_factory):
    self.device_with_ports    = test_device_factory()
    self.device_without_ports = test_device_factory()
    self.incompatible_device  = incompatible_device_factory()

    self.device_with_ports.register_input_port(1)
    self.device_with_ports.register_output_port(2)

class RequiresSetupDecoratorTests(GPIOTestHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, mock_rpi_gpio, test_device_factory, incompatible_device_factory):
    self.setup_gpio_test_helpers(mock_rpi_gpio, test_device_factory, incompatible_device_factory)

  def test_raise_if_first_arg_of_decorated_obj_has_not_attribute_named_is_gpio_initialized(self):
    from hwberry.utils.gpio import InvalidDeviceError

    device_name = self.incompatible_device.__class__.__name__

    with pytest.raises(InvalidDeviceError, match = f'{device_name}'):
      self.incompatible_device.do_something()

  def test_raises_if_device_was_not_set_up(self):
    from hwberry.utils.gpio import GPIOSetupRequired

    operation_name = self.device_without_ports.do_something.__name__

    with pytest.raises(GPIOSetupRequired, match = f'{operation_name}'):
      self.device_without_ports.do_something()

class GPIODeviceTests(GPIOTestHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, mock_rpi_gpio, test_device_factory, incompatible_device_factory):
    self.setup_gpio_test_helpers(mock_rpi_gpio, test_device_factory, incompatible_device_factory)

  def test_register_input_port(self):
    from hwberry.utils.gpio import PortDirection, Port

    input_port = self.device_with_ports.register_input_port(gpio = 20)

    assert isinstance(input_port, Port)
    assert input_port.gpio      == 20
    assert input_port.direction == PortDirection.IN

  def test_register_output_port(self):
    from hwberry.utils.gpio import PortDirection, Port

    output_port = self.device_with_ports.register_output_port(gpio = 20)

    assert isinstance(output_port, Port)
    assert output_port.gpio      == 20
    assert output_port.direction == PortDirection.OUT

  @pytest.mark.parametrize(
    'use_context_manager', [
      pytest.param(True,  id = 'use_context_manager'),
      pytest.param(False, id = 'setup_and_cleanup_manually')
    ])
  @pytest.mark.parametrize('device_attr', ['device_with_ports', 'device_without_ports'])
  def test_gpio_setup_and_gpio_cleanup(self, device_attr, use_context_manager):
    from RPi import GPIO

    device = getattr(self, device_attr)

    for port in device._ports:
      flexmock(GPIO).should_receive('setup').with_args(port.gpio, port.direction.value).once()

    flexmock(GPIO) \
      .should_receive('cleanup') \
      .with_args([port.gpio for port in device._ports]) \
      .once()

    if use_context_manager:
      assert device._is_gpio_initialized is False

      with device:
        assert device._is_gpio_initialized is True

      assert device._is_gpio_initialized is False

    else:
      assert device._is_gpio_initialized is False

      device.gpio_setup()

      assert device._is_gpio_initialized is True

      device.gpio_cleanup()

      assert device._is_gpio_initialized is False
