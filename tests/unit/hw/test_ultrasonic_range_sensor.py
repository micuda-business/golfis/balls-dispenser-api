import threading, time
import pytest, flexmock

from unit.hw.fixtures import mock_rpi_gpio

ECHO_PIN    = 15
TRIGGER_PIN = 4

class SignalTransmissionSimulator(threading.Thread):
  def __init__(self, rpi_gpio_mock, *, set_echo_to_high_after, set_echo_to_low_after):
    super().__init__()

    self._rpi_gpio_mock          = rpi_gpio_mock
    self._set_echo_to_high_after = set_echo_to_high_after
    self._set_echo_to_low_after  = set_echo_to_low_after

  def run(self):
    time.sleep(self._set_echo_to_high_after)
    self._set_echo_to_high()

    time.sleep(self._set_echo_to_low_after)
    self._set_echo_to_low()

  def _set_echo_to_high(self):
    flexmock(self._rpi_gpio_mock, input = lambda gpio: 1)

  def _set_echo_to_low(self):
    flexmock(self._rpi_gpio_mock, input = lambda gpio: 0)

class UltrasonicRangeSensorTests:
  @pytest.fixture(autouse = True)
  def setup(self, mock_rpi_gpio):
    from hwberry.ultrasonic_range_sensor import UltrasonicRangeSensor

    self.range_sensor   = UltrasonicRangeSensor(TRIGGER_PIN, ECHO_PIN)
    self._rpi_gpio_mock = mock_rpi_gpio

  def test_measure_distance(self):
    signal_transmission_simulator = SignalTransmissionSimulator(self._rpi_gpio_mock,
                                                                set_echo_to_high_after = 0.01,
                                                                set_echo_to_low_after  = 1)

    flexmock(self.range_sensor._trigger_port) \
      .should_receive('high_for') \
      .replace_with(lambda sec: signal_transmission_simulator.start())

    with self.range_sensor as range_sensor:
      actual = range_sensor.measure_distance()

    expected     = 17263
    delta        = expected * 0.05
    expected_min = expected - delta
    expected_max = expected + delta

    assert expected_min <= actual <= expected_max
