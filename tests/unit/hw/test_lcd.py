import time, pytest, flexmock

from hwberry.lcd   import LCD
from hwberry.utils import I2CDevice

LINE_WIDTH  = 20
LCD_ADDRESS = 0x3F

class LCDTests:
  @pytest.fixture(autouse = True)
  def setup(self):
    flexmock(time, sleep = lambda sec: None) # for test speed-up

    i2c_device_mock = flexmock(write_cmd = lambda cmd: None)
    flexmock(I2CDevice).new_instances(i2c_device_mock)

    self.lcd = LCD(address = LCD_ADDRESS, lines_count = 4, line_width = LINE_WIDTH)
    self.i2c_device_mock = i2c_device_mock

  i2c_device_write_cmd_mock = property(lambda self: self.i2c_device_mock.should_receive('write_cmd'))

  def test_lcd_initialization(self):
    self._assert_write_cmd_expectation_for(_input = 0x03)
    self._assert_write_cmd_expectation_for(_input = 0x03)
    self._assert_write_cmd_expectation_for(_input = 0x03)
    self._assert_write_cmd_expectation_for(_input = 0x02)

    self._assert_write_cmd_expectation_for(_input = 0x20 | 0x08 | 0x00 | 0x00)
    self._assert_write_cmd_expectation_for(_input = 0x08 | 0x04)
    self._assert_write_cmd_expectation_for(_input = 0x01)
    self._assert_write_cmd_expectation_for(_input = 0x04 | 0x02)

    LCD(address = LCD_ADDRESS, lines_count = 4, line_width = LINE_WIDTH)

  def test_print(self):
    text_lines = ['Hello,', 'my favourite user', 'too long line for display']

    self._assert_write_cmd_expectation_for(_input = 0x80)
    for char in text_lines[0]:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self._assert_write_cmd_expectation_for(_input = 0xC0)
    for char in text_lines[1]:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self._assert_write_cmd_expectation_for(_input = 0x94)
    for char in text_lines[2][:LINE_WIDTH]:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self.lcd.print(lines = text_lines, trim_text = True)

  @pytest.mark.parametrize(
    'line_no, line_no_cmd_repr', [
      pytest.param(1, 0x80, id = 'display_string_on_line_1'),
      pytest.param(2, 0xC0, id = 'display_string_on_line_2'),
      pytest.param(3, 0x94, id = 'display_string_on_line_3'),
      pytest.param(4, 0xD4, id = 'display_string_on_line_4'),
    ]
  )
  def test_display_text_no_trim_text(self, line_no, line_no_cmd_repr):
    text = 'Hello'

    self._assert_write_cmd_expectation_for(_input = line_no_cmd_repr)

    for char in text:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self.lcd.display_text(text, line_no = line_no)

  @pytest.mark.parametrize(
    'line_no, line_no_cmd_repr', [
      pytest.param(1, 0x80, id = 'display_string_on_line_1'),
      pytest.param(2, 0xC0, id = 'display_string_on_line_2'),
      pytest.param(3, 0x94, id = 'display_string_on_line_3'),
      pytest.param(4, 0xD4, id = 'display_string_on_line_4'),
    ]
  )
  def test_display_text_trim_text(self, line_no, line_no_cmd_repr):
    text = 'Hello from planet Earth'

    self._assert_write_cmd_expectation_for(_input = line_no_cmd_repr)

    for char in text[:LINE_WIDTH]:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self.lcd.display_text(text, line_no = line_no, trim_text = True)

  def test_display_text_on_invalid_line_displays_text_on_the_first_line(self):
    text = 'Hello'

    self._assert_write_cmd_expectation_for(_input = 0x80)

    for char in text:
      self._assert_write_cmd_expectation_for(_input = ord(char), mode = 1)

    self.lcd.display_text(text, line_no = 'invalid line no')

  def test_clear_display(self):
    self._assert_write_cmd_expectation_for(_input = 0x01)
    self._assert_write_cmd_expectation_for(_input = 0x02)

    self.lcd.clear_display()

  @pytest.mark.parametrize(
    'state, command', [pytest.param('on',  0x08, id = 'state_on'),
                       pytest.param('On',  0x08, id = 'state_On'),
                       pytest.param('ON',  0x08, id = 'state_ON'),
                       pytest.param(True,  0x08, id = 'state_True'),
                       pytest.param('off', 0x00, id = 'state_off'),
                       pytest.param('Off', 0x00, id = 'state_Off'),
                       pytest.param('OFF', 0x00, id = 'state_OFF'),
                       pytest.param(False, 0x00, id = 'state_False')]
  )
  def test_set_backlight(self, state, command):
    self._write_cmd_expects(command)

    self.lcd.set_backlight(state)

  def test_set_backlight_on(self):
    self._write_cmd_expects(0x08)

    self.lcd.set_backlight_on()

  def test_set_backlight_off(self):
    self._write_cmd_expects(0x00)

    self.lcd.set_backlight_off()

  def test_set_backlight_with_invalid_state_raises(self):
    with pytest.raises(LCD.InvalidBacklightState, match = '[S,s]tate "invalid state" is invalid'):
      self.lcd.set_backlight('invalid state')

  def _write_cmd_expects(self, arg):
    self.i2c_device_write_cmd_mock.with_args(arg).once().ordered()

  def _assert_write_cmd_expectation_for(self, *, _input, mode = 0):
    for arg in self._write_cmd_args_for(_input = _input, mode = mode):
      self._write_cmd_expects(arg)

  @staticmethod
  def _write_cmd_args_for(*, _input, mode = 0):
    return [mode | _input & 0xF0 | 0x08,
            mode | _input & 0xF0 | 0x04 | 0x08,
            mode | _input & 0xF0 & ~0x04 | 0x08,
            mode | (_input << 4) & 0xF0 | 0x08,
            mode | (_input << 4) & 0xF0 | 0x04 | 0x08,
            mode | (_input << 4) & 0xF0 & ~0x04 | 0x08]
