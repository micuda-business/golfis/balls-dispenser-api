def pytest_collection_modifyitems(session, config, items):
  """
  Because of mocked `new_instances` are not properly cleaned by teardown
  (issue https://github.com/bkabrda/flexmock/issues/16,
  it's a Python bug, see https://bugs.python.org/issue25731)
  we have to put tests in `test_i2c_device` before `test_lcd` tests,
  that use `new_instances` feature.
  """
  items.sort(key = lambda i: str(i.fspath).endswith('test_lcd.py'))
