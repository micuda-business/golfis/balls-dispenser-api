import string
from datetime               import datetime
from dateutil.relativedelta import relativedelta

import factory, factory.fuzzy

from golferis.config     import settings
from golferis.schemas    import player, card, user, credit, balls_height, balls_dispenser
from golferis.db.session import SessionLocal

class PlayerCreateDictFactory(factory.DictFactory):
  golfer_id    = None
  is_active    = True
  first_name   = factory.Faker('first_name')
  last_name    = factory.Faker('last_name')
  phone_number = factory.Faker('phone_number', locale = 'cs_CZ')
  created_at   = None

  @factory.lazy_attribute_sequence
  def email(self, n):
    if   self.first_name is None and self.last_name is None: email_part = f'{n}'
    elif self.first_name is None:                            email_part = self.last_name.lower()
    elif self.last_name  is None:                            email_part = self.first_name.lower()
    else:
      email_part = f'{self.first_name.lower()}.{self.last_name.lower()}'

    return f'{email_part}@example.com'

class CardCreateFactory(factory.Factory):
  class Meta:
    model = card.CardCreate

  owner_id    = None
  holder_id   = factory.SelfAttribute('owner_id')
  is_active   = True
  card_number = factory.fuzzy.FuzzyText(length = 10, chars = string.digits)

class UserCreateFactory(factory.Factory):
  class Meta:
    model = user.UserCreate

  email                  = factory.Faker('safe_email')
  password               = factory.fuzzy.FuzzyText()
  phone_number           = factory.Faker('phone_number', locale = 'cs_CZ')
  is_active              = True
  is_golf_course_manager = False
  is_superuser           = False

class PurchaseCreditFactory(factory.Factory):
  class Meta:
    model = credit.PurchaseCredit

  player_id     = None
  card_id       = None
  credits_count = 100
  notes         = None
  price         = 3500
  expiry_at     = factory.LazyFunction(lambda: datetime.now() + relativedelta(years = 1))

class SpendCreditFactory(factory.Factory):
  class Meta:
    model = credit.SpendCredit

  player_id     = None
  credits_count = 100
  notes         = None

class BallsHeightLogCreateFactory(factory.Factory):
  class Meta:
    model = balls_height.BallsHeightLogCreate

  measured_height   = factory.fuzzy.FuzzyInteger(low = 0, high = 100)
  notification_made = False
  notes             = None

class BallsHeightConfigCreateFactory(factory.Factory):
  class Meta:
    model = balls_height.BallsHeightConfigCreate

  notify_at_height = settings.BALLS_HEIGHT_NOTIFY_AT_HEIGHT_DEFAULT
  otification_text = settings.BALLS_HEIGHT_NOTIFICATION_TEXT_DEFAULT

class BallsDispenserConfigCreateFactory(factory.Factory):
  class Meta:
    model = balls_dispenser.BallsDispenserConfigCreate

  no_credit_text             = settings.BALLS_DISPENSER_NO_CREDIT_TEXT_DEFAULT
  unknown_player_text        = settings.BALLS_DISPENSER_UNKNOWN_PLAYER_TEXT_DEFAULT
  notify_at_credit_amount    = settings.BALLS_DISPENSER_NOTIFY_AT_CREDIT_AMOUNT
  current_credit_amount_text = settings.BALLS_DISPENSER_CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT
