import pytest, flexmock
from micuda.utils import Success, Failure

from golferis                        import crud
from golferis.services.sms_service   import SMSService
from golferis.services.balls_service import BallsService

from unit.hw.fixtures import mock_rpi_gpio

@pytest.mark.skip(reason = 'Skip for now. Somehow breaks the `test_ultrasonic_range_sensor`')
@pytest.mark.parametrize(
  'send_message_to_result', [
    pytest.param(Success(),                           id = 'sending_sms_succeeded'),
    pytest.param(Failure(detail = 'Cannot send SMS'), id = 'sending_sms_failed'),
  ]
)
@pytest.mark.parametrize(
  'balls_height', [
    pytest.param(99,  id = 'balls_height_is_lower_than_the_config_value'),
    pytest.param(100, id = 'balls_height_is_equal_to_the_config_value'),
    pytest.param(101, id = 'balls_height_is_higher_than_the_config_value')
  ]
)
def test_check_balls_height(
  mock_rpi_gpio, db, golf_course_manager_factory,
  balls_height_config_factory, balls_height, send_message_to_result
):
  from hwberry.ultrasonic_range_sensor import UltrasonicRangeSensor

  from golferis.scripts import check_balls_height

  sms_service         = SMSService()
  manager             = golf_course_manager_factory(is_active = True)
  balls_height_config = balls_height_config_factory(notify_at_height  = 100,
                                                    notification_text = 'Dochazeji micky')

  flexmock(UltrasonicRangeSensor) \
    .should_receive('measure_distance') \
    .and_return(balls_height)
  send_message_to_mock = flexmock(sms_service) \
    .should_receive('send_message_to') \
    .with_args(*[manager.phone_number], message = 'Dochazeji micky') \
    .and_return(send_message_to_result)

  if balls_height >= balls_height_config.notify_at_height:
    send_message_to_mock.once()
    expected_notification_made = send_message_to_result.is_success
  else:
    send_message_to_mock.never()
    expected_notification_made = False

  check_balls_height.run(trigger_pin   = 4,
                         echo_pin      = 15,
                         balls_service = BallsService(db, sms_service = sms_service))

  balls_height_log = crud.balls_height_log.get_multi(db, limit = 1)[0]

  actual_measured_height   = balls_height_log.measured_height
  actual_notification_made = balls_height_log.notification_made

  assert actual_notification_made is expected_notification_made
  assert actual_measured_height   == balls_height
