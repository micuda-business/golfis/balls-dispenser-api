from unittest.mock import Mock
from functools     import partial

import pytest, flexmock
from micuda.utils import Success, Failure

from golferis                                  import crud
from golferis.services.sms_service             import SMSService
from golferis.services.balls_dispenser_service import BallsDispenserService

from factories import PurchaseCreditFactory

unknown_card_number = pytest.fixture(lambda : 10 * '0')

@pytest.fixture
def player_factory(player_factory):
  return partial(player_factory, first_name = 'Tom', last_name  = 'Vrbicky')

@pytest.fixture
def activated_player(player_factory, card_factory):
  player = player_factory(is_active = True)

  player.activated_card   = card_factory(owner_id = player.id, is_active = True)
  player.deactivated_card = card_factory(owner_id = player.id, is_active = False)

  return player

@pytest.fixture
def deactivated_player(player_factory, card_factory):
  player = player_factory(is_active = False)

  player.card = card_factory(owner_id = player.id, is_active = True)

  return player

class BallsDispenserServiceTests:
  @pytest.fixture(autouse = True)
  def setup(self, db, balls_dispenser_config_factory):
    self.db                      = db
    self.balls_dispenser_service = BallsDispenserService(self.db, logger = Mock())

    balls_dispenser_config_factory(
      no_credit_text                  = '{{ last_name }} {{ first_name }},\nnemáte již kredit',
      unknown_player_text             = 'Neznámá identifikace,\nkontaktujte recepci',
      current_credit_amount_text      = '{{ last_name }} {{ first_name }},\nZbývá kreditů: {{ credits_left }}',
      player_credit_notification_text = 'Dobrý den, {{ last_name }} {{ first_name }}, Váš aktuální počet kreditů: {{ credits_left }}',
      notify_at_credit_amount = [3, 1, 0]
    )

  def add_credit_to_player(self, player, *, credits_count):
    purchase_credit = PurchaseCreditFactory(player_id = player.id, credits_count = credits_count)

    return crud.player.purchase_credit(self.db, obj_in = purchase_credit)

  def assert_balls_dispenser_log_entry(self, *, expected_card_id, expected_card_number):
    db_balls_dispenser_log = crud.balls_dispenser_log.get_multi(self.db, limit = 1)[0]

    assert db_balls_dispenser_log.card_id     == expected_card_id
    assert db_balls_dispenser_log.card_number == expected_card_number

  def test_dispense_balls(self, activated_player):
    flexmock(SMSService).should_receive('send_message_to').and_return(Success()).never()

    self.add_credit_to_player(activated_player, credits_count = 10)

    card = activated_player.activated_card

    result = self.balls_dispenser_service.dispense_balls(card.card_number)

    assert result.is_success
    assert result.detail == 'Vrbicky Tom,\nZbývá kreditů: 9'

    self.assert_balls_dispenser_log_entry(expected_card_id     = card.id,
                                          expected_card_number = card.card_number)

  @pytest.mark.parametrize(
    'send_message_to_result', [
      pytest.param(Success(),                         id = 'sending_sms_succeeded'),
      pytest.param(Failure(detail = 'SMS API error'), id = 'sending_sms_failed'),
    ]
  )
  def test_dispense_balls_should_notify_player(self, activated_player, send_message_to_result):
    flexmock(SMSService) \
      .should_receive('send_message_to') \
      .with_args(activated_player.phone_number,
                 message = 'Dobrý den, Vrbicky Tom, Váš aktuální počet kreditů: 0') \
      .and_return(send_message_to_result) \
      .once()

    # if send_message_to_result.is_failure:
      # TODO: assert logging

    self.add_credit_to_player(activated_player, credits_count = 1)

    card = activated_player.activated_card

    result = self.balls_dispenser_service.dispense_balls(card.card_number)

    assert result.is_success
    assert result.detail == 'Vrbicky Tom,\nZbývá kreditů: 0'

    self.assert_balls_dispenser_log_entry(expected_card_id     = card.id,
                                          expected_card_number = card.card_number)

  def test_dispense_balls_player_has_no_credit(self, activated_player):
    card = activated_player.activated_card

    result = self.balls_dispenser_service.dispense_balls(card.card_number)

    assert result.is_failure
    assert result.detail == 'Vrbicky Tom,\nnemáte již kredit'

    self.assert_balls_dispenser_log_entry(expected_card_id     = card.id,
                                          expected_card_number = card.card_number)

  def test_dispense_balls_deactivated_player(self, deactivated_player):
    card = deactivated_player.card

    result = self.balls_dispenser_service.dispense_balls(card.card_number)

    assert result.is_failure
    assert result.detail == 'Neznámá identifikace,\nkontaktujte recepci'

    self.assert_balls_dispenser_log_entry(expected_card_id     = card.id,
                                          expected_card_number = card.card_number)

  def test_dispense_balls_deactivated_card(self, activated_player):
    card = activated_player.deactivated_card

    result = self.balls_dispenser_service.dispense_balls(card.card_number)

    assert result.is_failure
    assert result.detail == 'Neznámá identifikace,\nkontaktujte recepci'

    self.assert_balls_dispenser_log_entry(expected_card_id     = None,
                                          expected_card_number = card.card_number)

  def test_dispense_balls_unknown_card(self, unknown_card_number):
    result = self.balls_dispenser_service.dispense_balls(unknown_card_number)

    assert result.is_failure
    assert result.detail == 'Neznámá identifikace,\nkontaktujte recepci'

    self.assert_balls_dispenser_log_entry(expected_card_id     = None,
                                          expected_card_number = unknown_card_number)
