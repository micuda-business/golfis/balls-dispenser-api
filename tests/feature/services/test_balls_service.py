from unittest.mock import Mock
from datetime      import datetime

import pytest, flexmock
from freezegun import freeze_time

from micuda.utils import Success, Failure

from golferis                        import crud
from golferis.services.sms_service   import SMSService
from golferis.services.balls_service import BallsService

current_datetime = datetime.now()

class ReportBallsHeightTests:
  @pytest.fixture(autouse = True)
  def setup(self, db, golf_course_manager_factory, balls_height_config_factory):
    self.balls_service = BallsService(db, logger = Mock())

    self.manager_1                    = golf_course_manager_factory(is_active = True)
    self.manager_2                    = golf_course_manager_factory(is_active = True)
    self.inactive_manager             = golf_course_manager_factory(is_active = False)
    self.manager_without_phone_number = golf_course_manager_factory(phone_number = None)

    balls_height_config_factory(
      notify_at_height  = 100,
      notification_text = '{{ current_datetime }} - počet míčů v golfovém vydavači klesl pod stanovenou hodnotu.'
    )

  def test_report_balls_height_for_height_lower_than_value_in_config(self):
    flexmock(SMSService).should_receive('send_message_to').never()

    balls_height_log = self.balls_service.report_balls_height(balls_height = 99)

    assert balls_height_log.notification_made is False
    assert balls_height_log.notes             is None
    assert balls_height_log.measured_height   == 99

  @freeze_time(current_datetime)
  @pytest.mark.parametrize(
    'send_message_to_result', [
      pytest.param(Success(),                         id = 'sending_sms_succeeded'),
      pytest.param(Failure(detail = 'SMS API error'), id = 'sending_sms_failed'),
    ]
  )
  @pytest.mark.parametrize(
    'balls_height', [
      pytest.param(100, id = 'balls_height_is_equal_to_the_config_value'),
      pytest.param(101, id = 'balls_height_is_higher_than_the_config_value')
    ]
  )
  def test_report_balls_height_for_height_higher_or_equal_with_the_value_in_config(
    self, balls_height, send_message_to_result
  ):
    active_managers_phone_numbers = \
      sorted([self.manager_1.phone_number, self.manager_2.phone_number])

    expected_notification_text = (
      f'{current_datetime.strftime("%d.%m.%Y %H:%M:%S")} - počet míčů v golfovém '
      'vydavači klesl pod stanovenou hodnotu.'
    )

    flexmock(SMSService) \
      .should_receive('send_message_to') \
      .with_args(*active_managers_phone_numbers, message = expected_notification_text) \
      .and_return(send_message_to_result) \
      .once()

    balls_height_log = self.balls_service.report_balls_height(balls_height = balls_height)

    actual_balls_height_log_note = balls_height_log.notes

    if send_message_to_result.is_success:
      expected_balls_height_log_note = \
        f'Message: {expected_notification_text}\nSent to: {", ".join(active_managers_phone_numbers)}'
    else:
      expected_balls_height_log_note = send_message_to_result.detail

    assert balls_height_log.notification_made is send_message_to_result.is_success
    assert balls_height_log.measured_height   == balls_height
    assert actual_balls_height_log_note       == expected_balls_height_log_note
