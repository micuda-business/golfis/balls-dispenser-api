from uuid      import UUID
from functools import partial

import pytest, flexmock
from fastapi.testclient import TestClient

from golferis            import crud
from golferis.api        import deps
from golferis.api.main   import app
from golferis.db.models  import ModelBase
from golferis.db.session import engine, session_provider

from helpers   import AuthHelpers
from factories import (
  PlayerCreateDictFactory, CardCreateFactory, UserCreateFactory, BallsHeightLogCreateFactory,
  BallsHeightConfigCreateFactory, BallsDispenserConfigCreateFactory
)

@pytest.yield_fixture(scope = 'session')
def setup_db():
  ModelBase.metadata.create_all(bind = engine)
  yield
  ModelBase.metadata.drop_all(bind = engine)

@pytest.yield_fixture
def db(setup_db):
  try:
    connection  = engine.connect()
    transaction = connection.begin()

    with session_provider(bind = connection) as session:
      app.dependency_overrides[deps.get_db] = lambda: session

      yield session

  finally:
    transaction.rollback()
    connection.close()

@pytest.fixture
def api_client():
  with TestClient(app) as c:
    yield c

@pytest.fixture
def auth_helpers(api_client):
  return AuthHelpers(api_client)

@pytest.fixture
def user_factory(db):
  def factory(**kwargs):
    user_create = UserCreateFactory(**kwargs)

    return crud.user.create(db, obj_in = user_create)
  return factory

@pytest.fixture
def golf_course_manager_factory(user_factory):
  return partial(user_factory, is_golf_course_manager = True)

user_password      = pytest.fixture(lambda: 'password')
superuser_password = pytest.fixture(lambda: 'password')
fake_uuid          = pytest.fixture(lambda: UUID('ffffffff-ffff-ffff-ffff-ffffffffffff'))

@pytest.fixture
def user(user_factory, user_password):
  return user_factory(password = user_password, is_superuser = False)

@pytest.fixture
def superuser(user_factory, superuser_password):
  return user_factory(password = superuser_password, is_superuser = True)

@pytest.fixture
def player_factory(db):
  def factory(**kwargs):
    player_create = PlayerCreateDictFactory(**kwargs)

    return crud.player.create(db, obj_in = player_create)
  return factory

@pytest.fixture
def card_factory(db):
  def factory(**kwargs):
    card_create = CardCreateFactory(**kwargs)

    return crud.card.create(db, obj_in = card_create)
  return factory

activated_player   = pytest.fixture(lambda player_factory: player_factory(is_active = True))
deactivated_player = pytest.fixture(lambda player_factory: player_factory(is_active = False))
inexistent_player  = pytest.fixture(lambda fake_uuid:      flexmock(id = fake_uuid))
inexistent_card    = pytest.fixture(lambda fake_uuid:      flexmock(id = fake_uuid))

@pytest.fixture
def balls_height_log_factory(db):
  def factory(**kwargs):
    balls_height_log_create = BallsHeightLogCreateFactory(**kwargs)

    return crud.balls_height_log.create(db, obj_in = balls_height_log_create)
  return factory

@pytest.fixture
def balls_height_config_factory(db):
  def factory(**kwargs):
    balls_height_config_create = BallsHeightConfigCreateFactory(**kwargs)

    return crud.balls_height_config.create(db, obj_in = balls_height_config_create)
  return factory

@pytest.fixture
def balls_dispenser_config_factory(db):
  def factory(**kwargs):
    balls_dispenser_config_create = BallsDispenserConfigCreateFactory(**kwargs)

    return crud.balls_dispenser_config.create(db, obj_in = balls_dispenser_config_create)
  return factory
