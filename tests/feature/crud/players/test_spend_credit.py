from datetime               import datetime
from dateutil.relativedelta import relativedelta

import pytest
from freezegun import freeze_time

from golferis         import crud
from golferis.schemas import SpendCredit

from helpers   import add_player_debt
from factories import PurchaseCreditFactory

pytestmark = pytest.mark.usefixtures('db')

class SpendCreditTests:
  @pytest.fixture(autouse = True)
  def setup(self, db, activated_player, card_factory):
    self.db               = db
    self.activated_player = activated_player
    self.player_card      = card_factory(owner_id = self.activated_player.id, is_active = True)

  def do_spend_credit(self, *, player = None, credits_count = 1, card = None, notes = None):
    spend_credit = SpendCredit(
      player_id     = (player or self.activated_player).id,
      card_id       = (card   or self.player_card).id,
      credits_count = credits_count,
      notes         = notes
    )

    return crud.player.spend_credit(self.db, obj_in = spend_credit)

  def add_credit_to_player(self, *, player = None, credits_count = 1,
                           expiry_at = None, created_at = None):
    purchase_credit = PurchaseCreditFactory(
      player_id     = (player or self.activated_player).id,
      credits_count = credits_count,
      expiry_at     = expiry_at
    )

    player_credit = crud.player.purchase_credit(self.db, obj_in = purchase_credit)

    if created_at is not None:
      player_credit.updated_at = created_at
      player_credit.created_at = created_at

      self.db.add(player_credit)
      self.db.commit()

    return player_credit

  def add_player_debt(self, *, credits_count, player = None):
    return add_player_debt(self.db,
                           credits_count = credits_count,
                           player        = player or self.activated_player)

  def test_spend_credit(self):
    player_credit = self.add_credit_to_player(credits_count = 3)

    credit_transtaction = self.do_spend_credit(credits_count = 1)

    self.db.refresh(player_credit)

    assert player_credit.credits_spend       == 1
    assert player_credit.credits_left        == 2
    assert credit_transtaction.player_id     == self.activated_player.id
    assert credit_transtaction.credits_count == 1
    assert credit_transtaction.notes         is None

  def test_spend_credit_subtracts_credits_from_the_oldest_players_credit(self):
    now    = datetime.now()
    future = now + relativedelta(years = 1)

    player_credit_with_expiration = \
      self.add_credit_to_player(credits_count = 3, expiry_at = future, created_at = now)
    player_credit_without_expiration = \
      self.add_credit_to_player(credits_count = 3, created_at = now + relativedelta(minutes = 1))

    self.do_spend_credit()

    with freeze_time(future + relativedelta(minutes = 1)):
      self.do_spend_credit()
      self.do_spend_credit()

    self.db.refresh(player_credit_with_expiration)
    self.db.refresh(player_credit_without_expiration)

    assert player_credit_with_expiration.credits_spend == 1
    assert player_credit_with_expiration.credits_left  == 2

    assert player_credit_without_expiration.credits_spend == 2
    assert player_credit_without_expiration.credits_left  == 1

    assert len(self.activated_player.credit_transactions) == 5

  def test_spend_credit_player_has_no_credit(self):
    with pytest.raises(crud.player.PlayerHasNoCredit):
      self.do_spend_credit()

    self.add_credit_to_player(credits_count = 2)

    self.do_spend_credit()
    self.do_spend_credit()

    with pytest.raises(crud.player.PlayerHasNoCredit):
      self.do_spend_credit()

  def test_spend_credit_player_in_debt(self):
    with pytest.raises(crud.player.PlayerHasNoCredit):
      self.do_spend_credit()

    self.add_player_debt(credits_count = 2)

    with pytest.raises(crud.player.PlayerHasNoCredit):
      self.do_spend_credit()

  def test_spend_credit_inexistent_player(self, inexistent_player):
    with pytest.raises(crud.player.PlayerNotFound):
      self.do_spend_credit(player = inexistent_player)
