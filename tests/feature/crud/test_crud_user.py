import pytest

from golferis import crud

def test_get_active_managers_phone_numbers(db, golf_course_manager_factory, superuser):
  active_manager_1                    = golf_course_manager_factory(is_active    = True)
  active_manager_2                    = golf_course_manager_factory(is_active    = True)
  active_manager_without_phone_number = golf_course_manager_factory(phone_number = None)
  inactive_manager                    = golf_course_manager_factory(is_active    = False)
  active_superuser_and_manager        = golf_course_manager_factory(is_superuser = True)

  actual   = sorted(crud.user.get_active_managers_phone_numbers(db))
  expected = sorted([u.phone_number
                     for u
                     in  [active_manager_1, active_manager_2, active_superuser_and_manager]])

  assert actual == expected
