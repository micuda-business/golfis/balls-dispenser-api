import pytest
from fastapi.encoders import jsonable_encoder

from golferis          import crud
from golferis.schemas  import BallsDispenserConfig, BallsDispenserConfigUpdate
from golferis.api.core import settings

from helpers   import APITestsSetup
from factories import BallsDispenserConfigCreateFactory

NO_CREDIT_TEXT_DEFAULT                  = 'Nemate kredit'
UNKNOWN_PLAYER_TEXT_DEFAULT             = 'Neznamy hrac'
NOTIFY_AT_CREDIT_AMOUNT_DEFAULT         = [3, 1, 0]
CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT      = 'Stavajici pocet kreditu: '
PLAYER_CREDIT_NOTIFICATION_TEXT_DEFAULT = 'Vas aktualni pocet kreditu: '

class BallsDispenserConfigTestsHelpers(APITestsSetup):
  def do_create_balls_dispenser_config(
    self, *,
    no_credit_text                  = NO_CREDIT_TEXT_DEFAULT,
    unknown_player_text             = UNKNOWN_PLAYER_TEXT_DEFAULT,
    notify_at_credit_amount         = NOTIFY_AT_CREDIT_AMOUNT_DEFAULT,
    current_credit_amount_text      = CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT,
    player_credit_notification_text = PLAYER_CREDIT_NOTIFICATION_TEXT_DEFAULT,
    token                           = None
  ):
    token                         = token or self.superuser_token
    balls_dispenser_config_create = BallsDispenserConfigCreateFactory(
      no_credit_text                  = no_credit_text,
      unknown_player_text             = unknown_player_text,
      notify_at_credit_amount         = notify_at_credit_amount,
      current_credit_amount_text      = current_credit_amount_text,
      player_credit_notification_text = player_credit_notification_text
    )

    return self.api_client.post(f'{settings.API_V1_STR}/balls-dispenser/config',
                                json    = jsonable_encoder(balls_dispenser_config_create),
                                headers = self.auth_helpers.auth_header(token))

class ReadBallsDispenserConfigTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password,
            balls_dispenser_config_factory):
    self.db_balls_dispenser_config = balls_dispenser_config_factory(
      no_credit_text                  = NO_CREDIT_TEXT_DEFAULT,
      unknown_player_text             = UNKNOWN_PLAYER_TEXT_DEFAULT,
      notify_at_credit_amount         = NOTIFY_AT_CREDIT_AMOUNT_DEFAULT,
      current_credit_amount_text      = CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT,
      player_credit_notification_text = PLAYER_CREDIT_NOTIFICATION_TEXT_DEFAULT
    )

    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_balls_dispenser_config(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/balls-dispenser/config',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_balls_dispenser_config(self):
    response = self.do_read_balls_dispenser_config()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder(BallsDispenserConfig.from_orm(self.db_balls_dispenser_config))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_balls_dispenser_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_balls_dispenser_config(token = user_token)

    assert response.status_code == 403

class CreateBallsDispenserConfigTests(BallsDispenserConfigTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def test_create_balls_dispenser_config(self, db):
    response = self.do_create_balls_dispenser_config()

    actual_response_data      = response.json()
    db_balls_dispenser_config = crud.balls_dispenser_config.get(db, actual_response_data['id'])
    expected_response_data    = \
      jsonable_encoder(BallsDispenserConfig.from_orm(db_balls_dispenser_config))

    assert response.status_code == 201
    assert actual_response_data == expected_response_data

  def test_create_balls_dispenser_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_create_balls_dispenser_config(token = user_token)

    assert response.status_code == 403

  def test_create_balls_dispenser_config_fail_if_config_record_already_exists(self):
    self.do_create_balls_dispenser_config()

    response = self.do_create_balls_dispenser_config()

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Config for balls dispenser script already exists' }

    assert response.status_code == 400
    assert actual_response_data == expected_response_data

class UpdateBallsDispenserConfigTests(BallsDispenserConfigTestsHelpers):
  # TODO note that this is full entity update
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_update_balls_dispenser_config(self, *,
                                       no_credit_text                  = None,
                                       unknown_player_text             = None,
                                       notify_at_credit_amount         = None,
                                       current_credit_amount_text      = None,
                                       player_credit_notification_text = None,
                                       token                           = None):
    token = token or self.superuser_token

    params = {}
    if current_credit_amount_text is not None:
      params['current_credit_amount_text'] = current_credit_amount_text
    if no_credit_text is not None:
      params['no_credit_text'] = no_credit_text
    if unknown_player_text is not None:
      params['unknown_player_text'] = unknown_player_text
    if notify_at_credit_amount is not None:
      params['notify_at_credit_amount'] = notify_at_credit_amount
    if player_credit_notification_text is not None:
      params['player_credit_notification_text'] = player_credit_notification_text

    balls_dispenser_config_update = BallsDispenserConfigUpdate(**params)

    return self.api_client.put(f'{settings.API_V1_STR}/balls-dispenser/config',
                               json    = jsonable_encoder(balls_dispenser_config_update),
                               headers = self.auth_helpers.auth_header(token))

  def test_update_balls_dispenser_config(self, db):
    create_response = self.do_create_balls_dispenser_config()

    create_response_data      = create_response.json()
    db_balls_dispenser_config = crud.balls_dispenser_config.get(db, create_response_data['id'])

    assert db_balls_dispenser_config.no_credit_text                  == 'Nemate kredit'
    assert db_balls_dispenser_config.unknown_player_text             == 'Neznamy hrac'
    assert db_balls_dispenser_config.notify_at_credit_amount         == [3, 1, 0]
    assert db_balls_dispenser_config.current_credit_amount_text      == 'Stavajici pocet kreditu: '
    assert db_balls_dispenser_config.player_credit_notification_text == 'Vas aktualni pocet kreditu: '

    response = self.do_update_balls_dispenser_config(
      no_credit_text                  = 'No credit left',
      unknown_player_text             = 'Unknown player',
      notify_at_credit_amount         = [5],
      current_credit_amount_text      = 'Credits left: ',
      player_credit_notification_text = 'Your current credit status: '
    )

    actual_response_data              = response.json()
    db_updated_balls_dispenser_config = \
      crud.balls_dispenser_config.get(db, actual_response_data['id'])
    expected_response_data            = \
      jsonable_encoder(BallsDispenserConfig.from_orm(db_updated_balls_dispenser_config))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

    assert db_balls_dispenser_config.no_credit_text                  == 'No credit left'
    assert db_balls_dispenser_config.unknown_player_text             == 'Unknown player'
    assert db_balls_dispenser_config.notify_at_credit_amount         == [5]
    assert db_balls_dispenser_config.current_credit_amount_text      == 'Credits left: '
    assert db_balls_dispenser_config.player_credit_notification_text == 'Your current credit status: '

  def test_update_balls_dispenser_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_update_balls_dispenser_config(token = user_token)

    assert response.status_code == 403

  def test_update_balls_dispenser_config_if_config_does_not_exist(self):
    response = self.do_update_balls_dispenser_config()

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Config for balls dispenser script not found' }

    assert response.status_code == 404
    assert actual_response_data == expected_response_data
