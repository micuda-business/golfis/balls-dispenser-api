import pytest, flexmock
from pydash import py_
from fastapi.encoders import jsonable_encoder

from golferis              import crud
from golferis.api.core     import settings
from golferis.schemas.user import User, UserCreate, UserUpdate, UserPage

from helpers   import APITestsSetup
from factories import UserCreateFactory

pytestmark = pytest.mark.usefixtures('db')

inexistent_user = pytest.fixture(lambda fake_uuid: flexmock(id = fake_uuid))

class CreateUserTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_create_user(self, *, user_create = None, token = None):
    token       = token       or self.superuser_token
    user_create = user_create or UserCreateFactory()

    return self.api_client.post(f'{settings.API_V1_STR}/users/',
                                json    = jsonable_encoder(user_create),
                                headers = self.auth_helpers.auth_header(token))

  def test_create_user(self, db):
    response = self.do_create_user()

    actual_response_data   = response.json()
    db_user                = crud.user.get(db, actual_response_data['id'])
    expected_response_data = jsonable_encoder(User.from_orm(db_user))

    assert response.status_code == 201
    assert actual_response_data == expected_response_data

  def test_create_user_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_create_user(token = user_token)

    assert response.status_code == 403

  def test_create_user_existing_email(self, user_factory):
    db_user     = user_factory()
    user_create = UserCreate(**User.from_orm(db_user).dict(), password = 'password')

    response = self.do_create_user(user_create = user_create)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Email is already registered' }

class ReadUsersTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_users(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/users/',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_users(self, user_factory):
    response = self.do_read_users()

    assert response.status_code == 200
    assert response.json()      == \
      jsonable_encoder(UserPage(count = 1, result = [jsonable_encoder(User.from_orm(self.superuser))]))

    db_user = user_factory()

    response = self.do_read_users()

    users                  = sorted([db_user, self.superuser], key = lambda u: u.id)
    actual_response_data   = response.json()
    expected_response_data = jsonable_encoder(UserPage(count  = 2,
                                                       result = list(map(jsonable_encoder, users))))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_users_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_users(token = user_token)

    assert response.status_code == 403

class ReadUserTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_user(self, *, db_user, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/users/{db_user.id}',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_user(self):
    response = self.do_read_user(db_user = self.superuser)

    assert response.status_code == 200
    assert response.json()      == jsonable_encoder(User.from_orm(self.superuser))

  def test_read_user_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_user(db_user = user, token = user_token)

    assert response.status_code == 403

  def test_read_user_inexistent_user(self, inexistent_user):
    response = self.do_read_user(db_user = inexistent_user)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'User not found' }

class UpdateUserTests(APITestsSetup):
  # TODO note that this is full entity update
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, user_factory):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.db_user = user_factory(phone_number = None)

  def do_update_user(self, *, update_data, user_id = None, token = None):
    user_id = user_id or self.db_user.id
    token   = token   or self.superuser_token

    return self.api_client.put(f'{settings.API_V1_STR}/users/{user_id}',
                               json    = jsonable_encoder(update_data),
                               headers = self.auth_helpers.auth_header(token))

  def update_data_for(self, *, db_user = None, **kwargs):
    db_user   = db_user or self.db_user
    user_data = py_.pick(db_user, 'email', 'phone_number', 'is_active',
                         'is_golf_course_manager', 'is_superuser')

    return { **user_data, **kwargs }

  def test_update_user(self, db):
    response = self.do_update_user(update_data = self.update_data_for(phone_number = '123456789'))

    db.refresh(self.db_user)

    actual_response_data   = response.json()
    expected_response_data = jsonable_encoder(User.from_orm(self.db_user))

    assert self.db_user.phone_number == '123456789'

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_update_user_set_email_that_is_already_used_by_another_user(self):
    response = self.do_update_user(update_data = self.update_data_for(email = self.superuser.email))

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Email is already registered' }

  def test_update_user_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_update_user(token       = user_token,
                                   update_data = self.update_data_for(phone_number = '123456789'))

    assert response.status_code == 403

  def test_update_user_inexistent_user(self, inexistent_user):
    response = self.do_update_user(user_id     = inexistent_user.id,
                                   update_data = self.update_data_for(phone_number = '123456789'))

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'User not found' }
