import pytest

from golferis          import crud
from golferis.api.core import settings

class GetAccessTokenTests:
  @pytest.fixture(autouse = True)
  def setup(self, api_client, user, user_password):
    self.user          = user
    self.api_client    = api_client
    self.user_password = user_password

  def do_get_login_token(self, login_data = None):
    if login_data is None:
      login_data = { "username": self.user.email,
                     "password": self.user_password }

    return self.api_client.post(f"{settings.API_V1_STR}/auth/access-token", data = login_data)

  def test_get_access_token(self):
    response = self.do_get_login_token()

    response_data = response.json()

    assert response.status_code == 200
    assert "access_token"       in response_data
    assert response_data["access_token"]

  @pytest.mark.parametrize(
    'username_modifier, password_modifier', [
      pytest.param('foo', '',    id = 'invalid_username'),
      pytest.param('',    'foo', id = 'invalid_password'),
      pytest.param('foo', 'bar', id = 'invalid_username_and_password')
    ]
  )
  def test_get_access_token_incorrect_login_data(self, username_modifier, password_modifier):
    login_data = { 'username': f'{self.user.email}{username_modifier}',
                   'password': f'{self.user_password}{password_modifier}' }

    response = self.do_get_login_token(login_data = login_data)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Incorrect email or password' }

  def test_get_access_token_inactive_user(self, db):
    crud.user.update(db, db_obj = self.user, obj_in = { 'is_active': False })

    response = self.do_get_login_token()

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Inactive user' }
