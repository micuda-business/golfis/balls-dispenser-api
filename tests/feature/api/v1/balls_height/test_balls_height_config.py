import pytest
from fastapi.encoders import jsonable_encoder

from golferis          import crud
from golferis.schemas  import BallsHeightConfig, BallsHeightConfigUpdate
from golferis.api.core import settings

from helpers   import APITestsSetup
from factories import BallsHeightConfigCreateFactory

NOTIFY_AT_HEIGHT_DEFAULT  = 100
NOTIFICATION_TEXT_DEFAULT = 'Dochazeji micky'

class BallsHeightConfigTestsHelpers(APITestsSetup):
  def do_create_balls_height_config(self, *,
                                    notify_at_height  = NOTIFY_AT_HEIGHT_DEFAULT,
                                    notification_text = NOTIFICATION_TEXT_DEFAULT,
                                    token             = None):
    token                      = token or self.superuser_token
    balls_height_config_create = \
      BallsHeightConfigCreateFactory(notify_at_height  = notify_at_height,
                                     notification_text = notification_text)

    return self.api_client.post(f'{settings.API_V1_STR}/balls-height/config',
                                json    = jsonable_encoder(balls_height_config_create),
                                headers = self.auth_helpers.auth_header(token))

class ReadBallsHeightConfigTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password,
            balls_height_config_factory):
    self.db_balls_height_config = \
      balls_height_config_factory(notify_at_height  = NOTIFY_AT_HEIGHT_DEFAULT,
                                  notification_text = NOTIFICATION_TEXT_DEFAULT)

    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_balls_height_config(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/balls-height/config',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_balls_height_config(self):
    response = self.do_read_balls_height_config()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder(BallsHeightConfig.from_orm(self.db_balls_height_config))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_balls_height_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_balls_height_config(token = user_token)

    assert response.status_code == 403

class CreateBallsHeightConfigTests(BallsHeightConfigTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def test_create_balls_config(self, db):
    response = self.do_create_balls_height_config()

    actual_response_data   = response.json()
    db_balls_height_config = crud.balls_height_config.get(db, actual_response_data['id'])
    expected_response_data = jsonable_encoder(BallsHeightConfig.from_orm(db_balls_height_config))

    assert response.status_code == 201
    assert actual_response_data == expected_response_data

  def test_create_balls_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_create_balls_height_config(token = user_token)

    assert response.status_code == 403

  def test_create_balls_config_fail_if_config_record_already_exists(self):
    self.do_create_balls_height_config()

    response = self.do_create_balls_height_config()

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Config for balls height script already exists' }

    assert response.status_code == 400
    assert actual_response_data == expected_response_data

class UpdateBallsHeightConfigTests(BallsHeightConfigTestsHelpers):
  # TODO note that this is full entity update
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_update_balls_height_config(self, *, notify_at_height = None, notification_text = None,
                                    token = None):
    token = token or self.superuser_token

    params = {}
    if notify_at_height  is not None: params['notify_at_height']  = notify_at_height
    if notification_text is not None: params['notification_text'] = notification_text

    balls_height_config_update = BallsHeightConfigUpdate(**params)

    return self.api_client.put(f'{settings.API_V1_STR}/balls-height/config',
                               json    = jsonable_encoder(balls_height_config_update),
                               headers = self.auth_helpers.auth_header(token))

  def test_update_balls_config(self, db):
    create_response = self.do_create_balls_height_config()

    create_response_data   = create_response.json()
    db_balls_height_config = crud.balls_height_config.get(db, create_response_data['id'])

    assert db_balls_height_config.notify_at_height  == 100
    assert db_balls_height_config.notification_text == 'Dochazeji micky'

    response = self.do_update_balls_height_config(notify_at_height  = 111,
                                                  notification_text = "You're out of balls soon")

    actual_response_data           = response.json()
    db_updated_balls_height_config = crud.balls_height_config.get(db, actual_response_data['id'])
    expected_response_data         = \
      jsonable_encoder(BallsHeightConfig.from_orm(db_updated_balls_height_config))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

    assert db_updated_balls_height_config.notify_at_height  == 111
    assert db_updated_balls_height_config.notification_text == "You're out of balls soon"

  def test_update_balls_config_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_update_balls_height_config(token = user_token)

    assert response.status_code == 403

  def test_update_balls_config_if_config_does_not_exist(self):
    response = self.do_update_balls_height_config()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder({ 'detail': 'Config for balls height script not found' })

    assert response.status_code == 404
    assert actual_response_data == expected_response_data
