import pytest
from fastapi.encoders import jsonable_encoder

from golferis.api.core             import settings
from golferis.schemas.balls_height import BallsHeightLogPage, BallsHeightLog

from helpers import APITestsSetup

class ReadBallsHeightLogTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_balls_height_log(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/balls-height/log',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_balls_height_log(self, balls_height_log_factory):
    db_balls_height_log = balls_height_log_factory()

    response = self.do_read_balls_height_log()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder(
        BallsHeightLogPage(
          count = 1, result = [jsonable_encoder(BallsHeightLog.from_orm(db_balls_height_log))]
        )
      )

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_balls_height_log_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_balls_height_log(token = user_token)

    assert response.status_code == 403
