from datetime import datetime, timedelta

import pytest
from pydash           import py_
from fastapi.encoders import jsonable_encoder

from golferis                import crud
from golferis.api.core       import settings
from golferis.schemas.player import Player, PlayerCreate, PlayerPage

from helpers   import APITestsSetup
from factories import PlayerCreateDictFactory

pytestmark = pytest.mark.usefixtures('db')

@pytest.fixture
def activated_player(activated_player, card_factory):
  player_id = activated_player.id

  activated_player.activated_card   = card_factory(owner_id = player_id, is_active = True)
  activated_player.deactivated_card = card_factory(owner_id = player_id, is_active = False)

  return activated_player

@pytest.fixture
def deactivated_player(deactivated_player, card_factory):
  player_id = deactivated_player.id

  deactivated_player.activated_card   = card_factory(owner_id = player_id, is_active = True)
  deactivated_player.deactivated_card = card_factory(owner_id = player_id, is_active = False)

  return deactivated_player

class CreatePlayerTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_create_player(self, *, player_create = None, token = None):
    token         = token         or self.superuser_token
    player_create = player_create or PlayerCreateDictFactory()

    return self.api_client.post(f'{settings.API_V1_STR}/players/',
                                json    = jsonable_encoder(player_create),
                                headers = self.auth_helpers.auth_header(token))

  def test_create_player(self, db):
    response = self.do_create_player()

    actual_response_data   = response.json()
    db_player              = crud.player.get(db, actual_response_data['id'])
    expected_response_data = jsonable_encoder(Player.from_orm(db_player))

    assert response.status_code == 201
    assert actual_response_data == expected_response_data

  def test_create_player_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_create_player(token = user_token)

    assert response.status_code == 403

  def test_create_player_existing_email(self, player_factory):
    db_player     = player_factory()
    player_create = PlayerCreate(**Player.from_orm(db_player).dict())

    response = self.do_create_player(player_create = player_create)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Email is already registered' }

class ReadPlayersTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_players(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/players/',
                               headers = self.auth_helpers.auth_header(token))

  def do_read_players_with_incomplete_profile(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/players/incomplete-profile',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_players(self, player_factory):
    response = self.do_read_players()

    assert response.status_code == 200
    assert response.json()      == jsonable_encoder(PlayerPage(count = 0, result = []))

    db_player = player_factory()

    response = self.do_read_players()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder(PlayerPage(count = 1, result = [jsonable_encoder(Player.from_orm(db_player))]))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_players_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_players(token = user_token)

    assert response.status_code == 403

  def test_read_players_with_incomplete_profile(self, player_factory):
    future_datetime = datetime.now() + timedelta(minutes = 5)

    full_profile_player          = player_factory()
    three_items_profile_player_1 = player_factory(first_name   = None, created_at = future_datetime)
    three_items_profile_player_2 = player_factory(last_name    = None)
    two_items_profile_player_1   = player_factory(first_name   = None, last_name = None)
    two_items_profile_player_2   = player_factory(email        = None, last_name = None,
                                                  created_at   = future_datetime)
    one_item_profile_player_1    = player_factory(first_name   = None, last_name = None,
                                                  phone_number = None)
    one_item_profile_player_2    = player_factory(email        = None, last_name = None,
                                                  phone_number = None, created_at = future_datetime)
    empty_profile_player         = player_factory(first_name   = None, last_name = None,
                                                  phone_number = None, email     = None)

    response = self.do_read_players_with_incomplete_profile()

    actual_response_data   = response.json()
    expected_response_data = self.players_to_response(empty_profile_player,
                                                      one_item_profile_player_1,
                                                      one_item_profile_player_2,
                                                      two_items_profile_player_1,
                                                      two_items_profile_player_2,
                                                      three_items_profile_player_2,
                                                      three_items_profile_player_1)

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_players_with_incomplete_profile_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_players_with_incomplete_profile(token = user_token)

    assert response.status_code == 403

  @staticmethod
  def players_to_response(*players):
    return [jsonable_encoder(Player.from_orm(p)) for p in players]

class ReadPlayerTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_read_player(self, *, db_player, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/players/{db_player.id}',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_player(self, activated_player):
    response = self.do_read_player(db_player = activated_player)

    assert response.status_code == 200
    assert response.json()      == jsonable_encoder(Player.from_orm(activated_player))

  def test_read_player_for_non_superuser_fails(self, activated_player, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_player(db_player = activated_player, token = user_token)

    assert response.status_code == 403

  def test_read_player_inexistent_player(self, inexistent_player):
    response = self.do_read_player(db_player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

class UpdatePlayerTests(APITestsSetup):
  # TODO note that this is full entity update
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, player_factory):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.db_player = player_factory(first_name = 'John', last_name = 'Doe')

  def do_update_player(self, *, update_data, player_id = None, token = None):
    player_id = player_id or self.db_player.id
    token     = token     or self.superuser_token

    return self.api_client.put(f'{settings.API_V1_STR}/players/{player_id}',
                               json    = jsonable_encoder(update_data),
                               headers = self.auth_helpers.auth_header(token))

  def update_data_for(self, *, db_player = None, **kwargs):
    db_player   = db_player or self.db_player
    player_data = py_.pick(db_player, 'first_name', 'last_name', 'phone_number',
                           'golfer_id', 'email', 'user_id')

    return { **player_data, **kwargs }

  def test_update_player(self, db):
    response = self.do_update_player(update_data = self.update_data_for(first_name = 'Jane'))

    db.refresh(self.db_player)

    actual_response_data   = response.json()
    expected_response_data = jsonable_encoder(Player.from_orm(self.db_player))

    assert self.db_player.first_name == 'Jane'

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_update_player_set_email_that_is_already_used_by_another_player(self, player_factory):
    another_player = player_factory()

    response = \
      self.do_update_player(update_data = self.update_data_for(email = another_player.email))

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Email is already registered' }

  def test_update_player_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_update_player(token       = user_token,
                                     update_data = self.update_data_for(first_name = 'Jane'))

    assert response.status_code == 403

  def test_update_player_inexistent_player(self, inexistent_player):
    response = self.do_update_player(player_id   = inexistent_player.id,
                                     update_data = self.update_data_for(first_name = 'Jane'))

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

class ActivatePlayerTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_activate_player(self, *, db_player, include_cards = None, token = None):
    token = token or self.superuser_token

    if include_cards is None: params = None
    else:                     params = { 'include_cards': include_cards }

    return self.api_client.post(f'{settings.API_V1_STR}/players/{db_player.id}/activation',
                                params  = params,
                                headers = self.auth_helpers.auth_header(token))

  def test_activate_player(self, deactivated_player):
    response = self.do_activate_player(db_player = deactivated_player)

    assert response.status_code                          == 204
    assert deactivated_player.is_active                  is True
    assert deactivated_player.activated_card.is_active   is True
    assert deactivated_player.deactivated_card.is_active is False

  def test_activate_player_for_non_superuser_fails(self, deactivated_player, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_activate_player(db_player = deactivated_player, token = user_token)

    assert response.status_code == 403

  def test_activate_player_and_include_cards(self, deactivated_player):
    response = self.do_activate_player(db_player = deactivated_player, include_cards = True)

    assert response.status_code                          == 204
    assert deactivated_player.is_active                  is True
    assert deactivated_player.activated_card.is_active   is True
    assert deactivated_player.deactivated_card.is_active is True

  def test_activate_player_that_is_already_activated(self, activated_player):
    response = self.do_activate_player(db_player = activated_player)

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Player is already activated' }

    assert response.status_code                        == 412
    assert actual_response_data                        == expected_response_data
    assert activated_player.is_active                  is True
    assert activated_player.activated_card.is_active   is True
    assert activated_player.deactivated_card.is_active is False

  def test_activate_player_and_include_cards_for_already_activated_player(self, activated_player):
    response = self.do_activate_player(db_player = activated_player, include_cards = True)

    assert response.status_code                        == 204
    assert activated_player.is_active                  is True
    assert activated_player.activated_card.is_active   is True
    assert activated_player.deactivated_card.is_active is True

  def test_activate_player_inexistent_player(self, inexistent_player):
    response = self.do_activate_player(db_player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

class DeactivatePlayerTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

  def do_deactivate_player(self, *, db_player, token = None):
    token = token or self.superuser_token

    return self.api_client.delete(f'{settings.API_V1_STR}/players/{db_player.id}/activation',
                                  headers = self.auth_helpers.auth_header(token))

  def test_deactivate_player(self, activated_player):
    response = self.do_deactivate_player(db_player = activated_player)

    assert response.status_code                        == 204
    assert activated_player.is_active                  is False
    assert activated_player.activated_card.is_active   is False
    assert activated_player.deactivated_card.is_active is False

  def test_deactivate_player_for_non_superuser_fails(self, activated_player, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_deactivate_player(db_player = activated_player, token = user_token)

    assert response.status_code == 403

  def test_deactivate_player_inexistent_player(self, inexistent_player):
    response = self.do_deactivate_player(db_player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

  def test_deactivate_player_that_is_already_deactivated(self, deactivated_player):
    response = self.do_deactivate_player(db_player = deactivated_player)

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Player is already deactivated' }

    assert response.status_code                          == 412
    assert actual_response_data                          == expected_response_data
    assert deactivated_player.is_active                  is False
    assert deactivated_player.activated_card.is_active   is True
    assert deactivated_player.deactivated_card.is_active is False
