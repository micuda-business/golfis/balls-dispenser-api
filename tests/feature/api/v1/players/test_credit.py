from decimal                import Decimal
from datetime               import datetime
from dateutil.relativedelta import relativedelta

import pytest
from fastapi.encoders import jsonable_encoder

from golferis          import crud, schemas
from golferis.db       import models
from golferis.api.core import settings

from helpers   import APITestsSetup, add_player_debt
from factories import PurchaseCreditFactory, SpendCreditFactory

pytestmark = pytest.mark.usefixtures('db')

class CreditTestsHelpers(APITestsSetup):
  def credit_tests_setup(self, db, api_client, auth_helpers, superuser,
                         superuser_password, activated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.db               = db
    self.activated_player = activated_player

  def do_purchase_credit(self, *, player = None, credits_count = 100, card = None,
                         notes = None, price = 3500, expiry_at = None, token = None):
    token           = token or self.superuser_token
    purchase_credit = PurchaseCreditFactory(
      player_id     = (player or self.activated_player).id,
      card_id       = card and card.id,
      credits_count = credits_count,
      notes         = notes,
      price         = price,
      expiry_at     = expiry_at
    )

    return self.api_client.post(f'{settings.API_V1_STR}/players/credit',
                                json    = jsonable_encoder(purchase_credit),
                                headers = self.auth_helpers.auth_header(token))

  def add_player_debt(self, *, credits_count, player = None):
    return add_player_debt(self.db,
                           credits_count = credits_count,
                           player        = player or self.activated_player)

  def _credits_amount_response_data(self, *, credits_count, player = None):
    if player is None: player = self.activated_player

    return jsonable_encoder(schemas.CreditAmount(player_id     = player.id,
                                                 credits_count = credits_count))

class PurchaseCreditTests(CreditTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, db, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.credit_tests_setup(db, api_client, auth_helpers, superuser,
                            superuser_password, activated_player)

  def test_purchase_credit(self):
    purchase_credit_expiry_at = (datetime.now() + relativedelta(years = 1)).replace(microsecond = 0)

    response = self.do_purchase_credit(
      player = self.activated_player, credits_count = 100,
      notes = 'some note', price = 3500, expiry_at = purchase_credit_expiry_at
    )

    assert response.status_code == 200
    assert response.json()      == self._credits_amount_response_data(credits_count = 100)

  @pytest.mark.parametrize(
    'purchase_credits_count, available_credits', [
      pytest.param(20, 10, id = 'debt_overpay'),
      pytest.param(10,  0, id = 'debt_settled'),
      pytest.param(5,  -5, id = 'debt_partially_paid')
    ]
  )
  def test_purchase_credit_for_player_in_debt(self, purchase_credits_count, available_credits):
    player_credit = self.add_player_debt(credits_count = 10)

    assert player_credit.credit_purchase_id is None

    purchase_credit_expiry_at = (datetime.now() + relativedelta(years = 1)).replace(microsecond = 0)

    response = self.do_purchase_credit(
      player = self.activated_player, credits_count = purchase_credits_count,
      notes = 'some note', price = 3500, expiry_at = purchase_credit_expiry_at
    )

    assert response.status_code == 200
    assert response.json()      == self._credits_amount_response_data(credits_count = available_credits)

  def test_purchase_credit_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_purchase_credit(token = user_token)

    assert response.status_code == 403

  def test_purchase_credit_with_invalid_card(self, player_factory, card_factory):
    another_player = player_factory(is_active = True)
    card           = card_factory(owner_id = another_player.id, is_active = True)

    response = self.do_purchase_credit(
      player = self.activated_player, credits_count = 100,
      card = card, notes = 'some note', price = 3500
    )

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_purchase_credit_with_inexistent_card(self, inexistent_card):
    response = self.do_purchase_credit(
      player = self.activated_player, credits_count = 100,
      card = inexistent_card, notes = 'some note', price = 3500
    )

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_purchase_credit_with_deactivated_card(self, card_factory):
    card = card_factory(owner_id = self.activated_player.id, is_active = False)

    response = self.do_purchase_credit(
      player = self.activated_player, credits_count = 100,
      card = card, notes = 'some note', price = 3500
    )

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_purchase_credit_inexistent_player(self, inexistent_player):
    response = self.do_purchase_credit(player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

  def test_purchase_credit_deactivated_player(self, deactivated_player):
    response = self.do_purchase_credit(player = deactivated_player)

    assert response.status_code == 412
    assert response.json()      == { 'detail': 'Cannot purchase credit, player is deactivated' }

class PurchaseCreditDataTests(CreditTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, db, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.credit_tests_setup(db, api_client, auth_helpers, superuser,
                            superuser_password, activated_player)

  def test_purchase_credit(self):
    purchase_credit_expiry_at = (datetime.now() + relativedelta(years = 1)).replace(microsecond = 0)

    self.do_purchase_credit(
      player = self.activated_player, credits_count = 100,
      notes = 'some note', price = 3500, expiry_at = purchase_credit_expiry_at
    )

    player_credit = self.db \
      .query(models.PlayerCredit) \
      .filter(models.PlayerCredit.player_id == self.activated_player.id) \
      .first()

    assert player_credit               is not None
    assert player_credit.credits_spend == 0

    credit_transtaction = player_credit.credit_purchase

    assert credit_transtaction.player_id     == self.activated_player.id
    assert credit_transtaction.credits_count == 100
    assert credit_transtaction.notes         == 'some note'
    assert credit_transtaction.price         == Decimal(3500)
    assert credit_transtaction.expiry_at     == purchase_credit_expiry_at

  @pytest.mark.parametrize(
    'purchase_credits_count, is_debt_repaid', [
      pytest.param(20, True,  id = 'debt_overpay'),
      pytest.param(10, True,  id = 'debt_settled'),
      pytest.param(5,  False, id = 'debt_partially_paid')
    ]
  )
  def test_purchase_credit_for_player_in_debt(self, purchase_credits_count, is_debt_repaid):
    PLAYER_DEBT   = 10
    player_credit = self.add_player_debt(credits_count = PLAYER_DEBT)

    assert player_credit.credit_purchase_id is None

    purchase_credit_expiry_at = (datetime.now() + relativedelta(years = 1)).replace(microsecond = 0)

    self.do_purchase_credit(
      player = self.activated_player, credits_count = purchase_credits_count,
      notes = 'some note', price = 3500, expiry_at = purchase_credit_expiry_at
    )

    self.db.refresh(player_credit)

    player_credits_stmt = self.db \
      .query(models.PlayerCredit) \
      .filter(models.PlayerCredit.player_id == self.activated_player.id) \

    player_credits_count = player_credits_stmt.count()

    if is_debt_repaid:
      assert player_credits_count        == 1
      assert player_credit.credits_spend == PLAYER_DEBT
    else:
      assert player_credits_count        == 2
      assert player_credit.credits_spend == purchase_credits_count

      debt_player_credit = player_credits_stmt \
        .filter(models.PlayerCredit.credit_purchase_id.is_(None)) \
        .first()

      assert debt_player_credit.credits_spend == PLAYER_DEBT - purchase_credits_count

    credit_transtaction = player_credit.credit_purchase

    assert credit_transtaction               is not None
    assert credit_transtaction.player_id     == self.activated_player.id
    assert credit_transtaction.credits_count == purchase_credits_count
    assert credit_transtaction.notes         == 'some note'
    assert credit_transtaction.price         == Decimal(3500)
    assert credit_transtaction.expiry_at     == purchase_credit_expiry_at

class SpendCreditTests(CreditTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, db, api_client, auth_helpers, superuser, superuser_password, activated_player,
            card_factory):
    self.credit_tests_setup(db, api_client, auth_helpers, superuser,
                            superuser_password, activated_player)

    self.player_card = card_factory(owner_id = self.activated_player.id, is_active = True)

  def do_spend_credit(self, *, player = None, credits_count = 1, card = None,
                      notes = None, token = None):
    token        = token or self.superuser_token
    spend_credit = SpendCreditFactory(
      player_id     = (player or self.activated_player).id,
      card_id       = (card   or self.player_card).id,
      credits_count = credits_count, notes = notes
    )

    return self.api_client.delete(f'{settings.API_V1_STR}/players/credit',
                                  json    = jsonable_encoder(spend_credit),
                                  headers = self.auth_helpers.auth_header(token))

  def test_spend_credit(self):
    self.do_purchase_credit(credits_count = 10)

    response = self.do_spend_credit()

    assert response.status_code == 200
    assert response.json()      == self._credits_amount_response_data(credits_count = 9)

  def test_spend_credit_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    self.do_purchase_credit(credits_count = 10)

    response = self.do_spend_credit(token = user_token)

    assert response.status_code == 403

  def test_spend_credit_with_invalid_card(self, player_factory, card_factory):
    another_player = player_factory(is_active = True)
    card           = card_factory(owner_id = another_player.id, is_active = True)

    self.do_purchase_credit(credits_count = 10)

    response = self.do_spend_credit(card = card)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_spend_credit_with_inexistent_card(self, inexistent_card):
    self.do_purchase_credit(credits_count = 10)

    response = self.do_spend_credit(card = inexistent_card)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_spend_credit_with_deactivated_card(self, card_factory):
    card = card_factory(owner_id = self.activated_player.id, is_active = False)

    self.do_purchase_credit(credits_count = 10)

    response = self.do_spend_credit(card = card)

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Provided card does not exist or is not active' }

  def test_spend_credit_inexistent_player(self, inexistent_player):
    response = self.do_spend_credit(player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

  def test_spend_credit_deactivated_player(self, deactivated_player):
    response = self.do_spend_credit(player = deactivated_player)

    assert response.status_code == 412
    assert response.json()      == { 'detail': 'Cannot spend credit, player is deactivated' }

  def test_spend_credit_player_has_no_credit(self):
    self.do_purchase_credit(credits_count = 1)
    self.do_spend_credit()

    response = self.do_spend_credit()

    assert response.status_code == 412
    assert response.json()      == { 'detail': 'Cannot spend credit, player has no credit' }

class GetCreditTests(CreditTestsHelpers):
  @pytest.fixture(autouse = True)
  def setup(self, db, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.credit_tests_setup(db, api_client, auth_helpers, superuser,
                            superuser_password, activated_player)

  def do_get_credit_amount(self, *, player = None, token = None):
    token     = token   or self.superuser_token
    player_id = (player or self.activated_player).id

    return self.api_client.get(f'{settings.API_V1_STR}/players/credit/{player_id}',
                               headers = self.auth_helpers.auth_header(token))

  def test_get_credit_amount(self):
    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == 0

    self.do_purchase_credit(credits_count = 10)

    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == 10

  def test_get_credit_amount_for_player_in_debt(self):
    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == 0

    self.add_player_debt(credits_count = 10)

    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == -10

  def test_get_credit_amount_deactivated_player(self):
    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == 0

    self.do_purchase_credit(credits_count = 10)

    # deactivate player
    self.api_client.delete(f'{settings.API_V1_STR}/players/{self.activated_player.id}/activation',
                           headers = self.auth_helpers.auth_header(self.superuser_token))

    response = self.do_get_credit_amount()

    assert response.status_code == 200
    assert response.json()      == 10

  def test_get_credit_amount_inexistent_player(self, inexistent_player):
    response = self.do_get_credit_amount(player = inexistent_player)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Player not found' }

  def test_get_credit_amount_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_get_credit_amount(token = user_token)

    assert response.status_code == 403
