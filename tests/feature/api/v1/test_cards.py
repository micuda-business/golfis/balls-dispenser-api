from functools import partial

import pytest, flexmock
from fastapi.encoders import jsonable_encoder

from golferis              import crud
from golferis.api.core     import settings
from golferis.schemas.card import Card, CardPage

from helpers   import APITestsSetup
from factories import CardCreateFactory

pytestmark = pytest.mark.usefixtures('db')

inexistent_card = pytest.fixture(lambda fake_uuid: flexmock(id = fake_uuid))

class CreateCardTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.activated_player = activated_player

  def do_create_card(self, *, card_create = None, token = None):
    token       = token       or self.superuser_token
    card_create = card_create or CardCreateFactory(owner_id = self.activated_player.id)

    return self.api_client.post(f'{settings.API_V1_STR}/cards/',
                                json    = jsonable_encoder(card_create),
                                headers = self.auth_helpers.auth_header(token))

  def test_create_card(self, db):
    response = self.do_create_card()

    db_card = self._db_card_from_response(db, response)

    assert response.status_code == 201
    assert response.json()      == jsonable_encoder(Card.from_orm(db_card))

  def test_create_card_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_create_card(token = user_token)

    assert response.status_code == 403

  def test_create_card_for_inexistent_player(self, inexistent_player):
    response = self.do_create_card(card_create = CardCreateFactory(owner_id = inexistent_player.id))

    assert response.status_code == 400
    assert response.json()      == { 'detail': 'Player does not exist' }

  def test_create_card_that_player_already_own(self, db):
    original_card_response = self.do_create_card()
    db_card                = self._db_card_from_response(db, original_card_response)
    card_create_factory    = partial(CardCreateFactory,
                                     card_number = db_card.card_number,
                                     owner_id    = db_card.owner_id)

    active_card_create   = card_create_factory(is_active = True)
    inactive_card_create = card_create_factory(is_active = False)

    active_card_response = self.do_create_card(card_create = active_card_create)

    assert active_card_response.status_code == 412
    assert active_card_response.json()      == { 'detail': 'Player already owns this card' }

    inactive_card_response = self.do_create_card(card_create = inactive_card_create)

    inactive_db_card = self._db_card_from_response(db, inactive_card_response)

    assert inactive_card_response.status_code == 201
    assert inactive_card_response.json()      == jsonable_encoder(Card.from_orm(inactive_db_card))

  @staticmethod
  def _db_card_from_response(db, response):
    return crud.card.get(db, response.json()['id'])

class ReadCardsTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.activated_player = activated_player

  def do_read_cards(self, *, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/cards/',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_cards(self, card_factory):
    response = self.do_read_cards()

    assert response.status_code == 200
    assert response.json()      == jsonable_encoder(CardPage(count = 0, result = []))

    db_card = card_factory(owner_id = self.activated_player.id)

    response = self.do_read_cards()

    actual_response_data   = response.json()
    expected_response_data = \
      jsonable_encoder(CardPage(count = 1, result = [jsonable_encoder(Card.from_orm(db_card))]))

    assert response.status_code == 200
    assert actual_response_data == expected_response_data

  def test_read_players_for_non_superuser_fails(self, user, user_password):
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_cards(token = user_token)

    assert response.status_code == 403

class ReadCardTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, activated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.activated_player = activated_player

  def do_read_card(self, *, db_card, token = None):
    token = token or self.superuser_token

    return self.api_client.get(f'{settings.API_V1_STR}/cards/{db_card.id}',
                               headers = self.auth_helpers.auth_header(token))

  def test_read_player(self, card_factory):
    db_card = card_factory(owner_id = self.activated_player.id)

    response = self.do_read_card(db_card = db_card)

    assert response.status_code == 200
    assert response.json()      == jsonable_encoder(Card.from_orm(db_card))

  def test_read_card_for_non_superuser_fails(self, card_factory, user, user_password):
    db_card    = card_factory(owner_id = self.activated_player.id)
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_read_card(db_card = db_card, token = user_token)

    assert response.status_code == 403

  def test_read_card_inexistent_card(self, inexistent_card):
    response = self.do_read_card(db_card = inexistent_card)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Card not found' }

class ActivateCardTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, activated_player,
            deactivated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.activated_player   = activated_player
    self.deactivated_player = deactivated_player

  def do_activate_card(self, *, db_card, token = None):
    token = token or self.superuser_token

    return self.api_client.post(f'{settings.API_V1_STR}/cards/{db_card.id}/activation',
                                headers = self.auth_helpers.auth_header(token))

  def test_activate_card_of_activated_player(self, db, card_factory):
    db_card = card_factory(owner_id = self.activated_player.id, is_active = False)

    response = self.do_activate_card(db_card = db_card)

    db.refresh(db_card)

    assert response.status_code == 204
    assert db_card.is_active    is True

  def test_activate_card_of_deactivated_player(self, db, card_factory):
    db_card = card_factory(owner_id = self.activated_player.id, is_active = False)

    response = self.do_activate_card(db_card = db_card)

    db.refresh(db_card)

    assert response.status_code == 204
    assert db_card.is_active    is True

  def test_activate_card_of_activated_player_that_already_has_card_with_same_number(
    self, db, card_factory
  ):
    activated_db_card = card_factory(owner_id = self.activated_player.id, is_active = True)
    tested_db_card    = card_factory(is_active   = False,
                                     owner_id    = self.activated_player.id,
                                     card_number = activated_db_card.card_number)

    response = self.do_activate_card(db_card = tested_db_card)

    db.refresh(tested_db_card)

    assert response.status_code     == 412
    assert response.json()          == { 'detail': 'Player already owns this card' }
    assert tested_db_card.is_active is False

  def test_activate_card_that_is_already_activated(self, db, card_factory):
    db_card = card_factory(owner_id = self.activated_player.id, is_active = True)

    response = self.do_activate_card(db_card = db_card)

    db.refresh(db_card)

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Card is already activated' }

    assert response.status_code == 412
    assert actual_response_data == expected_response_data
    assert db_card.is_active    is True

  def test_activate_card_for_non_superuser_fails(self, card_factory, user, user_password):
    db_card    = card_factory(owner_id = self.activated_player.id, is_active = False)
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_activate_card(db_card = db_card, token = user_token)

    assert response.status_code == 403

  def test_activate_card_inexistent_card(self, inexistent_card):
    response = self.do_activate_card(db_card = inexistent_card)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Card not found' }

class DeactivateCardTests(APITestsSetup):
  @pytest.fixture(autouse = True)
  def setup(self, api_client, auth_helpers, superuser, superuser_password, activated_player,
            deactivated_player):
    self.base_setup(api_client, auth_helpers, superuser, superuser_password)

    self.activated_player   = activated_player
    self.deactivated_player = deactivated_player

  def do_deactivate_card(self, *, db_card, token = None):
    token = token or self.superuser_token

    return self.api_client.delete(f'{settings.API_V1_STR}/cards/{db_card.id}/activation',
                                  headers = self.auth_helpers.auth_header(token))

  @pytest.mark.parametrize(
    'player', [
      pytest.lazy_fixture('activated_player'),
      pytest.lazy_fixture('deactivated_player')
    ]
  )
  def test_deactivate_card(self, db, card_factory, player):
    db_card = card_factory(owner_id = player.id, is_active = True)

    response = self.do_deactivate_card(db_card = db_card)

    db.refresh(db_card)

    assert response.status_code == 204
    assert db_card.is_active    is False

  @pytest.mark.parametrize(
    'player', [
      pytest.lazy_fixture('activated_player'),
      pytest.lazy_fixture('deactivated_player')
    ]
  )
  def test_deactivate_card_that_is_already_deactivated(self, db, card_factory, player):
    db_card = card_factory(owner_id = player.id, is_active = False)

    response = self.do_deactivate_card(db_card = db_card)

    db.refresh(db_card)

    actual_response_data   = response.json()
    expected_response_data = { 'detail': 'Card is already deactivated' }

    assert response.status_code == 412
    assert actual_response_data == expected_response_data
    assert db_card.is_active    is False

  def test_activate_card_for_non_superuser_fails(self, card_factory, user, user_password):
    db_card    = card_factory(owner_id = self.activated_player.id, is_active = False)
    user_token = self.auth_helpers.get_token(username = user.email, password = user_password)

    response = self.do_deactivate_card(db_card = db_card, token = user_token)

    assert response.status_code == 403

  def test_activate_card_inexistent_card(self, inexistent_card):
    response = self.do_deactivate_card(db_card = inexistent_card)

    assert response.status_code == 404
    assert response.json()      == { 'detail': 'Card not found' }
