from golferis.db         import models
from golferis.api.core   import settings
from golferis.db.session import session_provider

class AuthHelpers:
  def __init__(self, api_client):
    self.api_client = api_client

  @staticmethod
  def auth_header(token):
    return { 'Authorization': f'Bearer {token}' }

  def get_token(self, username: str, password: str) -> str:
    login_data = { 'username': username, 'password': password }

    response = self.api_client.post(f'{settings.API_V1_STR}/auth/access-token', data = login_data)

    return response.json()['access_token']

class APITestsSetup:
  def base_setup(self, api_client, auth_helpers, superuser, superuser_password):
    self.api_client         = api_client
    self.auth_helpers       = auth_helpers
    self.superuser          = superuser
    self.superuser_password = superuser_password
    self.superuser_token    = auth_helpers.get_token(superuser.email, superuser_password)

def add_player_debt(db, *, player: models.Player, credits_count: int) -> models.PlayerCredit:
  player_credit = models.PlayerCredit(player_id = player.id, credits_spend = credits_count)

  db.add(player_credit)
  db.commit()
  db.refresh(player_credit)

  return player_credit
