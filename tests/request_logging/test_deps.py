import pytest

from fastapi_request_logging.deps import multi_values_dict

def test_multi_values_dict():
  data = [('username', 'tealover'),
          ('tea',      'Feng Huang Dan Cong'),
          ('tea',      'Zheng Shan Xiao Zhong')]

  actual   = multi_values_dict(data)
  expected = { 'username': 'tealover', 'tea': ['Feng Huang Dan Cong', 'Zheng Shan Xiao Zhong'] }

  assert actual == expected
