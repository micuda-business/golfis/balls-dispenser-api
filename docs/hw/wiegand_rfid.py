import time

from hwberry.rfid import WiegandRFID

PIN_0       = 0  # GPIO Pin 17 |  Green cable | Data0
PIN_1       = 2  # GPIO Pin 27 |  White cable | Data1
PIN_SOUND   = 25 # GPIO Pin 26 | Yellow cable | Sound

def to_binary_repr(data):
  return ''.join(f'{d:08b}' for d in data)

if __name__ == '__main__':
  try:
    rfid_reader = WiegandRFID(d0_pin = PIN_0, d1_pin = PIN_1, sound_pin = PIN_SOUND)

    for bits_count, data in rfid_reader:
      bytes_count     = int(bits_count / 8 + 1)
      time_from_epoch = f'{int(time.time() * 10**9)}'
      hex_repr        = ''.join(f'{d:02X}' for d in data)
      binary_repr     = to_binary_repr(data)

      message = \
        f'{time_from_epoch} Read {bits_count} bits ({bytes_count} bytes): {hex_repr} : {binary_repr}'

      print(message)

  except KeyboardInterrupt:
    print("Stopped by user")
