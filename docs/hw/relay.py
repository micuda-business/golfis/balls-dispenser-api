import time

from RPi import GPIO

from hwberry.relay import LowOnRelay

RELAY_PIN = 23

GPIO.setmode(GPIO.BCM)

if __name__ == '__main__':
  try:
    with LowOnRelay(RELAY_PIN) as relay:
      relay.on()
      time.sleep(1.5)
      relay.off()

  except KeyboardInterrupt:
    print("Stopped by user")
