import time

from RPi import GPIO

from hwberry.ultrasonic_range_sensor import UltrasonicRangeSensor

ECHO_PIN    = 15
TRIGGER_PIN = 4

GPIO.setmode(GPIO.BCM)

if __name__ == '__main__':
  try:
    with UltrasonicRangeSensor(TRIGGER_PIN, ECHO_PIN) as range_sensor:
      while True:
        distance = range_sensor.measure_distance()

        print(f'Measured distance is {distance:.1f} cm.')

        time.sleep(1)

  except KeyboardInterrupt:
    print("Measurement stopped by user")
