from hwberry.lcd import LCD

LCD_ADDRESS = 0x3F

if __name__ == '__main__':
  try:
    lcd = LCD(address = LCD_ADDRESS, lines_count = 4, line_width = 20)

    lcd.clear_display()
    lcd.print(lines = ['Hello World'], trim_text = True)

  except KeyboardInterrupt:
    print("Stopped by user")
