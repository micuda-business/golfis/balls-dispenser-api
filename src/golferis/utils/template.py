from jinja2 import Environment

def render_template(template_string: str, context: dict = None) -> str:
  context   = context or {}
  jinja_env = Environment()
  template  = jinja_env.from_string(template_string)

  return template.render(context)
