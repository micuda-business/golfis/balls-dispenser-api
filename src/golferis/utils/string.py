import unicodedata

def strip_accents(text: str) -> str:
  return \
    unicodedata.normalize('NFD', text) \
    .encode('ascii', 'ignore') \
    .decode('utf-8')
