from contextlib import contextmanager, closing

from sqlalchemy     import create_engine
from sqlalchemy.orm import sessionmaker

from golferis.api.core import settings

engine       = create_engine(settings.DATABASE_URI)
SessionLocal = sessionmaker(autocommit = False, autoflush = False, bind = engine)

@contextmanager
def session_provider(*args, **kwargs):
  with closing(SessionLocal(*args, **kwargs)) as db:
    yield db
