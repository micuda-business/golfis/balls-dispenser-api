from sqlalchemy_repr            import PrettyRepresentableBase
from sqlalchemy.ext.declarative import declarative_base

ModelBase = declarative_base(cls = PrettyRepresentableBase)
