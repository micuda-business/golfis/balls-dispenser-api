from sqlalchemy     import Column, JSON
from sqlalchemy.orm import relationship

from .model_base import ModelBase
from .fields     import PrimaryKey, CharField, UpdatedAtField, CreatedAtField, ForeignKey

class BallsDispenserConfig(ModelBase):
  __tablename__ = 'balls_dispenser_config'

  id                              = PrimaryKey()
  no_credit_text                  = CharField(length = 90,  nullable = False)
  unknown_player_text             = CharField(length = 90,  nullable = False)
  current_credit_amount_text      = CharField(length = 90,  nullable = False)
  player_credit_notification_text = CharField(length = 120, nullable = False)
  notify_at_credit_amount         = Column(JSON, nullable = False)
  updated_at                      = UpdatedAtField()
  created_at                      = CreatedAtField()

class BallsDispenserLog(ModelBase):
  __tablename__ = 'balls_dispenser_log'

  id          = PrimaryKey()
  card_id     = ForeignKey('cards.id', on_delete = 'SET NULL', default = None,
                           server_default = None)
  card_number = CharField(length = 20, nullable = True, default = None, server_default = None)
  updated_at  = UpdatedAtField()
  created_at  = CreatedAtField()

  card = relationship('Card')
