from sqlalchemy import Column, Boolean, Integer

from .model_base import ModelBase
from .fields     import PrimaryKey, CharField, UpdatedAtField, CreatedAtField

class BallsHeightLog(ModelBase):
  __tablename__ = 'balls_height_log'

  id                = PrimaryKey()
  measured_height   = Column(Integer)
  notification_made = Column(Boolean)
  notes             = CharField(length = 512, nullable = True, default = None, server_default = None)
  updated_at        = UpdatedAtField()
  created_at        = CreatedAtField()

class BallsHeightConfig(ModelBase):
  __tablename__ = 'balls_height_config'

  id                = PrimaryKey()
  notify_at_height  = Column(Integer)
  notification_text = CharField(length = 256, nullable = False)
  updated_at        = UpdatedAtField()
  created_at        = CreatedAtField()
