from datetime import datetime

from sqlalchemy     import Column, Integer, Numeric, text
from sqlalchemy.orm import relationship

from .model_base import ModelBase
from .fields     import (
  PrimaryKey, CharField, DateTimeField, UpdatedAtField, CreatedAtField, ForeignKey
)

class PlayerCredit(ModelBase):
  __tablename__ = 'player_credits'

  id                 = PrimaryKey()
  player_id          = ForeignKey('players.id', on_delete = 'CASCADE', on_update = 'CASCADE')
  credit_purchase_id = ForeignKey('credit_transactions.id',
                                  on_delete = 'CASCADE', on_update = 'CASCADE')
  credits_spend      = Column(Integer, default = 0, server_default = text('0'))
  updated_at         = UpdatedAtField()
  created_at         = CreatedAtField()

  credit_purchase = relationship('CreditTransaction')
  player          = relationship('Player', back_populates = 'credits')

  @property
  def credits_left(self) -> int:
    return self.credit_purchase.credits_count - self.credits_spend

  @property
  def available_credits(self) -> int:
    if datetime.now() <= self.credit_purchase.expiry_at:
      return self.credits_left
    return 0

class CreditTransaction(ModelBase):
  __tablename__ = 'credit_transactions'

  id            = PrimaryKey()
  player_id     = ForeignKey('players.id', on_delete = 'CASCADE', on_update = 'CASCADE')
  card_id       = ForeignKey('cards.id',   on_delete = 'SET NULL',
                             default = None, server_default = None)
  credits_count = Column(Integer)
  notes         = CharField(length = 512, default = None, server_default = None)
  price         = Column(Numeric(8, 2), default = None, server_default = None)
  expiry_at     = DateTimeField(default = None, server_default = None)
  updated_at    = UpdatedAtField()
  created_at    = CreatedAtField()

  player = relationship('Player', back_populates = 'credit_transactions')
