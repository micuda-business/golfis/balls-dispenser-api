from sqlalchemy     import Column, Boolean
from sqlalchemy.orm import relationship

from .model_base import ModelBase
from .fields     import PrimaryKey, CharField, UpdatedAtField, CreatedAtField, ForeignKey

class Player(ModelBase):
  __tablename__ = 'players'

  id           = PrimaryKey()
  first_name   = CharField(length = 50,  nullable = True, default = None, server_default = None)
  last_name    = CharField(length = 100, nullable = True, default = None, server_default = None)
  phone_number = CharField(length = 15,  nullable = True, default = None, server_default = None)
  golfer_id    = CharField(length = 7,   nullable = True, default = None, server_default = None)
  email        = CharField(length = 254, nullable = True, default = None, server_default = None,
                           unique = True, index = True)
  user_id      = ForeignKey('users.id', on_delete = 'SET NULL', unique = True,
                            default = None, server_default = None)
  is_active    = Column(Boolean, default = False)
  updated_at   = UpdatedAtField()
  created_at   = CreatedAtField()
  # state      = None # TODO: probably remove

  owned_cards = \
    relationship('Card', foreign_keys = 'Card.owner_id', back_populates = 'owner')
  held_cards = \
    relationship('Card', foreign_keys = 'Card.holder_id', back_populates = 'holder')
  credits              = relationship('PlayerCredit',      back_populates = 'player')
  credit_transactions  = relationship('CreditTransaction', back_populates = 'player')
  user                 = relationship('User',              back_populates = 'player')

  @property
  def fullname(self) -> str:
    return f'{self.first_name or ""} {self.last_name or ""}'.strip()
