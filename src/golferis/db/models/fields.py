import uuid
from functools import partial

from sqlalchemy import Column, Integer, String, ForeignKey as SAForeignKey
from sqlalchemy.sql                 import func
from sqlalchemy.types               import TypeDecorator, BINARY
from sqlalchemy.dialects.mysql      import DATETIME
from sqlalchemy.dialects.postgresql import UUID as psqlUUID

DATETIME_PRECISION = 6

class UUID(TypeDecorator):
  """
  Platform-independent GUID type.

  Uses Postgresql's UUID type, otherwise uses
  BINARY(16), to store UUID.
  """
  impl = BINARY

  def load_dialect_impl(self, dialect):
    if dialect.name == 'postgresql':
      return dialect.type_descriptor(psqlUUID())
    else:
      return dialect.type_descriptor(BINARY(16))

  def process_bind_param(self, value, dialect):
    if value is None:
      return value
    else:
      if not isinstance(value, uuid.UUID):
        if   isinstance(value, bytes): value = uuid.UUID(bytes = value)
        elif isinstance(value, int):   value = uuid.UUID(int   = value)
        elif isinstance(value, str):   value = uuid.UUID(value)

    if dialect.name == 'postgresql': return str(value)
    else:                            return value.bytes

  def process_result_value(self, value, dialect):
    if value is None:                return value
    if dialect.name == 'postgresql': return uuid.UUID(value)
    else:                            return uuid.UUID(bytes = value)

PrimaryKey     = partial(Column, UUID, default = uuid.uuid4, primary_key = True, index = True)
DateTimeField  = partial(Column, DATETIME(timezone = True, fsp = DATETIME_PRECISION))
UpdatedAtField = partial(DateTimeField,
                         server_default = func.now(DATETIME_PRECISION),
                         onupdate       = func.now(DATETIME_PRECISION))
CreatedAtField = partial(DateTimeField, server_default = func.now(DATETIME_PRECISION))

def ForeignKey(to, on_update = None, on_delete = None, **kwargs):
  return Column(UUID, SAForeignKey(to, onupdate = on_update, ondelete = on_delete), **kwargs)

def CharField(*, length = None, **kwargs):
  return Column(String(length = length), **kwargs)
