from sqlalchemy     import Column, Boolean
from sqlalchemy.orm import relationship

from .model_base import ModelBase
from .fields     import PrimaryKey, CharField, UpdatedAtField, CreatedAtField, ForeignKey

class Card(ModelBase):
  __tablename__ = 'cards'

  id          = PrimaryKey()
  card_number = CharField(length = 20)
  is_active   = Column(Boolean, default = False)
  owner_id    = ForeignKey('players.id', on_delete = 'SET NULL',
                           default = None, server_default = None)
  holder_id   = ForeignKey('players.id', on_delete = 'SET NULL',
                           default = None, server_default = None)
  updated_at  = UpdatedAtField()
  created_at  = CreatedAtField()

  owner  = relationship('Player', foreign_keys = owner_id,  back_populates = 'owned_cards')
  holder = relationship('Player', foreign_keys = holder_id, back_populates = 'held_cards')
