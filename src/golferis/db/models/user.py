from sqlalchemy     import Column, Boolean
from sqlalchemy.orm import relationship

from .model_base import ModelBase
from .fields     import PrimaryKey, CharField, UpdatedAtField, CreatedAtField

class User(ModelBase):
  __tablename__ = 'users'

  id                     = PrimaryKey()
  email                  = CharField(length = 254, unique = True, index = True)
  phone_number           = CharField(length  = 15,   nullable       = True,
                                     default = None, server_default = None)
  password               = CharField(length = 254, nullable = False)
  is_active              = Column(Boolean, default = False)
  is_golf_course_manager = Column(Boolean, default = False)
  is_superuser           = Column(Boolean, default = False)
  updated_at             = UpdatedAtField()
  created_at             = CreatedAtField()

  player = relationship('Player', back_populates = 'user')
