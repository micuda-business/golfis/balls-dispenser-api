"""create balls height log table

Revision ID: 247955f7fef2
Revises: cc4a66353d9a
Create Date: 2020-07-26 10:40:44.473640

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '247955f7fef2'
down_revision = 'cc4a66353d9a'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'balls_height_log',
    sa.Column('id',                fields.UUID(),           nullable = False),
    sa.Column('measured_height',   sa.Integer(),            nullable = True),
    sa.Column('notification_made', sa.Boolean(),            nullable = True),
    sa.Column('notes',             sa.String(length = 512), nullable = True),
    sa.Column('updated_at',        mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',        mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_balls_height_log_id'), 'balls_height_log', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_balls_height_log_id'), table_name = 'balls_height_log')
  op.drop_table('balls_height_log')
