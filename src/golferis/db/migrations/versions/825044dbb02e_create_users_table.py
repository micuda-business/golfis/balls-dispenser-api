"""create users table

Revision ID: 825044dbb02e
Revises: 247955f7fef2
Create Date: 2020-07-26 10:40:59.960613

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '825044dbb02e'
down_revision = '247955f7fef2'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'users',
    sa.Column('id',                     fields.UUID(),           nullable = False),
    sa.Column('email',                  sa.String(length = 254), nullable = True),
    sa.Column('phone_number',           sa.String(length = 15),  nullable = True),
    sa.Column('password',               sa.String(length = 254), nullable = False),
    sa.Column('is_active',              sa.Boolean(),            nullable = True),
    sa.Column('is_golf_course_manager', sa.Boolean(),            nullable = True),
    sa.Column('is_superuser',           sa.Boolean(),            nullable = True),
    sa.Column('updated_at',             mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',             mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_users_email'), 'users', ['email'], unique = True)
  op.create_index(op.f('ix_users_id'),    'users', ['id'],    unique = False)

def downgrade():
  op.drop_index(op.f('ix_users_id'),    table_name = 'users')
  op.drop_index(op.f('ix_users_email'), table_name = 'users')
  op.drop_table('users')
