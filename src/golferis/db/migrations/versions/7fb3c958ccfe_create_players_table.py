"""create players table

Revision ID: 7fb3c958ccfe
Revises: 825044dbb02e
Create Date: 2020-07-26 10:41:23.302740

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '7fb3c958ccfe'
down_revision = '825044dbb02e'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'players',
    sa.Column('id',           fields.UUID(),           nullable = False),
    sa.Column('first_name',   sa.String(length = 50),  nullable = True),
    sa.Column('last_name',    sa.String(length = 100), nullable = True),
    sa.Column('phone_number', sa.String(length = 15),  nullable = True),
    sa.Column('golfer_id',    sa.String(length = 7),   nullable = True),
    sa.Column('email',        sa.String(length = 254), nullable = True),
    sa.Column('user_id',      fields.UUID(),           nullable = True),
    sa.Column('is_active',    sa.Boolean(),            nullable = True),
    sa.Column('updated_at',   mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',   mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ondelete = 'SET NULL'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id')
  )
  op.create_index(op.f('ix_players_email'), 'players', ['email'], unique = True)
  op.create_index(op.f('ix_players_id'),    'players', ['id'],    unique = False)

def downgrade():
  op.drop_index(op.f('ix_players_id'),    table_name = 'players')
  op.drop_index(op.f('ix_players_email'), table_name = 'players')
  op.drop_table('players')
