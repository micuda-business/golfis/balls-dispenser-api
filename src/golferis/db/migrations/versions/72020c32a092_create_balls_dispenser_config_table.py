"""create balls dispenser config table

Revision ID: 72020c32a092
Revises:
Create Date: 2020-07-26 10:37:44.640632

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '72020c32a092'
down_revision = None
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'balls_dispenser_config',
    sa.Column('id',                               fields.UUID(),           nullable = False),
    sa.Column('no_credit_text',                   sa.String(length = 90),  nullable = False),
    sa.Column('unknown_player_text',              sa.String(length = 90),  nullable = False),
    sa.Column('current_credit_amount_text',       sa.String(length = 90),  nullable = False),
    sa.Column('player_credit_notification_text',  sa.String(length = 120), nullable = False),
    sa.Column('notify_at_credit_amount',          sa.JSON(),               nullable = False),
    sa.Column('updated_at',                       mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',                       mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_balls_dispenser_config_id'), 'balls_dispenser_config', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_balls_dispenser_config_id'), table_name = 'balls_dispenser_config')
  op.drop_table('balls_dispenser_config')
