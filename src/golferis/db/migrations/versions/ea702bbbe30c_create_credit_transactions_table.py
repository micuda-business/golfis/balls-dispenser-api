"""create credit transactions table

Revision ID: ea702bbbe30c
Revises: 1a907faa242c
Create Date: 2020-07-26 10:42:09.547853

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = 'ea702bbbe30c'
down_revision = '1a907faa242c'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'credit_transactions',
    sa.Column('id',            fields.UUID(),                        nullable = False),
    sa.Column('player_id',     fields.UUID(),                        nullable = True),
    sa.Column('card_id',       fields.UUID(),                        nullable = True),
    sa.Column('credits_count', sa.Integer(),                         nullable = True),
    sa.Column('notes',         sa.String(length = 512),              nullable = True),
    sa.Column('price',         sa.Numeric(precision = 8, scale = 2), nullable = True),
    sa.Column('expiry_at',     mysql.DATETIME(timezone = True, fsp = 6),
              nullable = True),
    sa.Column('updated_at',    mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',    mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.ForeignKeyConstraint(['card_id'],   ['cards.id'],   ondelete = 'SET NULL'),
    sa.ForeignKeyConstraint(['player_id'], ['players.id'], onupdate = 'CASCADE', ondelete = 'CASCADE'),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_credit_transactions_id'), 'credit_transactions', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_credit_transactions_id'), table_name = 'credit_transactions')
  op.drop_table('credit_transactions')
