"""create cards table

Revision ID: 3c075c8c5eda
Revises: 7fb3c958ccfe
Create Date: 2020-07-26 10:41:31.472331

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '3c075c8c5eda'
down_revision = '7fb3c958ccfe'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'cards',
    sa.Column('id',          fields.UUID(),          nullable = False),
    sa.Column('card_number', sa.String(length = 20), nullable = True),
    sa.Column('is_active',   sa.Boolean(),           nullable = True),
    sa.Column('owner_id',    fields.UUID(),          nullable = True),
    sa.Column('holder_id',   fields.UUID(),          nullable = True),
    sa.Column('updated_at',  mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',  mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.ForeignKeyConstraint(['holder_id'], ['players.id'], ondelete = 'SET NULL'),
    sa.ForeignKeyConstraint(['owner_id'],  ['players.id'], ondelete = 'SET NULL'),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_cards_id'), 'cards', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_cards_id'), table_name = 'cards')
  op.drop_table('cards')
