"""create balls dispenser log table

Revision ID: 1a907faa242c
Revises: 3c075c8c5eda
Create Date: 2020-07-26 10:41:47.282845

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = '1a907faa242c'
down_revision = '3c075c8c5eda'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'balls_dispenser_log',
    sa.Column('id',          fields.UUID(),          nullable = False),
    sa.Column('card_id',     fields.UUID(),          nullable = True),
    sa.Column('card_number', sa.String(length = 20), nullable = True),
    sa.Column('updated_at',  mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',  mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.ForeignKeyConstraint(['card_id'], ['cards.id'], ondelete = 'SET NULL'),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_balls_dispenser_log_id'), 'balls_dispenser_log', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_balls_dispenser_log_id'), table_name = 'balls_dispenser_log')
  op.drop_table('balls_dispenser_log')
