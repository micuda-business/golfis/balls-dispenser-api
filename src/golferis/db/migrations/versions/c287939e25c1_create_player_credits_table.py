"""create player credits table

Revision ID: c287939e25c1
Revises: ea702bbbe30c
Create Date: 2020-07-26 10:42:24.930746

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = 'c287939e25c1'
down_revision = 'ea702bbbe30c'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'player_credits',
    sa.Column('id',                 fields.UUID(), nullable = False),
    sa.Column('player_id',          fields.UUID(), nullable = True),
    sa.Column('credit_purchase_id', fields.UUID(), nullable = True),
    sa.Column('credits_spend',      sa.Integer(),  server_default = sa.text('0'), nullable = True),
    sa.Column('updated_at',         mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',         mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.ForeignKeyConstraint(['credit_purchase_id'], ['credit_transactions.id'], onupdate = 'CASCADE',
                            ondelete = 'CASCADE'),
    sa.ForeignKeyConstraint(['player_id'], ['players.id'], onupdate = 'CASCADE', ondelete = 'CASCADE'),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_player_credits_id'), 'player_credits', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_player_credits_id'), table_name = 'player_credits')
  op.drop_table('player_credits')
