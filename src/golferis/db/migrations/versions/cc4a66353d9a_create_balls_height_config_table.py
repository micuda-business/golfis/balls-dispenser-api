"""create balls height config table

Revision ID: cc4a66353d9a
Revises: 72020c32a092
Create Date: 2020-07-26 10:38:09.539290

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from golferis.db.models import fields

# revision identifiers, used by Alembic.
revision      = 'cc4a66353d9a'
down_revision = '72020c32a092'
branch_labels = None
depends_on    = None

def upgrade():
  op.create_table(
    'balls_height_config',
    sa.Column('id',                fields.UUID(),           nullable = False),
    sa.Column('notify_at_height',  sa.Integer(),            nullable = True),
    sa.Column('notification_text', sa.String(length = 256), nullable = False),
    sa.Column('updated_at',        mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.Column('created_at',        mysql.DATETIME(timezone = True, fsp = 6),
              server_default = sa.text('now(6)'), nullable = True),
    sa.PrimaryKeyConstraint('id')
  )
  op.create_index(op.f('ix_balls_height_config_id'), 'balls_height_config', ['id'], unique = False)

def downgrade():
  op.drop_index(op.f('ix_balls_height_config_id'), table_name = 'balls_height_config')
  op.drop_table('balls_height_config')
