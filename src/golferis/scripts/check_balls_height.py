#!/usr/bin/env python3
from hwberry.ultrasonic_range_sensor import UltrasonicRangeSensor

from golferis.services.balls_service import BallsService

def run(trigger_pin: int, echo_pin: int, balls_service: BallsService):
  with UltrasonicRangeSensor(trigger_pin, echo_pin) as range_sensor:
    balls_height = range_sensor.measure_distance()

    balls_service.report_balls_height(balls_height = balls_height)


if __name__ == '__main__':
  from RPi import GPIO

  from golferis.db.session import session_provider
  from golferis.logging    import CheckBallsHeightLogger

  ECHO_PIN    = 15
  TRIGGER_PIN = 4

  try:
    logger = CheckBallsHeightLogger()
    logger.script_start()

    GPIO.setmode(GPIO.BCM)

    with session_provider() as db:
      balls_service = BallsService(db, logger = logger)

      run(trigger_pin = TRIGGER_PIN, echo_pin = ECHO_PIN, balls_service = balls_service)

  except KeyboardInterrupt:
    logger.script_keyboard_interrupt()

  except:
    logger.script_error()
