#!/usr/bin/env python3
import sys

from loguru import logger

from golferis            import crud
from golferis.schemas    import UserCreate
from golferis.db.session import session_provider

logger.remove()
logger.add(sys.stdout, format = '{message}')

def run(*, email: str, password: str):
  user = UserCreate(email = email, is_active = True, is_superuser = True, password = password)

  with session_provider() as db:
    try:
      db_user = crud.user.create(db, obj_in = user)
    except crud.user.EmailAlreadyUsed:
      sys.exit(f'Email "{user.email}" is already registered')

    logger.info(f'Superuser with email "{db_user.email}" was successfully created')


if __name__ == '__main__':
  import argparse, getpass

  parser = argparse.ArgumentParser()
  parser.add_argument('email',  help = 'superuser email', type = str)

  args     = parser.parse_args()
  password = getpass.getpass()

  run(email = args.email, password = password)
