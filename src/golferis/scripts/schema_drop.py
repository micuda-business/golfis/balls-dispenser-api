#!/usr/bin/env python3
from golferis.db        import engine
from golferis.db.models import ModelBase
from golferis.logging   import GenericScriptLogger

SCRIPT_NAME = 'Drop Schema'

logger = GenericScriptLogger(script_name = SCRIPT_NAME)

def run():
  ModelBase.metadata.drop_all(bind = engine)

  logger.info(f'Dropping schema was successfull')


if __name__ == '__main__':
  logger.script_start()

  try:
    run()

  except KeyboardInterrupt:
    logger.script_keyboard_interrupt()

  except:
    logger.script_error()
