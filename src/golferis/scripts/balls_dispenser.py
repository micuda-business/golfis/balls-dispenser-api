#!/usr/bin/env python3
import time
from typing import TYPE_CHECKING

from golferis.utils import string

if TYPE_CHECKING:
  from hwberry.relay import Relay
  from hwberry.lcd   import LCD

  from golferis.services.balls_dispenser_service import BallsDispenserService

def run(lcd:                     'LCD',
        relay:                   'Relay',
        balls_dispenser_service: 'BallsDispenserService',
        card_number:             str):
  dispense_result = balls_dispenser_service.dispense_balls(card_number)
  message         = string.strip_accents(dispense_result.detail)

  lcd.print(lines     = message.split('\n'),
            trim_text = True)

  if dispense_result.is_success:
    with relay:
      relay.on()
      time.sleep(1.5)
      relay.off()

  time.sleep(.5)
  lcd.clear_display()


if __name__ == '__main__':
  from RPi import GPIO

  from hwberry.lcd   import LCD
  from hwberry.rfid  import WiegandRFID
  from hwberry.relay import LowOnRelay

  from golferis.logging                          import BallsDispenserLogger
  from golferis.db.session                       import session_provider
  from golferis.services.balls_dispenser_service import BallsDispenserService

  # RFID
  PIN_0     = 0  # GPIO Pin 17 |  Green cable | Data0
  PIN_1     = 2  # GPIO Pin 27 |  White cable | Data1
  PIN_SOUND = 25 # GPIO Pin 26 | Yellow cable | Sound

  # RELAY
  RELAY_PIN = 23

  # LCD
  LCD_ADDRESS = 0x3F

  try:
    logger = BallsDispenserLogger()
    logger.script_start()

    GPIO.setmode(GPIO.BCM)

    lcd = LCD(address = LCD_ADDRESS, lines_count = 4, line_width = 20)
    lcd.clear_display()

    rfid_reader = WiegandRFID(d0_pin = PIN_0, d1_pin = PIN_1, sound_pin = PIN_SOUND)

    for bits_count, data in rfid_reader:
      with session_provider() as db:
        run(lcd                     = lcd,
            relay                   = LowOnRelay(RELAY_PIN),
            balls_dispenser_service = BallsDispenserService(db, logger = logger),
            card_number             = ''.join(map(str, data)))

  except KeyboardInterrupt:
    logger.script_keyboard_interrupt()

  except:
    logger.script_error()
