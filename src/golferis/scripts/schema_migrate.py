#!/usr/bin/env python3
from __future__ import annotations

from enum import Enum

from alembic        import command
from alembic.config import Config

from golferis.db     import engine
from golferis.config import settings

HEAD_REVISION = 'head'

class Revision:
  class Sign(Enum):
    POSITIVE = '+'
    NEGATIVE = '-'

  HEAD_REVISION_STRING = 'head'
  BASE_REVISION_STRING = 'base'

  def __init__(self, revision_string: str):
    self._sign = None

    revision_string = revision_string.lower()

    if revision_string == '':
      self._revision = self.HEAD_REVISION_STRING
      return

    if revision_string in (self.HEAD_REVISION_STRING, self.BASE_REVISION_STRING):
      self._revision = revision_string
      return

    starts_with_sign = revision_string[0] in {'+', '-'}

    if starts_with_sign:
      sign           = self.Sign(revision_string[0])
      self._revision = revision_string[1:]
    else:
      sign           = self.Sign.POSITIVE
      self._revision = revision_string

    if not self._is_specific_revision(self._revision):
      self._sign = sign

  @property
  def sign(self) -> Optional[Sign]:
    return self._sign

  @property
  def value(self):
    return self._revision if self.is_specific else f'{self._sign.value}{self._revision}'

  @property
  def is_head(self) -> bool:
    return self._revision == self.HEAD_REVISION_STRING

  @property
  def is_base(self) -> bool:
    return self._revision == self.BASE_REVISION_STRING

  @property
  def is_specific(self) -> bool:
    return self.is_head or self.is_base or self._is_specific_revision(self._revision)

  @staticmethod
  def _is_specific_revision(revision_string: str) -> bool:
    try:               revision_string.isdigit()
    except ValueError: return True
    else:              return False


def run(revision_string: str = 'head'):
  alembic_config = Config(settings.PROJECT_DIR / 'db' / 'alembic.ini')

  revision        = Revision(revision_string)
  alembic_command = command_for_revision(revision)

  with engine.begin() as connection:
    alembic_config.attributes['connection'] = connection

    alembic_command(alembic_config, revision.value)

def command_for_revision(revision: Revision):
  if   revision.is_head:                                                return command.upgrade
  elif revision.is_base:                                                return command.downgrade
  elif revision.is_specific or revision.sign is Revision.Sign.POSITIVE: return command.upgrade
  else:                                                                 return command.downgrade


if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('revision',
                      default  = Revision.HEAD_REVISION_STRING,
                      help     = 'revision number or relative migration identifier',
                      type     = str)

  args = parser.parse_args()

  run(revision_string = args.revision)
