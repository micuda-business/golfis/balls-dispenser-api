from __future__ import annotations

import sys, copy, logging
from typing import TYPE_CHECKING

from loguru import logger

from golferis.config import settings

if TYPE_CHECKING:
  from loguru._logger import Logger
  from micuda.utils   import ResultObject
  from golferis.db    import models

# remove default handler because it is not pickleable
logger.remove()

_logger_without_handlers = copy.deepcopy(logger)

def create_logger(*, with_default_handler = False) -> Logger:
  logger = copy.deepcopy(_logger_without_handlers)

  if with_default_handler: logger.add(sys.stderr)

  return logger

# add the default handler
logger.add(sys.stderr)

class InterceptHandler(logging.Handler):
  """
  Default handler from examples in Loguru documentation.
  See https://loguru.readthedocs.io/en/stable/overview.html#entirely-compatible-with-standard-logging
  """
  def __init__(self, logger: Logger = logger, **kwargs):
    super().__init__(**kwargs)

    self._logger = logger

  def emit(self, record):
    # get corresponding Loguru level if it exists
    try:
      level = self._logger.level(record.levelname).name
    except ValueError:
      level = record.levelno

    # find caller from where originated the logged message
    frame, depth = logging.currentframe(), 2
    while frame.f_code.co_filename == logging.__file__:
      frame  = frame.f_back
      depth += 1

    self._logger \
      .opt(depth = depth, exception = record.exc_info) \
      .log(level, record.getMessage())

# set Loguru for handling of root logging logger
# logging.basicConfig(handlers = [InterceptHandler()], level = 0)

class ScriptLoggerMixin:
  SCRIPT_NAME = None

  def script_start(self):
    self.logger.info(f'{self.SCRIPT_NAME} script started')

  def script_keyboard_interrupt(self):
    self.logger.info(f'{self.SCRIPT_NAME} script stopped by user')

  def script_error(self):
    self.logger.exception(f'{self.SCRIPT_NAME} script fails')

class GenericScriptLogger(ScriptLoggerMixin):
  def __init__(self, script_name: str, logger: Logger = logger):
    self.logger      = logger
    self.SCRIPT_NAME = script_name

  def info(self, message: str):
    self.logger.info(message)

class CheckBallsHeightLogger(ScriptLoggerMixin):
  SCRIPT_NAME = 'Check Balls Height'

  logger = create_logger(with_default_handler = True)
  logger.add(
    settings.LOG_DIR / 'check_balls_height' / 'check_balls_height.log',
    enqueue     = True,
    rotation    = '1 weeks',
    compression = 'gz'
  )

  def report_balls_height(self, balls_height: int):
    self.logger.info(f'Reporting ball height: {balls_height}cm')

  def notify_managers(self, balls_height: int, result: ResultObject):
    if result.is_success:
      self.logger.info(f'Managers were successfully notified about balls height ({balls_height}cm)')
    else:
      self.logger \
        .warning(f'Managers were not notified about balls height ({balls_height}cm) due to: '
                 f'{result.detail}')

class BallsDispenserLogger(ScriptLoggerMixin):
  SCRIPT_NAME = 'Balls Dispenser'

  logger = create_logger(with_default_handler = True)
  logger.add(
    settings.LOG_DIR / 'balls_dispenser' / 'balls_dispenser.log',
    enqueue     = True,
    rotation    = '0:00',
    compression = 'gz'
  )

  def unknown_or_deactivated_player(self, card_number: str, player: models.Player = None):
    player_status = 'Unknown player'

    if player:
      player_desc   = self._player_description(player)
      player_status = f'Unknown or probably deactivated player {player_desc}'

    self.logger.info(f'{player_status} for card number "{card_number}" attempts dispense balls')

  def no_credit_available(self, card_number: str, player: models.Player):
    player_desc = self._player_description(player)

    self.logger.info(f'Player {player_desc} for card number "{card_number}" '
                     f'attempting dispense balls has no credit')

  def notify_player(self, card_number: str, player: models.Player, result: ResultObject):
    player_desc = self._player_description(player)
    player_info = f'Player {player_desc} for card number "{card_number}"'

    if result.is_success:
      self.logger.info(f'{player_info} was successfully notified about current credit status')
    else:
      self.logger \
        .warning(f'{player_info} was not notified about current credit status due to: '
                 f'{result.detail}')

  def dispense_balls_allowed(self, card_number: str, player: models.Player, credits_left: int):
    player_desc = self._player_description(player)

    self.logger \
      .info(f'Dispensing balls for player {player_desc}, card number {card_number}, was allowed. '
            f'Remaining credits: {credits_left}')

  @staticmethod
  def _player_description(player: models.Player) -> str:
    description = []
    golfer_id   = player.golfer_id or ''

    if player_fullname := player.fullname:
      description.append(f'{player_fullname}')

      if golfer_id:
        description.append(f'({golfer_id})')

    elif golfer_id:
      description.append(f'{golfer_id}')

    description.append(f'[ID: {player.id}]')

    return ' '.join(description)
