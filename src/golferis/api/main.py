import os
from functools import partial

from fastapi                 import FastAPI, Depends
from fastapi_request_logging import request_logging_payload, request_logging_middleware

from .            import logging # just for initialization api logging
from .core        import settings
from .versions.v1 import api

def create_app(environment: str = os.getenv("GOLFERIS_ENVIRONMENT")):
  app = FastAPI(
    title = settings.PROJECT_NAME, openapi_url = f"{settings.API_V1_STR}/openapi.json"
  )

  include_main_router = partial(app.include_router, api.api_router, prefix = settings.API_V1_STR)

  if environment == 'testing':
    include_main_router()

  else:
    app.middleware('http')(request_logging_middleware)

    include_main_router(dependencies = [Depends(request_logging_payload)])

  return app

app = create_app()
