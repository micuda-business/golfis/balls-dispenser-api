import logging

from fastapi.logger import logger as fastapi_logger

from golferis.api.core.config import settings
from golferis.logging         import create_logger, InterceptHandler

API_LOGS_DIR = settings.LOG_DIR / 'api' # TODO consider moving to API settings

fastapi_loguru_logger   = create_logger(with_default_handler = True)
fastapi_logger.handlers = [InterceptHandler(logger = fastapi_loguru_logger)]

fastapi_loguru_logger.add(
  API_LOGS_DIR / 'requests.log',
  level       = 'INFO',
  enqueue     = True,
  backtrace   = True,
  rotation    = '1 weeks',
  retention   = '3 months',
  compression = 'gz'
)

fastapi_loguru_logger.add(
  API_LOGS_DIR / 'requests_error.log',
  level       = 'ERROR',
  enqueue     = True,
  backtrace   = True,
  rotation    = '1 weeks',
  retention   = '3 months',
  compression = 'gz'
)

uvicorn_loguru_logger                 = create_logger()
logging.getLogger('uvicorn').handlers = [InterceptHandler(logger = uvicorn_loguru_logger)]

uvicorn_loguru_logger.add(
  API_LOGS_DIR / 'uvicorn.log',
  level       = 'INFO',
  enqueue     = True,
  backtrace   = True,
  rotation    = '1 weeks',
  retention   = '3 months',
  compression = 'gz'
)
