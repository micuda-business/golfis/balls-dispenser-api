from typing import Any

from fastapi import HTTPException, status

class HTTP400BadRequest(HTTPException):
  def __init__(self, detail: str, headers: dict = None):
    super().__init__(status_code = status.HTTP_400_BAD_REQUEST, detail = detail, headers = headers)

def HTTP400InactiveUser(headers: dict = None) -> HTTP400BadRequest:
  return HTTP400BadRequest(detail = 'Inactive user', headers = headers)

class HTTP401Unauthorized(HTTPException):
  def __init__(self,
               detail:  str  = "Invalid authentication credentials",
               headers: dict = None):
    if headers is None: headers = { 'WWW-Authenticate': 'Bearer' }

    super().__init__(status_code = status.HTTP_401_UNAUTHORIZED, detail = detail, headers = headers)

class HTTP403Forbidden(HTTPException):
  def __init__(self,
               detail:  str  = "The user doesn't have enough privileges",
               headers: dict = None):
    super().__init__(status_code = status.HTTP_403_FORBIDDEN, detail = detail, headers = headers)

class HTTP404NotFound(HTTPException):
  def __init__(self, detail: str = 'Not found', headers: dict = None):
    super().__init__(status_code = status.HTTP_404_NOT_FOUND, detail = detail, headers = headers)

  @classmethod
  def for_resource(cls, *, name: str):
    return cls(detail = f'{name} not found')

class HTTP412PreconditionFailed(HTTPException):
  def __init__(self, detail: str, headers: dict = None):
    super().__init__(status_code = status.HTTP_412_PRECONDITION_FAILED,
                     detail      = detail,
                     headers     = headers)
