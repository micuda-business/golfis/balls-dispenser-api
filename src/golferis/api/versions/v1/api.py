from fastapi import APIRouter, Depends, status

from golferis          import schemas
from golferis.api.deps import get_current_active_superuser

from .endpoints import auth, players, cards, balls_height, balls_dispenser, users

responses = {
  status.HTTP_403_FORBIDDEN: {
    'model':       schemas.ResponseDetail,
    'description': "Could not validate credentials or the user doesn't have enough privileges"
  }
}

api_router = APIRouter()
api_router.include_router(auth.router, prefix = '/auth', tags = ['auth'])
api_router.include_router(users.router,
                          prefix       = '/users',
                          tags         = ['users'],
                          dependencies = [Depends(get_current_active_superuser)],
                          responses    = responses)
api_router.include_router(players.router,
                          prefix       = '/players',
                          tags         = ['players'],
                          dependencies = [Depends(get_current_active_superuser)],
                          responses    = responses)
api_router.include_router(cards.router,
                          prefix       = '/cards',
                          tags         = ['cards'],
                          dependencies = [Depends(get_current_active_superuser)],
                          responses    = responses)
api_router.include_router(balls_height.router,
                          prefix       = '/balls-height',
                          tags         = ['balls-height'],
                          dependencies = [Depends(get_current_active_superuser)],
                          responses    = responses)
api_router.include_router(balls_dispenser.router,
                          prefix       = '/balls-dispenser',
                          tags         = ['balls-dispenser'],
                          dependencies = [Depends(get_current_active_superuser)],
                          responses    = responses)
