from uuid import UUID

from sqlalchemy.orm import Session
from fastapi        import Depends, status

from golferis                 import crud, schemas
from golferis.api.deps        import get_db
from golferis.services.player import PlayersService
from golferis.api.exceptions  import HTTP400BadRequest, HTTP412PreconditionFailed

from .base import HTTP404PlayerNotFound, router, default_responses

def HTTP400CardNotExistOrIsDeactivated(headers: dict = None) -> HTTP400BadRequest:
  return HTTP400BadRequest(detail = 'Provided card does not exist or is not active', headers = headers)

@router.get(
  '/credit/{player_id}',
  response_model = int,
  responses      = { **default_responses }
)
def read_credit(player_id: UUID, db: Session = Depends(get_db)):
  try:
    return crud.player.get_credit_amount(db, player_id = player_id)
  except crud.player.PlayerNotFound:
    raise HTTP404PlayerNotFound()

@router.post(
  '/credit',
  response_model = schemas.CreditAmount,
  responses      = {
    **default_responses,
    status.HTTP_412_PRECONDITION_FAILED: {
      'model':       schemas.ResponseDetail,
      'description': 'Cannot purchase credit, player is deactivated'
    }
  }
)
def purchase_credit(purchase_credit: schemas.PurchaseCredit, db: Session = Depends(get_db)):
  try:
    crud.player.purchase_credit(db, obj_in = purchase_credit)
  except crud.player.PlayerNotFound:
    raise HTTP404PlayerNotFound()
  except crud.player.PlayerIsDeactivated:
    raise HTTP412PreconditionFailed(detail = 'Cannot purchase credit, player is deactivated')
  except (crud.player.CardNotFound, crud.player.CardIsDeactivated):
    raise HTTP400CardNotExistOrIsDeactivated()

  return schemas.CreditAmount(
    player_id     = purchase_credit.player_id,
    credits_count = crud.player.get_credit_amount(db, player_id = purchase_credit.player_id)
  )

@router.delete(
  '/credit',
  response_model = schemas.CreditAmount,
  responses      = {
    **default_responses,
    status.HTTP_412_PRECONDITION_FAILED: {
      'model':       schemas.ResponseDetail,
      'description': 'Cannot spend credit, player is deactivated'
    }
  }
)
def spend_credit(spend_credit: schemas.SpendCredit, db: Session = Depends(get_db)):
  try:
    crud.player.spend_credit(db, obj_in = spend_credit)
  except crud.player.PlayerNotFound:
    raise HTTP404PlayerNotFound()
  except (crud.player.CardNotFound, crud.player.CardIsDeactivated):
    raise HTTP400CardNotExistOrIsDeactivated()
  except crud.player.PlayerIsDeactivated:
    raise HTTP412PreconditionFailed(detail = 'Cannot spend credit, player is deactivated')
  except crud.player.PlayerHasNoCredit:
    raise HTTP412PreconditionFailed(detail = 'Cannot spend credit, player has no credit')

  return schemas.CreditAmount(
    player_id     = spend_credit.player_id,
    credits_count = crud.player.get_credit_amount(db, player_id = spend_credit.player_id)
  )
