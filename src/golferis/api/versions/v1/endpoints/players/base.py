from functools import partial

from fastapi import APIRouter, status

from golferis                import schemas
from golferis.api.exceptions import HTTP404NotFound

HTTP404PlayerNotFound = partial(HTTP404NotFound.for_resource, name = 'Player')

default_responses = { status.HTTP_404_NOT_FOUND: { 'model':       schemas.ResponseDetail,
                                                   'description': 'Player not found' } }

router = APIRouter()
