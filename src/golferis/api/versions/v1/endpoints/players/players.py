from uuid   import UUID
from typing import List

from sqlalchemy.orm import Session
from fastapi        import Depends, status

from golferis                 import crud, schemas
from golferis.api.deps        import get_db, Paginator
from golferis.services.player import PlayersService
from golferis.api.exceptions  import HTTP400BadRequest, HTTP412PreconditionFailed

from .base import HTTP404PlayerNotFound, router, default_responses

@router.post(
  '/',
  response_model = schemas.Player,
  status_code    = status.HTTP_201_CREATED,
  responses      = {
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': "Email is already registered" }
  }
)
def create_player(player: schemas.PlayerCreate, db: Session = Depends(get_db)):
  try:
    return crud.player.create(db, obj_in = player)
  except crud.player.EmailAlreadyUsed:
    raise HTTP400BadRequest(detail = "Email is already registered")

@router.get('/', response_model = schemas.PlayerPage)
def read_players(paginator: Paginator = Depends(), db: Session = Depends(get_db)):
  return paginator.paginate(crud.player)

# TODO: returns the Page object
@router.get('/incomplete-profile', response_model = List[schemas.Player])
def read_players_with_incomplete_profile(db: Session = Depends(get_db)):
  players_service = PlayersService()

  return players_service.get_players_with_incomplete_profile(db)

@router.get(
  '/{player_id}',
  response_model = schemas.Player,
  responses      = { **default_responses }
)
def read_player(player_id: UUID, db: Session = Depends(get_db)):
  db_player = crud.player.get(db, player_id)

  if db_player is None: raise HTTP404PlayerNotFound()

  return db_player

@router.put(
  '/{player_id}',
  response_model = schemas.Player,
  responses      = {
    **default_responses,
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': "Email is already registered" }
  }
)
def update_player(player_id: UUID, player: schemas.PlayerUpdate, db: Session = Depends(get_db)):
  db_player = crud.player.get(db, player_id)

  if db_player is None: raise HTTP404PlayerNotFound()

  try:
    return crud.player.update(db, db_obj = db_player, obj_in = player)
  except crud.player.EmailAlreadyUsed:
    raise HTTP400BadRequest(detail = "Email is already registered")

@router.post(
  '/{player_id}/activation',
  status_code = status.HTTP_204_NO_CONTENT,
  responses   = {
    **default_responses,
    status.HTTP_412_PRECONDITION_FAILED: { 'model':       schemas.ResponseDetail,
                                           'description': 'Player is already activated' }
  }
)
def activate_player(*, player_id: UUID, include_cards: bool = False, db: Session = Depends(get_db)):
  db_player = crud.player.get(db, player_id)

  if db_player is None: raise HTTP404PlayerNotFound()

  players_service = PlayersService()

  try:
    players_service.activate_player(db, db_player = db_player, include_cards = include_cards)

  except PlayersService.PlayerAlreadyActivated:
    raise HTTP412PreconditionFailed(detail = "Player is already activated")

@router.delete(
  '/{player_id}/activation',
  status_code = status.HTTP_204_NO_CONTENT,
  responses   = {
    **default_responses,
    status.HTTP_412_PRECONDITION_FAILED: { 'model':       schemas.ResponseDetail,
                                           'description': 'Player is already deactivated' }
  }
)
def deactivate_player(*, player_id: UUID, db: Session = Depends(get_db)):
  db_player = crud.player.get(db, player_id)

  if db_player is None: raise HTTP404PlayerNotFound()

  players_service = PlayersService()

  try:
    players_service.deactivate_player(db, db_player = db_player)

  except PlayersService.PlayerAlreadyDeactivated:
    raise HTTP412PreconditionFailed(detail = "Player is already deactivated")
