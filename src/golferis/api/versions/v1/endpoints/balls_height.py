from sqlalchemy.orm import Session
from fastapi        import APIRouter, Depends, status

from golferis                import crud, schemas
from golferis.api.deps       import get_db, Paginator
from golferis.api.exceptions import HTTP400BadRequest, HTTP404NotFound

router = APIRouter()

@router.get('/log', response_model = schemas.BallsHeightLogPage)
def read_log(paginator: Paginator = Depends()):
  return paginator.paginate(crud.balls_height_log)

@router.get('/config', response_model = schemas.BallsHeightConfig)
def read_config(db: Session = Depends(get_db)):
  return crud.balls_height_config.get_config(db)

@router.post(
  '/config',
  response_model = schemas.BallsHeightConfig,
  status_code    = status.HTTP_201_CREATED,
  responses      = {
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': 'Config for balls height script already exists' }
  }
)
def create_config(config: schemas.BallsHeightConfigCreate, db: Session = Depends(get_db)):
  try:
    return crud.balls_height_config.create(db, obj_in = config)
  except crud.balls_height_config.ConfigAlreadyExists:
    raise HTTP400BadRequest(detail = 'Config for balls height script already exists')

@router.put(
  '/config',
  response_model = schemas.BallsHeightConfig,
  responses      = {
    status.HTTP_404_NOT_FOUND: { 'model':       schemas.ResponseDetail,
                                 'description': 'Config for balls height script not found' }
  }
)
def update_config(config: schemas.BallsHeightConfigUpdate, db: Session = Depends(get_db)):
  try:
    return crud.balls_height_config.update(db, obj_in = config.dict())
  except crud.balls_height_config.BallsHeightConfigNotFound:
    raise HTTP404NotFound(detail = 'Config for balls height script not found')
