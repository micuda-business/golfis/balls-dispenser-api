from uuid import UUID

from sqlalchemy.orm import Session
from fastapi        import APIRouter, Depends, HTTPException, status

from golferis                import crud, schemas, services
from golferis.api.deps       import get_db, Paginator
from golferis.api.exceptions import HTTP400BadRequest, HTTP404NotFound, HTTP412PreconditionFailed

router = APIRouter()

card_not_found_reponse = {
  status.HTTP_404_NOT_FOUND: { 'model':       schemas.ResponseDetail,
                              'description': 'Card not found' }
}

@router.post(
  '/',
  response_model = schemas.Card,
  status_code    = status.HTTP_201_CREATED,
  responses      = {
    status.HTTP_400_BAD_REQUEST:         { 'model':       schemas.ResponseDetail,
                                           'description': 'Player does not exist' },
    status.HTTP_412_PRECONDITION_FAILED: { 'model':       schemas.ResponseDetail,
                                           'description': 'Player already owns this card' }
  }
)
def create_card(card: schemas.CardCreate, db: Session = Depends(get_db)):
  try:
    return crud.card.create(db, obj_in = card)
  except crud.player.PlayerNotFound:
    raise HTTP400BadRequest(detail = 'Player does not exist')
  except crud.card.PlayerAlreadyOwnsCard:
    raise HTTP412PreconditionFailed(detail = 'Player already owns this card')

@router.get('/', response_model = schemas.CardPage)
def read_cards(paginator: Paginator = Depends(), db: Session = Depends(get_db)):
  return paginator.paginate(crud.card)

@router.get(
  '/{card_id}',
  response_model = schemas.Card,
  responses      = { **card_not_found_reponse }
)
def read_card(card_id: UUID, db: Session = Depends(get_db)):
  db_card = crud.card.get(db, id = card_id)

  if db_card is None: raise HTTP404NotFound.for_resource(name = 'Card')

  return db_card

@router.post(
  '/{card_id}/activation',
  status_code = status.HTTP_204_NO_CONTENT,
  responses   = {
    **card_not_found_reponse,
    status.HTTP_412_PRECONDITION_FAILED: {
      'model':       schemas.ResponseDetail,
      'description': 'Player already owns this card or card is already activated'
    }
  }
)
def activate_card(*, card_id: UUID, db: Session = Depends(get_db)):
  db_card = crud.card.get(db, id = card_id)

  if db_card is None: raise HTTP404NotFound.for_resource(name = 'Card')

  try:
    services.card.activate_card(db, db_card = db_card)
  except crud.card.PlayerAlreadyOwnsCard:
    raise HTTP412PreconditionFailed(detail = 'Player already owns this card')
  except services.card.CardAlreadyActivated:
    raise HTTP412PreconditionFailed(detail = 'Card is already activated')

@router.delete(
  '/{card_id}/activation',
  status_code = status.HTTP_204_NO_CONTENT,
  responses   = {
    **card_not_found_reponse,
    status.HTTP_412_PRECONDITION_FAILED: {
      'model':       schemas.ResponseDetail,
      'description': 'Player already owns this card or card is already deactivated'
    }
  }
)
def deactivate_card(*, card_id: UUID, db: Session = Depends(get_db)):
  db_card = crud.card.get(db, id = card_id)

  if db_card is None: raise HTTP404NotFound.for_resource(name = 'Card')

  try:
    services.card.deactivate_card(db, db_card = db_card)
  except services.card.CardAlreadyDeactivated:
    raise HTTP412PreconditionFailed(detail = 'Card is already deactivated')
