from datetime import timedelta
from typing   import Any

from fastapi          import APIRouter, Body, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm   import Session

from golferis                import crud, schemas
from golferis.api.deps       import get_db
from golferis.api.core       import security, settings
from golferis.api.exceptions import HTTP400BadRequest, HTTP400InactiveUser

router = APIRouter()

@router.post(
  '/access-token',
  response_model = schemas.Token,
  responses = {
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': 'Incorrect email or password, or inactive user' }
  }
)
def get_access_token(db:        Session                   = Depends(get_db),
                     form_data: OAuth2PasswordRequestForm = Depends()) -> Any:
  """
  OAuth2 compatible token login, get an access token for future requests
  """
  user = crud.user.authenticate(db, email = form_data.username, password = form_data.password)

  if   not user:           raise HTTP400BadRequest(detail = "Incorrect email or password")
  elif not user.is_active: raise HTTP400InactiveUser()

  access_token_expires = timedelta(minutes = settings.ACCESS_TOKEN_EXPIRE_MINUTES)

  return {
    'access_token': security.create_access_token(user.id, expires_delta = access_token_expires),
    'token_type':   'bearer'
  }
