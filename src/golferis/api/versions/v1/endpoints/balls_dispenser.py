from sqlalchemy.orm import Session
from fastapi        import APIRouter, Depends, status

from golferis                import crud, schemas
from golferis.api.deps       import get_db
from golferis.api.exceptions import HTTP400BadRequest, HTTP404NotFound

router = APIRouter()

@router.get(
  '/config',
  response_model = schemas.BallsDispenserConfig
)
def read_config(db: Session = Depends(get_db)):
  return crud.balls_dispenser_config.get_config(db)

@router.post(
  '/config',
  response_model = schemas.BallsDispenserConfig,
  status_code    = status.HTTP_201_CREATED,
  responses      = {
    status.HTTP_400_BAD_REQUEST: {
      'model':       schemas.ResponseDetail,
      'description': 'Config for balls dispenser script already exists'
    }
  }
)
def create_config(config: schemas.BallsDispenserConfigCreate, db: Session = Depends(get_db)):
  try:
    return crud.balls_dispenser_config.create(db, obj_in = config)
  except crud.balls_dispenser_config.ConfigAlreadyExists:
    raise HTTP400BadRequest(detail = 'Config for balls dispenser script already exists')

@router.put(
  '/config',
  response_model = schemas.BallsDispenserConfig,
  responses      = {
    status.HTTP_404_NOT_FOUND: {
      'model':       schemas.ResponseDetail,
      'description': 'Config for balls dispenser script not found'
    }
  }
)
def update_config(config: schemas.BallsDispenserConfigUpdate, db: Session = Depends(get_db)):
  try:
    return crud.balls_dispenser_config.update(db, obj_in = config.dict())
  except crud.balls_dispenser_config.BallsDispenserConfigNotFound:
    raise HTTP404NotFound(detail = 'Config for balls dispenser script not found')
