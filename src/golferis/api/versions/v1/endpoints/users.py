from uuid import UUID

from sqlalchemy.orm import Session
from fastapi        import APIRouter, Depends, HTTPException, status

from golferis                import crud, schemas
from golferis.api.deps       import get_db, Paginator
from golferis.api.exceptions import HTTP400BadRequest, HTTP404NotFound

router = APIRouter()

@router.post(
  '/',
  response_model = schemas.User,
  status_code    = status.HTTP_201_CREATED,
  responses      = {
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': 'Email is already registered' }
  }
)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
  try:
    return crud.user.create(db, obj_in = user)
  except crud.user.EmailAlreadyUsed:
    raise HTTP400BadRequest(detail = "Email is already registered")

@router.get('/', response_model = schemas.UserPage)
def read_users(paginator: Paginator = Depends()):
  return paginator.paginate(crud.user)

@router.get(
  '/{user_id}',
  response_model = schemas.User,
  responses      = {
    status.HTTP_404_NOT_FOUND: { 'model':       schemas.ResponseDetail,
                                 'description': 'User not found' }
  }
)
def read_user(user_id: UUID, db: Session = Depends(get_db)):
  db_user = crud.user.get(db, user_id)

  if db_user is None: raise HTTP404NotFound.for_resource(name = 'User')

  return db_user

@router.put(
  '/{user_id}',
  response_model = schemas.User,
  responses      = {
    status.HTTP_400_BAD_REQUEST: { 'model':       schemas.ResponseDetail,
                                   'description': 'Email is already registered' },
    status.HTTP_404_NOT_FOUND:   { 'model':       schemas.ResponseDetail,
                                   'description': 'User not found' }
  }
)
def update_user(user_id: UUID, user: schemas.UserUpdate, db: Session = Depends(get_db)):
  db_user = crud.user.get(db, user_id)

  if db_user is None: raise HTTP404NotFound.for_resource(name = 'User')

  try:
    return crud.user.update(db, db_obj = db_user, obj_in = user)
  except crud.user.EmailAlreadyUsed:
    raise HTTP400BadRequest(detail = "Email is already registered")
