import secrets

from golferis.config import Settings as BaseSettings

class Settings(BaseSettings):
  PROJECT_NAME:                str
  API_V1_STR:                  str = "/api/v1"
  ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
  SECRET_KEY:                  str = secrets.token_urlsafe(32)

settings = Settings()
