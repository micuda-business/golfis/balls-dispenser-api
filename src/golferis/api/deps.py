from typing import Generator, Optional

from jose             import jwt
from pydantic         import ValidationError
from fastapi          import Query, Depends, Request, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm   import Session

from golferis                import schemas, crud
from golferis.db             import models
from golferis.api.core       import security, settings
from golferis.crud.base      import CRUDBase
from golferis.db.session     import session_provider
from golferis.api.exceptions import (
  HTTP401Unauthorized, HTTP400BadRequest, HTTP400InactiveUser, HTTP403Forbidden, HTTP404NotFound
)

def get_db() -> Generator:
  with session_provider() as db:
    yield db

reusable_oauth2 = OAuth2PasswordBearer(tokenUrl = f'{settings.API_V1_STR}/auth/access-token')

def get_current_user(db:    Session = Depends(get_db),
                     token: str     = Depends(reusable_oauth2)) -> models.User:
  try:
    payload    = jwt.decode(token, settings.SECRET_KEY, algorithms = [security.ALGORITHM])
    token_data = schemas.TokenPayload(**payload)

  except (jwt.JWTError, ValidationError):
    raise HTTP403Forbidden(detail = "Could not validate credentials")

  user = crud.user.get(db, id = token_data.sub)

  if not user: raise HTTP401Unauthorized

  return user

def get_current_active_user(current_user: models.User = Depends(get_current_user)) -> models.User:
  if not current_user.is_active: raise HTTP400InactiveUser()

  return current_user

def get_current_active_superuser(
  current_user: models.User = Depends(get_current_active_user)
) -> models.User:
  if not current_user.is_superuser: raise HTTP403Forbidden

  return current_user

class Paginator:
  default_offset = 0
  default_limit  = 100
  max_offset     = None
  max_limit      = 1000

  def __init__(self,
               request: Request,
               db:      Session = Depends(get_db),
               offset:  int     = Query(default_offset, ge = 0, le = max_offset),
               limit:   int     = Query(default_limit,  ge = 1, le = max_limit)):
    self._db      = db
    self._limit   = limit
    self._offset  = offset
    self._request = request

  def get_count(self, crud: CRUDBase) -> int:
    return crud.get_count(self._db)

  def get_previous_url(self) -> Optional[str]:
    if self._offset <= 0:
      return None

    if self._offset - self._limit <= 0:
      return str(self._request.url.remove_query_params(keys = ['offset']))

    return str(self._request.url.include_query_params(limit  = self._limit,
                                                      offset = self._offset - self._limit))

  def get_next_url(self, count: int) -> Optional[str]:
    if self._offset + self._limit >= count:
      return None

    return str(self._request.url.include_query_params(limit  = self._limit,
                                                      offset = self._offset + self._limit))

  def paginate(self, crud: CRUDBase) -> dict:
    records       = crud.get_multi(self._db, skip = self._offset, limit = self._limit)
    records_count = self.get_count(crud)

    return { 'count':    records_count,
             'result':   records,
             'previous': self.get_previous_url(),
             'next':     self.get_next_url(records_count) }
