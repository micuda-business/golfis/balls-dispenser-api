from typing   import Optional
from datetime import datetime

from sqlalchemy.orm import Session
from micuda.utils   import Success, Failure

from golferis                      import crud, schemas
from golferis.db.models            import BallsHeightLog
from golferis.logging              import CheckBallsHeightLogger
from golferis.utils.template       import render_template
from golferis.services.sms_service import SMSService

CURRENT_DATETIME_CONTEXT_VAR_NAME = 'current_datetime'

class BallsService:
  def __init__(self,
               db:          Session,
               sms_service: SMSService             = None,
               logger:      CheckBallsHeightLogger = None):
    self._db          = db
    self._logger      = logger      or CheckBallsHeightLogger()
    self._sms_service = sms_service or SMSService(logger = self._logger.logger)

  def report_balls_height(self, *, balls_height: int) -> BallsHeightLog:
    self._logger.report_balls_height(balls_height)

    notes             = None
    notification_made = False

    if self._should_notify(balls_height):
      notification_result = self._notify_managers()

      notification_made = notification_result.is_success
      notes             = notification_result.detail

      self._logger.notify_managers(balls_height, notification_result)

    balls_height_log_create = schemas.BallsHeightLogCreate(
      measured_height = balls_height, notification_made = notification_made, notes = notes
    )

    return crud.balls_height_log.create(self._db, obj_in = balls_height_log_create)

  def _notify_managers(self):
    if managers_phone_numbers := crud.user.get_active_managers_phone_numbers(self._db):
      message    = self._notication_message()
      sms_result = self._sms_service.send_message_to(*managers_phone_numbers, message = message)

      if sms_result.is_success:
        detail = f'''Message: {message}\nSent to: {', '.join(managers_phone_numbers)}'''

        return Success(detail = detail)

      return sms_result

    return Failure(detail = 'There is no golf course manager with phone number.')

  def _should_notify(self, balls_height) -> Optional[bool]:
    return balls_height >= crud.balls_height_config.get_config(self._db).notify_at_height

  def _notication_message(self) -> Optional[str]:
    return render_template(crud.balls_height_config.get_config(self._db).notification_text,
                           context = self._notication_message_context())

  def _notication_message_context(self):
    return {
      CURRENT_DATETIME_CONTEXT_VAR_NAME: datetime.now().strftime('%d.%m.%Y %H:%M:%S')
    }
