from __future__ import annotations

from typing import List, TYPE_CHECKING

import requests
from loguru       import logger
from micuda.utils import Success, Failure

from golferis.config import settings

if TYPE_CHECKING:
  from loguru._logger import Logger
  from micuda.utils   import ResultObject

class SMSService:
  def __init__(self, logger: Logger = logger):
    self._logger = logger

  def send_message_to(self, *phone_numbers: List[str], message: str) -> ResultObject:
    results = [self._do_send_message_to(phone_number, message) for phone_number in phone_numbers]

    if failures := list(filter(lambda ro: ro.is_failure, results)):
      failure = Failure(detail = [ro.detail for ro in failures])

      self._log_failure(failure)

      return failure

    self._log_success(phone_numbers)

    return Success()

  def _do_send_message_to(self, phone_number, message):
    payload = {
      'number':   phone_number,
      'messages': message,
      'user':     settings.SMS_GATEWAY_USERNAME,
      'pass':     settings.SMS_GATEWAY_PASSWORD
    }

    response = requests.get(settings.SMS_GATEWAY_URI, params = payload)

    try:
      response.raise_for_status()

    except requests.exceptions.HTTPError as e:
      return Failure(detail = e)

    return Success()

  def _log_failure(self, failure: Failure):
    self._logger.error('Cannot send SMS: {failure.detail!r}')

  def _log_success(self, phone_numbers: List[str]):
    self._logger.info('SMS was successfully sent: {phone_numbers!r}.')
