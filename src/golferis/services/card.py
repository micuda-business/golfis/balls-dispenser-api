from sqlalchemy.orm import Session

from golferis import crud
from golferis.db.models import Card

class CardAlreadyActivated(Exception):   pass
class CardAlreadyDeactivated(Exception): pass

def activate_card(db: Session, db_card: Card):
  if db_card.is_active:
    raise CardAlreadyActivated

  if crud.card.exists_active_card_for_player(
    db, player_id = db_card.owner_id, card_number = db_card.card_number
  ):
    raise crud.card.PlayerAlreadyOwnsCard

  crud.card.update(db, db_obj = db_card, obj_in = { 'is_active': True })

def deactivate_card(db: Session, db_card: Card):
  if not db_card.is_active:
    raise CardAlreadyDeactivated

  crud.card.update(db, db_obj = db_card, obj_in = { 'is_active': False })
