from typing import List

from sqlalchemy     import case
from sqlalchemy.orm import Session

from golferis.db import models

class PlayersService:
  class PlayerAlreadyDeactivated(ValueError): pass
  class PlayerAlreadyActivated(ValueError):   pass

  def get_players_with_incomplete_profile(self, db: Session) -> List[models.Player]:
    profile_weight = \
      case([(models.Player.first_name.is_(None),   1)], else_ = 0) + \
      case([(models.Player.last_name.is_(None),    1)], else_ = 0) + \
      case([(models.Player.phone_number.is_(None), 1)], else_ = 0) + \
      case([(models.Player.email.is_(None),        1)], else_ = 0)
    profile_weight.label('profile_weight')

    result = db.query(models.Player, profile_weight) \
      .filter(profile_weight != 0) \
      .order_by(profile_weight.desc(), models.Player.created_at) \
      .all()

    return [r.Player for r in result]

  def activate_player(self, db: Session, *, db_player: models.Player, include_cards = False):
    if db_player.is_active and not include_cards:
      raise self.PlayerAlreadyActivated

    db_player.is_active = True
    db.add(db_player)

    if include_cards:
      activated_cards_number = []
      # FIXME: unite logic below (only one active card with the same card_number is allowed)
      # with `crud_card.card.create`
      # TODO: btw. tests are missing for this business logic for this endpoint
      for card in db_player.owned_cards: # TODO: optimise
        if card.is_active:
          activated_cards_number.append(card.card_number)

        elif card.card_number not in activated_cards_number:
          activated_cards_number.append(card.card_number)

          card.is_active = True
          db.add(card)

    db.commit()

  def deactivate_player(self, db: Session, *, db_player: models.Player):
    if db_player.is_active is False:
      raise self.PlayerAlreadyDeactivated

    db_player.is_active = False
    db.add(db_player)

    for card in db_player.owned_cards: # TODO: optimise
      card.is_active = False
      db.add(card)

    db.commit()
