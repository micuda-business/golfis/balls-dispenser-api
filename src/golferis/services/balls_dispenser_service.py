from typing import Optional

from sqlalchemy.orm import Session
from micuda.utils   import Success, Failure, ResultObject

from golferis                      import crud, schemas
from golferis.db                   import models
from golferis.logging              import BallsDispenserLogger
from golferis.utils.template       import render_template
from golferis.services.sms_service import SMSService

FIRST_NAME_CONTEXT_VAR_NAME   = 'first_name'
LAST_NAME_CONTEXT_VAR_NAME    = 'last_name'
CREDITS_LEFT_CONTEXT_VAR_NAME = 'credits_left'

def default_player_context(player: models.Player) -> dict:
  return { FIRST_NAME_CONTEXT_VAR_NAME: player.first_name,
           LAST_NAME_CONTEXT_VAR_NAME:  player.last_name }

class UnknownPlayerResult(Failure):
  def __init__(self, config: models.BallsDispenserConfig):
    super().__init__(detail = config.unknown_player_text)

class NoCreditResult(Failure):
  def __init__(self, player: models.Player, config: models.BallsDispenserConfig):
    context = default_player_context(player)

    super().__init__(detail = render_template(config.no_credit_text, context))

class DispenseBallsAllowed(Success):
  def __init__(self, player: models.Player, config: models.BallsDispenserConfig, credits_left: int):
    context = { **default_player_context(player),
                CREDITS_LEFT_CONTEXT_VAR_NAME: credits_left }

    super().__init__(detail = render_template(config.current_credit_amount_text, context))

class BallsDispenserService:
  def __init__(self,
               db:          Session,
               sms_service: SMSService           = None,
               logger:      BallsDispenserLogger = None):
    self._db          = db
    self._logger      = logger      or BallsDispenserLogger()
    self._sms_service = sms_service or SMSService(logger = self._logger.logger)

  def dispense_balls(self, card_number: str) -> ResultObject:
    card                   = crud.card.active_card_for_card_number(self._db, card_number)
    db_balls_dispenser_log = self._dispense_balls_attempt(card_number, card)
    config                 = crud.balls_dispenser_config.get_config(self._db)
    player                 = self._player_from_dispenser_log(db_balls_dispenser_log)

    if player is None:
      self._logger.unknown_or_deactivated_player(card_number)

      return UnknownPlayerResult(config)

    spend_credit = schemas.SpendCredit(player_id = player.id, card_id = card.id, credits_count = 1)
    try:
      crud.player.spend_credit(self._db, obj_in = spend_credit)

    except (crud.player.PlayerNotFound, crud.player.PlayerIsDeactivated):
      self._logger.unknown_or_deactivated_player(card_number, player)

      return UnknownPlayerResult(config)

    except crud.player.PlayerHasNoCredit:
      self._logger.no_credit_available(card_number, player)

      return NoCreditResult(player, config)

    credit_amount = crud.player.get_credit_amount(self._db, player_id = player.id)

    if self._should_notify(credit_amount, config):
      notification_result = self._notify_player(player, config, credits_left = credit_amount)

      self._logger.notify_player(card_number, player, notification_result)

    self._logger.dispense_balls_allowed(card_number, player, credits_left = credit_amount)

    return DispenseBallsAllowed(player, config, credits_left = credit_amount)

  def _dispense_balls_attempt(
    self, card_number: str, card: Optional[models.Card]
  ) -> models.BallsDispenserLog:
    balls_dispenser_log_create = schemas.BallsDispenserLogCreate(card_number = card_number,
                                                                 card_id     = card and card.id)

    return crud.balls_dispenser_log.create(self._db, obj_in = balls_dispenser_log_create)

  def _player_from_dispenser_log(self, db_balls_dispenser_log):
    return \
      db_balls_dispenser_log \
      and db_balls_dispenser_log.card \
      and db_balls_dispenser_log.card.owner

  def _should_notify(self, credit_amount: int, config: models.BallsDispenserConfig) -> bool:
    return credit_amount in config.notify_at_credit_amount

  def _notify_player(self, player: models.Player, config: models.BallsDispenserConfig,
                     credits_left: int) -> ResultObject:
    message_context = { **default_player_context(player),
                        CREDITS_LEFT_CONTEXT_VAR_NAME: credits_left }

    message = render_template(config.player_credit_notification_text, message_context)

    if player.phone_number:
      sms_result = self._sms_service.send_message_to(player.phone_number, message = message)

      if sms_result.is_success:
        return Success()
      else:
        return Failure(detail = 'Balls Dispensing Service: Cannot send SMS to player')

    return Failure(detail = 'Player has no phone number set.')
