from pathlib import Path
from os      import getenv
from typing  import Any, Dict, Optional, List

from pydantic import BaseSettings, PostgresDsn, validator

class MySqlDsn(PostgresDsn): # TODO: move somewhere else
  allowed_schemes = { 'mysql' }

PROJECT_DIR   = Path(__file__).resolve(strict = True).parent
ENV_FILE_PATH = getenv('GOLFERIS_ENV_FILE_PATH', PROJECT_DIR / '.env')

class Settings(BaseSettings):
  PROJECT_DIR: Path = PROJECT_DIR
  LOG_DIR: Path     = PROJECT_DIR.parent / 'log'

  DATABASE_SERVER:   str
  DATABASE_USER:     str
  DATABASE_PASSWORD: str
  DATABASE_NAME:     str
  DATABASE_URI:      Optional[MySqlDsn] = None

  SMS_GATEWAY_URI:      str = 'https://smsgate.profiwh.com/'
  SMS_GATEWAY_USERNAME: str
  SMS_GATEWAY_PASSWORD: str

  BALLS_HEIGHT_NOTIFY_AT_HEIGHT_DEFAULT:  int = 100
  BALLS_HEIGHT_NOTIFICATION_TEXT_DEFAULT: str = \
    '{{ current_datetime }} - počet míčů v golfovém vydavači klesl pod stanovenou hodnotu.'

  BALLS_DISPENSER_NO_CREDIT_TEXT_DEFAULT: str = \
    'Dobrý den\n{{ last_name }} {{ first_name }},\nnemáte již kredit\nna míče.'
  BALLS_DISPENSER_UNKNOWN_PLAYER_TEXT_DEFAULT: str = \
    'Dobrý den,\nVaše identifikace\nnení rozpoznaná,\nkontaktujte recepci.'
  BALLS_DISPENSER_CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT: str = \
    'Dobrý den\n{{ last_name }} {{ first_name }},\npočet kreditů: {{ credits_left }}.'
  BALLS_DISPENSER_NOTIFY_AT_CREDIT_AMOUNT: List[int] = [3, 1, 0]
  BALLS_DISPENSER_PLAYER_CREDIT_NOTIFICATION_TEXT_DEFAULT: str = \
    'Dobrý den, {{ last_name }} {{ first_name }}, Váš aktuální počet kreditů: {{ credits_left }}'

  @validator('PROJECT_DIR', pre = True, always = True)
  def make_project_dir_value_static(cls, v):
    return PROJECT_DIR

  @validator("DATABASE_URI", pre = True)
  def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
    if isinstance(v, str):
      return v
    return MySqlDsn.build(scheme   = "mysql",
                          user     = values.get("DATABASE_USER"),
                          password = values.get("DATABASE_PASSWORD"),
                          host     = values.get("DATABASE_SERVER"),
                          path     = f"/{values.get('DATABASE_NAME', '')}",
                          query    = 'charset=utf8')

  class Config:
    case_sensitive = True
    env_file       = ENV_FILE_PATH
    env_prefix     = 'GOLFERIS_'

settings = Settings()
