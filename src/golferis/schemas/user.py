from uuid     import UUID
from typing   import Optional, List
from datetime import datetime

from pydantic import BaseModel, NoneStr, EmailStr

from .page_base import PageBase

class UserBase(BaseModel):
  phone_number:           NoneStr
  email:                  Optional[EmailStr]
  is_active:              bool               = True
  is_golf_course_manager: bool               = False
  is_superuser:           bool               = False

class UserCreate(UserBase):
  email:    EmailStr
  password: str

class UserUpdate(UserBase):
  password: NoneStr

class User(UserBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True

class UserPage(PageBase[User]):
  result: List[User]
