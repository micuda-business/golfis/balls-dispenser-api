from uuid     import UUID
from typing   import Optional
from decimal  import Decimal
from datetime import datetime

from pydantic import BaseModel, NoneStr

class CreditBase(BaseModel):
  player_id:     UUID
  credits_count: int

class CreditTransactionBase(CreditBase):
  notes:   NoneStr

class PurchaseCredit(CreditTransactionBase):
  price:     Decimal
  card_id:   Optional[UUID]
  expiry_at: Optional[datetime]

class SpendCredit(CreditTransactionBase):
  card_id: UUID

class CreditAmount(CreditBase): pass
