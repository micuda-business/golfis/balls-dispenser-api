from typing import Generic, TypeVar, Optional, List

from pydantic import BaseModel

ResultType = TypeVar('ResultType', bound = BaseModel)

# FIXME is usage of ResultType and Generic correct?
class PageBase(Generic[ResultType], BaseModel):
  count:    int
  result:   List[ResultType]
  previous: Optional[str]
  next:     Optional[str]
