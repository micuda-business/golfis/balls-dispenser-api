from uuid     import UUID
from typing   import List
from datetime import datetime

from pydantic import BaseModel, NoneStr

from .page_base import PageBase

class CardBase(BaseModel):
  card_number: str
  is_active:   bool

class CardCreate(CardBase):
  owner_id:  UUID
  holder_id: UUID

class CardUpdate(CardBase):
  card_number: NoneStr
  is_active:   bool    = False

class Card(CardBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True

class CardPage(PageBase[Card]):
  result: List[Card]
