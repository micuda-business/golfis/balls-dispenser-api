from uuid     import UUID
from typing   import List, Optional
from datetime import datetime

from pydantic import BaseModel, NoneStr

from .page_base import PageBase

class BallsHeightLogBase(BaseModel):
  measured_height:   int
  notification_made: bool
  notes:             NoneStr

class BallsHeightLogCreate(BallsHeightLogBase): pass

class BallsHeightLogUpdate(BallsHeightLogBase):
  measured_height:   Optional[int]
  notification_made: Optional[bool]

class BallsHeightLog(BallsHeightLogBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True

class BallsHeightLogPage(PageBase[BallsHeightLog]):
  result: List[BallsHeightLog]

class BallsHeightConfigBase(BaseModel):
  notify_at_height:  int
  notification_text: str

class BallsHeightConfigCreate(BallsHeightConfigBase): pass

class BallsHeightConfigUpdate(BallsHeightConfigBase):
  # TODO: add validation '> 0' for `notify_at_height`
  # TODO: add validation 'max len' for `notification_text`
  notify_at_height:  Optional[int]
  notification_text: NoneStr

class BallsHeightConfig(BallsHeightConfigBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True
