from uuid     import UUID
from typing   import List, Optional
from datetime import datetime

from pydantic import BaseModel, NoneStr, EmailStr

from .card      import Card
from .page_base import PageBase

class PlayerBase(BaseModel):
  first_name:   NoneStr
  last_name:    NoneStr
  phone_number: NoneStr
  email:        Optional[EmailStr]
  golfer_id:    NoneStr

class PlayerCreate(PlayerBase):
  is_active: bool = False

class PlayerUpdate(PlayerBase):
  pass

class Player(PlayerBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime
  cards:      List[Card] = []
  is_active:  bool

  class Config:
    orm_mode = True

class PlayerPage(PageBase[Player]):
  result: List[Player]
