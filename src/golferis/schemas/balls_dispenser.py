from uuid     import UUID
from typing   import List, Optional
from datetime import datetime

from pydantic import BaseModel, NoneStr

from .page_base import PageBase

class BallsDispenserLogBase(BaseModel):
  card_id:     Optional[int]
  card_number: NoneStr

class BallsDispenserLogCreate(BallsDispenserLogBase): pass
class BallsDispenserLogUpdate(BallsDispenserLogBase): pass

class BallsDispenserLog(BallsDispenserLogBase):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True

class BallsDispenserLogPage(PageBase[BallsDispenserLog]):
  result: List[BallsDispenserLog]

class BallsDispenserConfigBase(BaseModel):
  no_credit_text:                  str
  unknown_player_text:             str
  current_credit_amount_text:      str
  player_credit_notification_text: str
  notify_at_credit_amount:         List[int]

class BallsDispenserConfigCreate(BallsDispenserConfigBase): pass

class BallsDispenserConfigUpdate(BallsDispenserConfigBase):
  no_credit_text:                  NoneStr
  unknown_player_text:             NoneStr
  current_credit_amount_text:      NoneStr
  player_credit_notification_text: NoneStr
  notify_at_credit_amount:         Optional[List[int]]

class BallsDispenserConfig(BaseModel):
  id:         UUID
  updated_at: datetime
  created_at: datetime

  class Config:
    orm_mode = True
