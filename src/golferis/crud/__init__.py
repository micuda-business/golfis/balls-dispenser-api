from .crud_user             import user
from .crud_player           import player
from .crud_card             import card
from .crud_balls_height     import balls_height_log, balls_height_config
from .crud_balls_dispenser  import balls_dispenser_log, balls_dispenser_config

# For a new basic set of CRUD operations you could just do

# from golferis.db.models.card import Card
# from golferis.schemas.card   import CardCreate, CardUpdate

# from .base import CRUDBase

# card = CRUDBase[Card, CardCreate, CardUpdate](Card)
