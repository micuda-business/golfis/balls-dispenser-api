from typing import Optional, Union, Dict, Any

from sqlalchemy.orm import Session

from golferis.config                    import settings
from golferis.db.models.balls_dispenser import BallsDispenserConfig, BallsDispenserLog
from golferis.schemas.balls_dispenser   import (
  BallsDispenserConfigCreate, BallsDispenserConfigUpdate, BallsDispenserLogCreate,
  BallsDispenserLogUpdate
)

from .base import CRUDBase

class CRUDBallsDispenserConfig(
  CRUDBase[BallsDispenserConfig, BallsDispenserConfigCreate, BallsDispenserConfigUpdate]
):
  class ConfigAlreadyExists(Exception):          pass
  class BallsDispenserConfigNotFound(Exception): pass

  def create(self, db: Session, *, obj_in: BallsDispenserConfigCreate) -> BallsDispenserConfig:
    db_balls_dispenser_config = self._get_config(db)

    if db_balls_dispenser_config: raise self.ConfigAlreadyExists

    return super().create(db, obj_in = obj_in)

  def update(self, db: Session, *,
             obj_in: Union[BallsDispenserConfigUpdate, Dict[str, Any]]) -> BallsDispenserConfig:
    db_balls_dispenser_config = self._get_config(db)

    if db_balls_dispenser_config is None: raise self.BallsDispenserConfigNotFound

    return super().update(db, db_obj = db_balls_dispenser_config, obj_in = obj_in)

  def get_config(self, db: Session) -> BallsDispenserConfig:
    db_balls_dispenser_config = self._get_config(db)

    if db_balls_dispenser_config is None:
      db_balls_dispenser_config = self._create_default_config(db)

    return db_balls_dispenser_config

  def _create_default_config(self, db: Session) -> BallsDispenserConfig:
    no_credit_text                  = settings.BALLS_DISPENSER_NO_CREDIT_TEXT_DEFAULT
    unknown_player_text             = settings.BALLS_DISPENSER_UNKNOWN_PLAYER_TEXT_DEFAULT
    notify_at_credit_amount         = settings.BALLS_DISPENSER_NOTIFY_AT_CREDIT_AMOUNT
    current_credit_amount_text      = settings.BALLS_DISPENSER_CURRENT_CREDIT_AMOUNT_TEXT_DEFAULT
    player_credit_notification_text = \
      settings.BALLS_DISPENSER_PLAYER_CREDIT_NOTIFICATION_TEXT_DEFAULT

    config_create = BallsDispenserConfigCreate(
      no_credit_text                  = no_credit_text,
      unknown_player_text             = unknown_player_text,
      notify_at_credit_amount         = notify_at_credit_amount,
      current_credit_amount_text      = current_credit_amount_text,
      player_credit_notification_text = player_credit_notification_text
    )

    return super().create(db, obj_in = config_create)

  def _get_config(self, db: Session) -> Optional[BallsDispenserConfig]:
    try:               return self.get_multi(db, limit = 1)[0]
    except IndexError: return None


balls_dispenser_config = CRUDBallsDispenserConfig(BallsDispenserConfig)
balls_dispenser_log    = \
    CRUDBase[BallsDispenserLog, BallsDispenserLogCreate, BallsDispenserLogUpdate](BallsDispenserLog)
