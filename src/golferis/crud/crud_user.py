from uuid   import UUID
from typing import Optional, Union, Dict, Any, List

from sqlalchemy     import or_
from sqlalchemy.orm import Session

from golferis.db.models.user    import User
from golferis.schemas.user      import UserCreate, UserUpdate
from golferis.api.core.security import verify_password, get_password_hash

from .base import CRUDBase

class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
  class EmailAlreadyUsed(Exception): pass

  def get_active_managers_phone_numbers(self, db: Session) -> List[str]:
    phone_numbers_result = db \
      .query(self.model.phone_number) \
      .filter(self.model.is_active              == True,
              self.model.is_golf_course_manager == True,
              or_(self.model.phone_number.isnot(None), self.model.phone_number != '')) \
      .order_by(self.model.phone_number) \
      .all()

    return [r[0] for r in phone_numbers_result]

  def get_by_email(self, db: Session, *, email: str) -> Optional[User]:
    return db.query(User).filter(User.email == email).first()

  def authenticate(self, db: Session, *, email: str, password: str) -> Optional[User]:
    user = self.get_by_email(db, email = email)

    if   not user:                                     return None
    elif not verify_password(password, user.password): return None
    else:                                              return user

  def _get_create_data(self, obj_in: UserCreate) -> dict:
    obj_in_data = super()._get_create_data(obj_in)

    obj_in_data['password'] = get_password_hash(obj_in_data['password'])

    return obj_in_data

  def _get_update_data(self, obj_in: Union[UserUpdate, Dict[str, Any]]) -> dict:
    update_data = super()._get_update_data(obj_in)

    if 'password' in update_data and update_data['password']:
      update_data['password'] = get_password_hash(update_data['password'])

    return update_data

  def _validate_data(self,
                     db:     Session,
                     *,
                     data:   Dict[str, Any],
                     db_obj: Optional[User] = None):
    if 'email' in data:
      if self._is_email_used(db, email = data['email'], user_id = db_obj and db_obj.id):
        raise self.EmailAlreadyUsed

  def _is_email_used(self, db: Session, *, email: str, user_id: Optional[UUID] = None) -> bool:
    query = db.query(self.model).filter(self.model.email == email)

    if user_id:
      query = query.filter(self.model.id != user_id)

    return bool(query.count())


user = CRUDUser(User)
