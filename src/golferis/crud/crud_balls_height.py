from typing import Optional, Union, Dict, Any

from sqlalchemy.orm import Session

from golferis.config                 import settings
from golferis.db.models.balls_height import BallsHeightLog, BallsHeightConfig
from golferis.schemas.balls_height   import (
  BallsHeightLogCreate, BallsHeightLogUpdate, BallsHeightConfigCreate, BallsHeightConfigUpdate
)

from .base import CRUDBase

class CRUDBallsHeightConfig(
  CRUDBase[BallsHeightConfig, BallsHeightConfigCreate, BallsHeightConfigUpdate]
):
  class ConfigAlreadyExists(Exception):       pass
  class BallsHeightConfigNotFound(Exception): pass

  def create(self, db: Session, *, obj_in: BallsHeightConfigCreate) -> BallsHeightConfig:
    db_balls_height_config = self._get_config(db)

    if db_balls_height_config: raise self.ConfigAlreadyExists

    return super().create(db, obj_in = obj_in)

  def update(self, db: Session, *,
             obj_in: Union[BallsHeightConfigUpdate, Dict[str, Any]]) -> BallsHeightConfig:
    db_balls_height_config = self._get_config(db)

    if db_balls_height_config is None: raise self.BallsHeightConfigNotFound

    return super().update(db, db_obj = db_balls_height_config, obj_in = obj_in)

  def get_config(self, db: Session) -> BallsHeightConfig:
    db_balls_height_config = self._get_config(db)

    if db_balls_height_config is None:
      db_balls_height_config = self._create_default_config(db)

    return db_balls_height_config

  def _create_default_config(self, db: Session) -> BallsHeightConfig:
    config_create = BallsHeightConfigCreate(
      notify_at_height  = settings.BALLS_HEIGHT_NOTIFY_AT_HEIGHT_DEFAULT,
      notification_text = settings.BALLS_HEIGHT_NOTIFICATION_TEXT_DEFAULT
    )

    return super().create(db, obj_in = config_create)

  def _get_config(self, db: Session) -> Optional[BallsHeightConfig]:
    try:               return self.get_multi(db, limit = 1)[0]
    except IndexError: return None


balls_height_log = \
  CRUDBase[BallsHeightLog, BallsHeightLogCreate, BallsHeightLogUpdate](BallsHeightLog)
balls_height_config = CRUDBallsHeightConfig(BallsHeightConfig)
