from uuid     import UUID
from datetime import datetime
from typing   import Optional, Union, Dict, Any

from sqlalchemy                import or_, and_, func
from sqlalchemy.orm            import Session
from sqlalchemy.sql.expression import BooleanClauseList

from golferis                import schemas, crud
from golferis.db             import models
from golferis.schemas.credit import PurchaseCredit

from .base import CRUDBase

class CRUDPlayer(CRUDBase[models.Player, schemas.PlayerCreate, schemas.PlayerUpdate]):
  class PlayerNotFound(Exception):      pass
  class PlayerHasNoCredit(Exception):   pass
  class PlayerIsDeactivated(Exception): pass
  class EmailAlreadyUsed(Exception):    pass
  class CardNotFound(Exception):        pass
  class CardIsDeactivated(Exception):   pass

  def get_by_email(self, db: Session, *, email: str) -> Optional[models.Player]:
    return db.query(self.model).filter(self.model.email == email).first()

  # def get_by_card_number(self, db: Session, *, card_number: str) -> Optional[models.Player]:
  #   return db \
  #     .query(self.model) \
  #     .join(self.model.held_cards) \
  #     .filter(models.Card.card_number == card_number,
  #             models.Card.is_active   == True) \
  #     .group_by() \
  #     .first()

  def purchase_credit(self, db: Session, *, obj_in: schemas.PurchaseCredit) -> models.PlayerCredit:
    player = self.get(db, id = obj_in.player_id)

    if player is None:       raise self.PlayerNotFound
    if not player.is_active: raise self.PlayerIsDeactivated

    credit_transtaction = self._create_credit_transaction(db, obj_in = obj_in)
    player_credit       = self._debt_player_credit_for(db, player = player)

    # TODO refactor it please
    if player_credit:
      player_credit.credit_purchase_id = credit_transtaction.id

      if not self._is_debt_repaid(player_credit, credit_transtaction):
        new_debt_player_credit = models.PlayerCredit(
          player_id     = player.id,
          credits_spend = player_credit.credits_spend - credit_transtaction.credits_count
        )
        player_credit.credits_spend = credit_transtaction.credits_count

        db.add(new_debt_player_credit)

    else:
      player_credit = models.PlayerCredit(credits_spend      = 0,
                                          player_id          = player.id,
                                          credit_purchase_id = credit_transtaction.id)

    db.add(player_credit)
    db.commit()
    db.refresh(player_credit)

    return player_credit

  def spend_credit(self, db: Session, *, obj_in: schemas.SpendCredit) -> models.CreditTransaction:
    player = self.get(db, id = obj_in.player_id)

    if player is None:       raise self.PlayerNotFound
    if not player.is_active: raise self.PlayerIsDeactivated

    credit_transtaction = self._create_credit_transaction(db, obj_in = obj_in)
    player_credit       = self._get_player_credit(db, player_id = player.id)

    if player_credit is None:
      raise self.PlayerHasNoCredit

    self._increase_credits_spend(db, player_credit = player_credit)

    db.commit()

    return credit_transtaction

  def get_credit_amount(self, db: Session, *, player_id: UUID) -> int:
    if not self.exists(db, id = player_id): raise self.PlayerNotFound

    credits_left_expr = func.sum(
        func.ifnull(models.CreditTransaction.credits_count, 0) - models.PlayerCredit.credits_spend
      ).label('credits_left')


    credits_count = db \
      .query(credits_left_expr) \
      .select_from(models.PlayerCredit) \
      .outerjoin(
        models.CreditTransaction, models.PlayerCredit.credit_purchase_id == models.CreditTransaction.id
      ) \
      .filter(self._player_credit_query_filter(player_id)) \
      .group_by() \
      .scalar()

    if credits_count is None: credits_count = 0

    return credits_count

  def _get_player_credit(self, db: Session, *, player_id: UUID) -> Optional[models.PlayerCredit]:
    return db \
      .query(models.PlayerCredit) \
      .join(models.CreditTransaction) \
      .filter(self._player_credit_query_filter(player_id),
              models.CreditTransaction.credits_count - models.PlayerCredit.credits_spend > 0) \
      .order_by(models.CreditTransaction.created_at) \
      .first()

  @staticmethod
  def _player_credit_query_filter(player_id: UUID) -> BooleanClauseList:
    return and_(models.PlayerCredit.player_id == player_id,
                or_(models.CreditTransaction.expiry_at.is_(None),
                    models.CreditTransaction.expiry_at >= datetime.now()))

  @staticmethod
  def _increase_credits_spend(db: Session, *, player_credit: models.PlayerCredit) -> None:
    player_credit.credits_spend = models.PlayerCredit.credits_spend + 1

    db.add(player_credit)
    db.flush()

  def _create_credit_transaction(
    self, db: Session, *, obj_in: Union[schemas.PurchaseCredit, schemas.SpendCredit]
  ) -> models.CreditTransaction:
    if obj_in.card_id:
      if card := crud.card.get(db, id = obj_in.card_id):
        if card.owner_id != obj_in.player_id: raise self.CardNotFound
        elif not card.is_active:              raise self.CardIsDeactivated
      else:                                   raise self.CardNotFound

    credit_transtaction = models.CreditTransaction(**obj_in.dict())

    db.add(credit_transtaction)
    db.flush()
    db.refresh(credit_transtaction)

    return credit_transtaction

  def _validate_data(self,
                     db:     Session,
                     *,
                     data:   Dict[str, Any],
                     db_obj: Optional[models.Player] = None):
    if 'email' in data:
      if self._is_email_used(db, email = data['email'], user_id = db_obj and db_obj.id):
        raise self.EmailAlreadyUsed

  def _is_email_used(self, db: Session, *, email: str, user_id: Optional[UUID] = None) -> bool:
    query = db.query(self.model).filter(self.model.email.isnot(None), self.model.email == email)

    if user_id:
      query = query.filter(self.model.id != user_id)

    return bool(query.count())

  @staticmethod
  def _is_debt_repaid(
    debt_player_credit:          models.PlayerCredit,
    purchase_credit_transaction: models.CreditTransaction
  ) -> bool:
    return purchase_credit_transaction.credits_count >= debt_player_credit.credits_spend

  @staticmethod
  def _debt_player_credit_for(
    db: Session, *, player: models.Player
  ) -> Optional[models.PlayerCredit]:
    return db \
      .query(models.PlayerCredit) \
      .filter(models.PlayerCredit.credit_purchase_id.is_(None)) \
      .first()


player = CRUDPlayer(models.Player)
