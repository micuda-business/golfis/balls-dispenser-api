from uuid import UUID

from sqlalchemy.orm import Session

from golferis.db.models.card import Card
from golferis.schemas.card   import CardCreate, CardUpdate

from .     import crud_player
from .base import CRUDBase

class CRUDCard(CRUDBase[Card, CardCreate, CardUpdate]):
  class PlayerAlreadyOwnsCard(Exception): pass

  def create(self, db: Session, *, obj_in: CardCreate) -> Card:
    if (owner_id := obj_in.owner_id) is not None:
      owner = crud_player.player.get(db, id = owner_id)

      if owner is None:
        raise crud_player.player.PlayerNotFound

      if obj_in.is_active \
          and self.exists_active_card_for_player(db          = db,
                                                  player_id   = owner_id,
                                                  card_number = obj_in.card_number):
        raise self.PlayerAlreadyOwnsCard

    return super().create(db, obj_in = obj_in)

  def exists_active_card_for_player(self, db: Session, player_id: UUID, card_number: str) -> bool:
    query = db \
      .query(self.model) \
      .filter(self.model.owner_id    == player_id,
              self.model.card_number == card_number,
              self.model.is_active.is_(True))

    return bool(query.count())

  def active_card_for_card_number(self, db: Session, card_number: str) -> Card:
      return db \
        .query(self.model) \
        .filter(self.model.card_number == card_number,
                self.model.is_active.is_(True)) \
        .first()


card = CRUDCard(Card)
