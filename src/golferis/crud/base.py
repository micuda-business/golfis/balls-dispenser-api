from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

from sqlalchemy.orm   import Session
from pydantic         import BaseModel
from fastapi.encoders import jsonable_encoder

from golferis.db.models.model_base import ModelBase

ModelType        = TypeVar('ModelType',        bound = ModelBase)
CreateSchemaType = TypeVar('CreateSchemaType', bound = BaseModel)
UpdateSchemaType = TypeVar('UpdateSchemaType', bound = BaseModel)

class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
  def __init__(self, model: Type[ModelType]):
    """
    CRUD object with default methods to Create, Read, Update, Delete (CRUD).

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
    """
    self.model = model

  def get(self, db: Session, id: Any) -> Optional[ModelType]:
    return db.query(self.model).filter(self.model.id == id).first()

  def get_multi(self, db: Session, *, skip: int = 0, limit: int = 100) -> List[ModelType]:
    return db.query(self.model).offset(skip).limit(limit).all()

  def create(self, db: Session, *, obj_in: CreateSchemaType) -> ModelType:
    obj_in_data = self._get_create_data(obj_in)

    self._validate_data(db, data = obj_in_data)

    db_obj      = self.model(**obj_in_data)  # type: ignore

    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)

    return db_obj

  def update(self, db: Session, *, db_obj: ModelType,
             obj_in: Union[UpdateSchemaType, Dict[str, Any]]) -> ModelType:
    obj_data    = jsonable_encoder(db_obj)
    update_data = self._get_update_data(obj_in)

    self._validate_data(db, data = update_data, db_obj = db_obj)

    for field in obj_data:
      if field in update_data:
        setattr(db_obj, field, update_data[field])

    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)

    return db_obj

  def remove(self, db: Session, *, id: int) -> ModelType:
    obj = db.query(self.model).get(id)

    db.delete(obj)
    db.commit()

    return obj

  def get_count(self, db: Session) -> int:
    return db.query(self.model).count()

  def exists(self, db: Session, *, id: int) -> bool:
    return bool(db.query(self.model).filter(self.model.id == id).count())

  def _get_create_data(self, obj_in: CreateSchemaType) -> dict:
    return jsonable_encoder(obj_in)

  def _get_update_data(self, obj_in: Union[UpdateSchemaType, Dict[str, Any]]) -> dict:
    if isinstance(obj_in, dict): return obj_in
    else:                        return obj_in.dict(exclude_unset = True)

  def _validate_data(self,
                     db:     Session,
                     *,
                     data:   Dict[str, Any],
                     db_obj: Optional[ModelType] = None):
    pass
