import abc, time
from typing import Union, Callable, List, Tuple

import wiringpi

class RFIDIterator:
  def __init__(self, rfid_reader: 'RFID', check_data_time: Union[int, float],
               after_read_data: Callable[['RFID'], None] = lambda rfid_reader: None):
    self._rfid_reader     = rfid_reader
    self._check_data_time = check_data_time
    self._after_read_data = after_read_data

  def __iter__(self): return self

  def __next__(self):
    while not self._rfid_reader.has_data:
      time.sleep(self._check_data_time)

    bits_count, data = self._rfid_reader.read_data()

    self._after_read_data(self._rfid_reader)

    return bits_count, data

class RFID(abc.ABC):
  get_data_0:      Callable[[], None] = abc.abstractmethod(lambda self: None)
  get_data_1:      Callable[[], None] = abc.abstractmethod(lambda self: None)
  has_data:        bool               = property(abc.abstractmethod(lambda self: None))
  check_data_time: Union[int, float]  = property(abc.abstractmethod(lambda self: None))

  _data:      List = None
  _bit_count: int  = None

  def __init__(self, *, d0_pin, d1_pin, sound_pin):
    self._reset()
    self._sound_pin = sound_pin

    wiringpi.wiringPiSetup()

    wiringpi.pinMode(d0_pin,    wiringpi.INPUT)
    wiringpi.pinMode(d1_pin,    wiringpi.INPUT)
    wiringpi.pinMode(sound_pin, wiringpi.OUTPUT)

    wiringpi.wiringPiISR(d0_pin, wiringpi.INT_EDGE_FALLING, self.get_data_0)
    wiringpi.wiringPiISR(d1_pin, wiringpi.INT_EDGE_FALLING, self.get_data_1)

  def __iter__(self) -> RFIDIterator:
    return RFIDIterator(rfid_reader = self, check_data_time = self.check_data_time,
                        after_read_data = lambda rfid_reader: rfid_reader.make_beep(200, 1))

  def read_data(self) -> Tuple[int, List]:
    bit_count, data = 0, []

    if self.has_data:
      bit_count = self._bit_count
      data      = self._data[:int(self._bit_count / 8) + 1]

      self._reset()

    return bit_count, data

  def make_beep(self, millisecs: int, times: int) -> None:
    for _ in range(times):
      wiringpi.digitalWrite(self._sound_pin, wiringpi.LOW)
      wiringpi.delay(millisecs)

      wiringpi.digitalWrite(self._sound_pin, wiringpi.HIGH)
      wiringpi.delay(int(millisecs / 2))

  def _reset_data(self):      self._data      = []
  def _reset_bit_count(self): self._bit_count = 0

  def _reset(self):
    self._reset_data()
    self._reset_bit_count()

class WiegandRFID(RFID):
  MAXWIEGANDBITS = 32
  READER_TIMEOUT = 3000000

  check_data_time = .005

  _bit_time = 0

  def get_data_0(self):
    data_index = self._data_index

    if self._is_data_index_valid(data_index):
      self._bit_count         += 1
      self._data[data_index] <<= 1

    self._bit_time = self._get_monotonic_timestamp()

  def get_data_1(self):
    data_index = self._data_index

    if self._is_data_index_valid(data_index):
      self._bit_count         += 1
      self._data[data_index] <<= 1
      self._data[data_index]  |= 1

    self._bit_time = self._get_monotonic_timestamp()

  @property
  def has_data(self):
    now   = self._get_monotonic_timestamp()
    delta = now - self._bit_time

    return bool(self._bit_count) if delta > self.READER_TIMEOUT else False

  _data_index = property(lambda self: int(self._bit_count / 8))

  def _reset_data(self):
    self._data = [0 for _ in range(self.MAXWIEGANDBITS)]

  def _is_data_index_valid(self, data_index):
    return data_index < self.MAXWIEGANDBITS

  @staticmethod
  def _get_monotonic_timestamp():
    return time.clock_gettime_ns(time.CLOCK_MONOTONIC)
