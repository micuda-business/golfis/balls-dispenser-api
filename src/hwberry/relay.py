from hwberry.utils import gpio

class Relay(gpio.GPIODevice):
  on_at: gpio.Voltage = None

  def __init__(self, pin: int):
    if self.__class__ is Relay:
      raise TypeError(f'{self.__class__.__name__} cannot be instantiated.')

    super().__init__()

    self._port = self.register_output_port(pin)

  def __init_subclass__(cls, /, on_at: gpio.Voltage, **kwargs):
    validated_on_at = gpio.Voltage(on_at)

    super().__init_subclass__(**kwargs)

    cls.on_at = validated_on_at

  on  = gpio.requires_gpio_setup(lambda self: self._port.set_by_voltage(self.on_at))
  off = gpio.requires_gpio_setup(lambda self: self._port.set_by_voltage(~self.on_at))

class LowOnRelay(Relay,  on_at = gpio.Voltage.LOW):  pass
class HighOnRelay(Relay, on_at = gpio.Voltage.HIGH): pass
