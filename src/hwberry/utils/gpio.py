import time
from enum      import Enum
from functools import wraps

from RPi import GPIO

class InvalidDeviceError(TypeError):
  def __init__(self, device_name):
    message = \
      f'Device "{device_name}" is not valid for use of `requires_gpio_setup` decorator. ' \
      '(attribute `_is_gpio_initialized` is missing)'

    super().__init__(message)

class GPIOSetupRequired(Exception):
  def __init__(self, func_name):
    message = \
      f'Operation "{func_name}" requires GPIO was set up. You have to call `gpio_setup` first.'

    super().__init__(message)

def requires_gpio_setup(func):
  @wraps(func)
  def inner(self, *args, **kwargs):
    if not hasattr(self, '_is_gpio_initialized'): raise  InvalidDeviceError(self.__class__.__name__)
    if self._is_gpio_initialized:                 return func(self, *args, **kwargs)
    else:                                         raise  GPIOSetupRequired(func.__name__)

  return inner

class PortDirection(Enum):
  IN  = GPIO.IN
  OUT = GPIO.OUT

class Voltage(Enum):
  LOW  = GPIO.LOW
  HIGH = GPIO.HIGH

  def __invert__(self):
    if self is self.LOW: return self.HIGH
    else:                return self.LOW

class Port:
  # TODO consider raising exception when use invalid methods,
  # ie. calling `set_low`/`set_high` for PortDirection.IN
  # or calling `is_low`/`is_high` for PortDirection.OUT
  def __init__(self, gpio: int, direction: PortDirection):
    self._gpio      = gpio
    self._direction = direction

  gpio:      int           = property(lambda self: self._gpio)
  direction: PortDirection = property(lambda self: self._direction)
  is_low:    bool          = property(lambda self: GPIO.input(self.gpio) == GPIO.LOW)
  is_high:   bool          = property(lambda self: GPIO.input(self.gpio) == GPIO.HIGH)

  def set_by_voltage(self, voltage: Voltage) -> None:
    GPIO.output(self.gpio, voltage.value)

  def set_low(self)  -> None: self.set_by_voltage(Voltage.LOW)
  def set_high(self) -> None: self.set_by_voltage(Voltage.HIGH)

  def high_for(self, sec: int) -> None:
    self.set_high()
    time.sleep(sec)
    self.set_low()

class GPIODevice:
  def __init__(self):
    self._ports               = []
    self._is_gpio_initialized = False

  def __enter__(self):
    self.gpio_setup()

    return self

  def __exit__(self, exc_type, exc_value, traceback):
    self.gpio_cleanup()

    return False if exc_type else True

  def register_port(self, gpio: int, direction: PortDirection) -> Port:
    port = Port(gpio, direction)

    self._ports.append(port)

    return port

  def register_input_port(self, gpio: int) -> Port:
    return self.register_port(gpio, PortDirection.IN)

  def register_output_port(self, gpio: int) -> Port:
    return self.register_port(gpio, PortDirection.OUT)

  def gpio_setup(self) -> None:
    for port in self._ports: GPIO.setup(port.gpio, port.direction.value)

    self._is_gpio_initialized = True

  def gpio_cleanup(self) -> None:
    GPIO.cleanup([port.gpio for port in self._ports])

    self._is_gpio_initialized = False
