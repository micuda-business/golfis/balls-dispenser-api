﻿import time
import smbus

class I2CDevice:
  def __init__(self, addr, port = 1):
    self._address = addr
    self._sm_bus  = smbus.SMBus(port)

  ### READ
  def read(self):                 return self._sm_bus_read('read_byte')
  def read_data(self,       cmd): return self._sm_bus_read('read_byte_data',  cmd)
  def read_block_data(self, cmd): return self._sm_bus_read('read_block_data', cmd)

  def _sm_bus_read(self, method, *args): return getattr(self._sm_bus, method)(self._address, *args)

  ### WRITE
  def write_cmd(self,        cmd):       self._sm_bus_write('write_byte',       cmd)
  def write_cmd_arg(self,    cmd, data): self._sm_bus_write('write_byte_data',  cmd, data)
  def write_block_data(self, cmd, data): self._sm_bus_write('write_block_data', cmd, data)

  def _sm_bus_write(self, method, *args):
    getattr(self._sm_bus, method)(self._address, *args)
    time.sleep(0.0001)
