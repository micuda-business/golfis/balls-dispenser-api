import time

from hwberry.utils import gpio

SOUND_SPEED = 34300 # cm/s

class UltrasonicRangeSensor(gpio.GPIODevice):
  TRIGGER_PULSE_DURATION = 0.00001

  def __init__(self, trigger_pin: int, echo_pin: int):
    super().__init__()

    self._echo_port    = self.register_input_port(echo_pin)
    self._trigger_port = self.register_output_port(trigger_pin)

  @gpio.requires_gpio_setup
  def measure_distance(self) -> float:
    self._trigger_pulse()

    sent_at_default, received_at_default = time.time(), time.time()
    sent_at        = self._signal_sent_at(sent_at_default)
    received_at    = self._signal_received_at(received_at_default)
    pulse_duration = received_at - sent_at

    return (pulse_duration * SOUND_SPEED) / 2

  def _trigger_pulse(self):
    self._trigger_port.high_for(sec = self.TRIGGER_PULSE_DURATION)

  def _signal_sent_at(self, sent_at_default):
    sent_at = sent_at_default

    while self._echo_port.is_low: sent_at = time.time()

    return sent_at

  def _signal_received_at(self, received_at_default):
    received_at = received_at_default

    while self._echo_port.is_high: received_at = time.time()

    return received_at
