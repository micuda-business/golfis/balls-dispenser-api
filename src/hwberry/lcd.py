﻿import sys, time
from typing import Union, List

from .utils import I2CDevice

# commands
LCD_RETURNHOME     = 0x02
LCD_CURSORSHIFT    = 0x10
LCD_FUNCTIONSET    = 0x20
LCD_SETCGRAMADDR   = 0x40
LCD_SETDDRAMADDR   = 0x80
LCD_CLEARDISPLAY   = 0x01
LCD_ENTRYMODESET   = 0x04
LCD_DISPLAYCONTROL = 0x08

# flags for display entry mode
LCD_ENTRYLEFT           = 0x02
LCD_ENTRYRIGHT          = 0x00
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_BLINKON    = 0x01
LCD_BLINKOFF   = 0x00
LCD_CURSORON   = 0x02
LCD_CURSOROFF  = 0x00
LCD_DISPLAYON  = 0x04
LCD_DISPLAYOFF = 0x00

# flags for display/cursor shift
LCD_MOVELEFT    = 0x00
LCD_MOVERIGHT   = 0x04
LCD_CURSORMOVE  = 0x00
LCD_DISPLAYMOVE = 0x08

# flags for function set
LCD_2LINE    = 0x08
LCD_1LINE    = 0x00
LCD_5x8DOTS  = 0x00
LCD_5x10DOTS = 0x04
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00

# flags for backlight control
LCD_BACKLIGHT   = 0x08
LCD_NOBACKLIGHT = 0x00

ENABLE_BIT          = 0b00000100
READ_WRITE_BIT      = 0b00000010
REGISTER_SELECT_BIT = 0b00000001

class LCD:
  LINE_TO_COMMAND = { 1: 0x80,
                      2: 0xC0,
                      3: 0x94,
                      4: 0xD4 }

  class InvalidBacklightState(ValueError):
    def __init__(self, invalid_state):
      super().__init__(f'State "{invalid_state}" is invalid.')

  def __init__(self, *, address: int, lines_count: int, line_width: int):
    self._line_width  = line_width
    self._lines_count = lines_count

    self._device = I2CDevice(address)

    self._write_command(0x03)
    self._write_command(0x03)
    self._write_command(0x03)
    self._write_command(0x02)

    self._write_command(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
    self._write_command(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
    self._write_command(LCD_CLEARDISPLAY)
    self._write_command(LCD_ENTRYMODESET | LCD_ENTRYLEFT)

    time.sleep(0.2)

  def print(self, *, lines: List[str], trim_text = False) -> None:
    for line_no, text in enumerate(lines[:self._lines_count], start = 1):
      self.display_text(text, line_no, trim_text = trim_text)

  def display_text(self, text: str, line_no: int, trim_text = False) -> None:
    self._write_command(self.LINE_TO_COMMAND.get(line_no, self.LINE_TO_COMMAND[1]))

    for char in text[:self._line_width] if trim_text else text:
      self._write_command(ord(char), REGISTER_SELECT_BIT)

  def clear_display(self) -> None:
    self._write_command(LCD_CLEARDISPLAY)
    self._write_command(LCD_RETURNHOME)

  def set_backlight(self, state: Union[str, bool]) -> None:
    state = state.lower() if isinstance(state, str) else state

    if   state in ['on',  True]:  self._device.write_cmd(LCD_BACKLIGHT)
    elif state in ['off', False]: self._device.write_cmd(LCD_NOBACKLIGHT)
    else:                         raise self.InvalidBacklightState(state)

  def set_backlight_on(self)  -> None: self.set_backlight(True)
  def set_backlight_off(self) -> None: self.set_backlight(False)

  def _write_command(self, cmd, mode = 0):
    self._write_four_bits(mode | (cmd & 0xF0))
    self._write_four_bits(mode | ((cmd << 4) & 0xF0))

  def _write_four_bits(self, data):
    self._device.write_cmd(data | LCD_BACKLIGHT)
    self._strobe(data)

  def _strobe(self, data):
    self._device.write_cmd(data | ENABLE_BIT | LCD_BACKLIGHT)
    time.sleep(.0005)
    self._device.write_cmd((data & ~ENABLE_BIT) | LCD_BACKLIGHT)
    time.sleep(.0001)
