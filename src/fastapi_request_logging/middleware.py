from __future__ import annotations

import logging, http.client
from typing import List, Tuple, Dict

from fastapi                  import Request, status
from fastapi.logger           import logger as fastapi_logger
from starlette.datastructures import Address, URL

from .deps import REQUEST_LOGGING_PAYLOAD_ATTR

# TODO is it possible get endpoint method (e.g. from dependency)?
# TODO consider allowing logging of headers, body, form_data on separate line
# TODO consider limiting max body content length

async def request_logging_middleware(request: Request, call_next):
  try:
    response = await call_next(request)

  except Exception as e:
    exc_info    = e
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

    raise

  else:
    exc_info    = None
    status_code = response.status_code

    return response

  finally:
    log_message = LogMessage.from_request(request = request, http_status_code = status_code)
    message     = log_message.render()

    if status_code >= status.HTTP_400_BAD_REQUEST:
      fastapi_logger.log(logging.ERROR, message, exc_info = exc_info)
    else:
      fastapi_logger.info(message)

class LogMessage:
  TEMPLATE = \
    '{client} - "{http_method} {url_path} {http_protocol}" {http_status} {headers} {payload}'

  @classmethod
  def from_request(cls, *, request: Request, http_status_code: int) -> LogMessage:
    return cls(url              = request.url,
               client_address   = request.client,
               http_status_code = http_status_code,
               http_method      = request.method.upper(),
               headers          = request.scope['headers'],
               payload          = getattr(request.state, REQUEST_LOGGING_PAYLOAD_ATTR, None),
               http_protocol    = f'{request.scope["type"]}/{request.scope["http_version"]}'.upper())

  def __init__(self,
               *,
               url:              URL,
               http_method:      str,
               http_protocol:    str,
               http_status_code: int,
               client_address:   Address,
               headers:          List[Tuple[bytes, bytes]],
               payload:          Optional[Union[Dict[str, str], str]] = None):
    self._url              = url
    self._headers          = headers
    self._payload          = payload
    self._client_address   = client_address
    self._http_method      = http_method
    self._http_protocol    = http_protocol
    self._http_status_code = http_status_code

  def render(self, template: str = None) -> str:
    if template is None:
      template = self.TEMPLATE

    return template.format(**self._get_template_context()).strip()

  @property
  def client(self) -> str:
    tokens = []

    if self._client_address.host: tokens.append(self._client_address.host)
    if self._client_address.port: tokens.append(str(self._client_address.port))
    if not tokens:                tokens.append('[NO CLIENT ADDRESS]')

    return ':'.join(tokens)

  @property
  def url_path(self) -> str:
    return str(self._url.replace(scheme = '', netloc = ''))

  @property
  def http_method(self) -> str:
    return self._http_method

  @property
  def http_protocol(self) ->  str:
    return self._http_protocol

  @property
  def http_status(self) -> str:
    return f'{self.http_status_code} {self.http_status_name}'.strip()

  @property
  def http_status_code(self) -> int:
    return self._http_status_code

  @property
  def http_status_name(self) -> str:
    try:             return http.client.responses[self.http_status_code]
    except KeyError: return ''

  @property
  def payload(self) -> str:
    payload_tokens = []

    if self._payload:
      payload_tokens.append(f'<PAYLOAD>{self._payload}')

    return ' '.join(payload_tokens)

  @property
  def headers(self) -> str:
    # TODO hide sensitive data in headers
    # TODO consider include/exclude headers
    headers_dict = { name.decode(): value.decode() for  name, value in self._headers }

    return f'<HEADERS>{headers_dict}'

  def _get_template_context(self) -> Dict[str, str]:
    return {
      var: getattr(self, var)
      for var
      in  ['client', 'url_path', 'payload', 'headers', 'http_status',
           'http_status_code', 'http_status_name', 'http_method', 'http_protocol']
    }

  def __str__(self) -> str:
    return self.render()
