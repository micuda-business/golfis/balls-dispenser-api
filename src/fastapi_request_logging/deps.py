from functools import reduce
from typing    import List, Tuple, Any, Optional

from fastapi import Request, Header

REQUEST_LOGGING_PAYLOAD_ATTR = 'request_logging_payload'

def multi_values_dict(data: List[Tuple[str, Any]]):
  def inner(accumulator, kv_pair):
    key, value = kv_pair

    if key in accumulator:
      if isinstance(accumulated := accumulator[key], list): accumulated.append(value)
      else:                                                 accumulator[key] = [accumulated, value]
    else:
      accumulator[key] = value

    return accumulator

  return reduce(inner, data, {})

def media_type_for(*, content_type):
  return content_type.split(';')[0]

async def request_logging_payload(request: Request, content_type: Optional[str] = Header(None)):
  if hasattr(request.state, REQUEST_LOGGING_PAYLOAD_ATTR):
    return

  payload            = None
  content_media_type = None

  if content_type:
    content_media_type = media_type_for(content_type = content_type)

  if content_media_type in ['multipart/form-data', 'application/x-www-form-urlencoded']:
    form_data = await request.form()
    payload   = multi_values_dict(form_data.multi_items())

  elif content_media_type == 'application/json':
    payload = await request.json()

  else:
    payload = await request.body()

  setattr(request.state, REQUEST_LOGGING_PAYLOAD_ATTR, payload)
