import abc
from typing import Any

class ResultObject(abc.ABC):
  def __init__(self, detail: Any = None):
    self._detail = detail

  detail:     Any  = property(lambda self: self._detail)
  is_success: bool = property(abc.abstractmethod(lambda self: None))
  is_failure: bool = property(abc.abstractmethod(lambda self: None))

class Success(ResultObject):
  is_success: bool = property(lambda self: True)
  is_failure: bool = property(lambda self: False)

class Failure(ResultObject):
  is_success: bool = property(lambda self: False)
  is_failure: bool = property(lambda self: True)
