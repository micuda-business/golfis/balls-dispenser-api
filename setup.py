#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import os
from os import path
from pathlib import Path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description = f.read()

def get_scripts():
  scripts_dir = Path('.', 'src', 'golferis', 'scripts')

  return [str(executable)
          for executable
          in [f for f in scripts_dir.iterdir() if f.is_file() and os.access(f, os.X_OK)]]

setup(
  name                 = 'golferis', # ball dispenser
  version              = '0.0.1',
  description          = '',
  long_description     = long_description,
  url                  = '', # TODO
  author               = 'Adam Mičuda',
  author_email         = 'adam.micuda@gmail.com',
  license              = 'MIT', # FIXME
  keywords             = '', # 'json api django auth',
  packages             = find_packages(where = 'src'), # TODO
  package_dir          = {'': 'src'},
  include_package_data = True,

  install_requires = [
    'RPi.GPIO',
    'smbus',
    'wiringpi',
    'pydantic[dotenv,email]',
    'fastapi',
    'sqlalchemy',
    'sqlalchemy-repr',
    'mysqlclient',
    'passlib[bcrypt]',
    'python-jose[cryptography]',
    'python-multipart',
    'jinja2',
    'loguru',
    'alembic',
    'requests'
  ],

  # List additional groups of dependencies here (e.g. development
  # dependencies). You can install these using the following syntax,
  # for example:
  # $ pip install -e .[dev,test]
  extras_require = {
    'dev': [
      'uvicorn',
      'mycli',
      'httpie'
    ],
    'production': [
      'uvicorn',
      'gunicorn'
    ],
    'test': [
      'pytest',
      'flexmock',
      'factory_boy',
      'freezegun',
      'pydash',
      'pytest-lazy-fixture'
      # 'psycopg2-binary>=2.8',
    ],
    'pytest-watch': [
      'pytest-watch',
      'pytest-testmon'
    ]
  },

  scripts = get_scripts(),

  # # To provide executable scripts, use entry points in preference to the
  # # "scripts" keyword. Entry points provide cross-platform support and allow
  # # pip to create the appropriate form of executable for the target platform.
  # entry_points={
  #   'console_scripts': [
  #     'sample=sample:main',
  #   ],
  # },

  # # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
  # classifiers=[
  #   # How mature is this project? Common values are
  #   #   3 - Alpha
  #   #   4 - Beta
  #   #   5 - Production/Stable
  #   'Development Status :: 3 - Alpha',

  #   # Indicate who your project is intended for
  #   'Intended Audience :: Developers',
  #   'Topic :: Software Development :: Build Tools',

  #   # Pick your license as you wish (should match "license" above)
  #   'License :: OSI Approved :: MIT License',

  #   # Specify the Python versions you support here. In particular, ensure
  #   # that you indicate whether you support Python 2, Python 3 or both.
  #   'Programming Language :: Python :: 2',
  #   'Programming Language :: Python :: 2.6',
  #   'Programming Language :: Python :: 2.7',
  #   'Programming Language :: Python :: 3',
  #   'Programming Language :: Python :: 3.2',
  #   'Programming Language :: Python :: 3.3',
  #   'Programming Language :: Python :: 3.4',
  # ],
  )
